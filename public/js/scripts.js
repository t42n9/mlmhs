			
		
			
//Page Preloader
$(window).load(function() { 
	$("#loader").delay(20).fadeOut(); 
	$(".mask").delay(100).fadeOut("slow");
});


//Back To Top
/*	$(function() {
		$('a#back-top').click(function () {
			$('html, body').stop().animate({
				scrollTop: 0
			}, 1500,'easeInOutExpo');
		});
		
		
		
	});*/


//Navigation Scrolling
	$(function() {
		$('.nav a, .nav li a, #home-center a, a.move').bind('click',function(event){
		var $anchor = $(this);
	
		$('html, body').stop().animate({
		scrollTop: $($anchor.attr('href')).offset().top -44
		}, 1500,'easeInOutExpo');
		
		event.preventDefault();
		});
	});


	
 

//Parallax	
$(window).bind('load', function () {
	parallaxInit();						  
});

	function parallaxInit() {
		$('#quote-parallax').parallax("30%", 0.1);
		/*$('#contact-parallax').parallax("30%", 0.1);
		$('#home.parallax').parallax("30%", 0.1);*/
		/*add as necessary*/
	}	



  
  
//BxSlider
$(document).ready(function(){
	
	var onMobile = false;
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) { onMobile = true; }
	
	/*$('.fullwidth-slider').bxSlider({
		mode: "fade",
		speed: 1000,
		pager: false,
		
		onSlideBefore: function($slideElement) {
			($slideElement).find('.slide-caption').fadeOut().animate({top:'100px'},{queue:false, easing: 'easeOutQuad', duration: 550});
			($slideElement).find('.slide-caption').hide().animate({top:'-100px'});
		},
		onSlideAfter: function($slideElement) {
			($slideElement).find('.slide-caption').fadeIn().animate({top:'0'},{queue:false, easing: 'easeOutQuad', duration: 450});
		},
		
	});*/
	
	/*$('.bx-wrapper .bx-controls-direction a').attr('data-500','top:83%; opacity: 0;').attr('data-start','top:50%; opacity: 1;');*/
	
	
	if( ( onMobile === false ) && ( $('.parallax-slider').length ) ) {
	
		skrollr.init({
			edgeStrategy: 'set',
			smoothScrolling: false,
			forceHeight: false
		});
		
	}	
    
	$('.text-slide-vertical').bxSlider({
		controls: false,
		adaptiveHeight: true, 
		pager: false,		
		auto:true,
		mode:'fade',
		pause: 8000,
	});
	

	
});  
  
  
  

$(document).ready(function(){

//Elements Fading
	$('.element_from_top').each(function () {
		$(this).appear(function() {
		  $(this).delay(150).animate({opacity:1,top:"0px"},1000);
		});	
	});
	
	$('.element_from_bottom').each(function () {
		$(this).appear(function() {
		  $(this).delay(150).animate({opacity:1,bottom:"0px"},1000);
		});	
	});
	
	
	$('.element_from_left').each(function () {
		$(this).appear(function() {
		  $(this).delay(150).animate({opacity:1,left:"0px"},1000);
		});	
	});
	
	
	$('.element_from_right').each(function () {
		$(this).appear(function() {
		  $(this).delay(150).animate({opacity:1,right:"0px"},1000);
		});	
	});
	
	$('.element_fade_in').each(function () {
		$(this).appear(function() {
		  $(this).delay(150).animate({opacity:1,right:"0px"},1000);
		});	
	});


//Animated Progress Bars
/*	$('.bar li').each(function () {
		$(this).appear(function() {
		  $(this).animate({opacity:1,left:"0px"},1200);
		  var b = $(this).find("span").attr("data-width");
		  $(this).find("span").animate({
			width: b + "%"
		  }, 1700, "easeOutCirc");
		});	
	});


//Animated Counters
	$('.counters').each(function () {
		$(".timer .count").appear(function() {
		var counter = $(this).html();
		$(this).countTo({
			from: 0,
			to: counter,
			speed: 2000,
			refreshInterval: 60,
			});
		});
	});
	*/

//Fading Out AlertBox
/*	$('.shortcode_alertbox').find('.box_close').click(function(){
		$(this).parents('.alertboxes').animate({opacity:0},300).animate({height:"0px"});
	});*/



});