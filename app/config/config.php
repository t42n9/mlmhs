<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6LcEe_oSAAAAANbhD1i9XtJmLj17YfPlEFCjJnKW',
	'private_key'	=> '6LcEe_oSAAAAAPSVUh6Er9NHSsurowwNnXHDXQrI',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
	
);