<?php

define('DEFAULT_PASSWORD', '123456aB');


class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('ParametersTableSeeder');
		$this->command->info('Tabel parameters kelar (y)\n');

		$this->call('BonusLeaderTableSeeder');
		$this->command->info('Tabel bonus_leader kelar (y)\n');

		$this->call('BonusSponsorTableSeeder');
		$this->command->info('Tabel bonus_sponsor kelar (y)\n');

		$this->call('LeadersTableSeeder');
		$this->command->info('Tabel leaders kelar (y)\n');

		$this->call('AnggotaTableSeeder');
		$this->command->info('Tabel anggota kelar (y)\n');

		$this->call('AkunTableSeeder');
		$this->command->info('Tabel akun kelar (y)\n');

		$this->call('BankTableSeeder');
		$this->command->info('Tabel bank kelar (y)\n');

		$this->call('VUsersViewSeeder');
		$this->command->info('View vusers kelar (y)\n');
		 
		$this->command->info('Semua data sudah masuk, enak euuuyyy,,,,!!');

	}

}

class VUsersViewSeeder extends Seeder {

	public function run() {

		$query = 	'CREATE OR REPLACE
					 VIEW `vusers`
					 AS select akun.id,akun.id_anggota,
					akun.id_login,akun.nick_name,
					akun.kata_sandi,akun.id_sponsor,
					akun.id_manager,akun.pin,
					akun.pin_kadaluarsa,akun.is_manager,
					akun.level_manager,akun.is_aktif,
					akun.is_blokir,akun.is_main,
					akun.kiri, akun.kanan, 
					anggota.nama,anggota.ktp,anggota.email,
					anggota.hp, anggota.alamat, anggota.kota,
					anggota.anggota_type,
					akun.created_at,
					count(akun2.id) AS jml_akun,
					coalesce((select count(sejarah_login.is_gagal) AS jml_gagal from sejarah_login 
					where (sejarah_login.is_gagal <> 0) and (sejarah_login.waktu >= (now() - interval 10 minute)) 
					and (sejarah_login.id_akun = akun.id)),0) AS jml_gagal 
					from ((akun join anggota on((anggota.id = akun.id_anggota))) 
					left join akun akun2 on((anggota.id = akun2.id_anggota))) 
					group by akun.id,akun.id_anggota,akun.id_login,akun.nick_name,akun.kata_sandi';

		DB::statement($query);
	}
}

class ParametersTableSeeder extends Seeder {

	public function run() {

		DB::table('parameters')->delete();

		//Bikin contohan parameter
		DB::table('parameters')->insertGetId(
			array('status'=>1, 'created_by'=>'sys', 'updated_by'=>'sys', 'max_akun_per_member'=>20)
		);
	}

	
}

class BonusLeaderTableSeeder extends Seeder {

	public function run() {

		DB::table('bonus_leader')->delete();

		DB::table('bonus_leader')->insert(array(

			array('id'=>1, 'uraian'=>'Leader L2 (Leader Pertama di Atas Akun)', 'bonus_persen'=>5.00),
			array('id'=>2, 'uraian'=>'Leader L3 (Leader Pertama di Atas Leader L2)', 'bonus_persen'=>2.00),
			array('id'=>3, 'uraian'=>'Leader L4 (Leader Pertama di Atas Leader L3)', 'bonus_persen'=>0.25),
			array('id'=>4, 'uraian'=>'Leader L5+n (Leader Pertama dst di Atas Leader L4)', 'bonus_persen'=>0.10),
		));


	}
}

class BonusSponsorTableSeeder extends Seeder {

	public function run() {

		DB::table('bonus_sponsor')->delete();

		DB::table('bonus_sponsor')->insert(array(

			array('id'=>1, 'uraian'=>'Rendah', 'bp_min'=>1000000, 'bp_max'=>4900000, 'bonus_persen'=>4.00),
			array('id'=>2, 'uraian'=>'Menengah', 'bp_min'=>5000000, 'bp_max'=>7900000, 'bonus_persen'=>7.00),
			array('id'=>3, 'uraian'=>'Tinggi', 'bp_min'=>8000000, 'bp_max'=>10000000, 'bonus_persen'=>10.00),
		
		));
	}

}

class LeadersTableSeeder extends Seeder {

	public function run() {
		DB::table('leaders')->delete();

		DB::table('leaders')->insert(array(

			array('id'=>1, 'leader_nama'=>'Top Leader', 'min_member'=>1000000, 'max_tp_per_week'=>35000000),
			array('id'=>2, 'leader_nama'=>'Prime Leader', 'min_member'=>100000, 'max_tp_per_week'=>30000000),
			array('id'=>3, 'leader_nama'=>'Platinum Leader', 'min_member'=>10000, 'max_tp_per_week'=>25000000),
			array('id'=>4, 'leader_nama'=>'Gold Leader', 'min_member'=>1000, 'max_tp_per_week'=>20000000),
			array('id'=>5, 'leader_nama'=>'Silver Leader', 'min_member'=>100, 'max_tp_per_week'=>15000000),

		));

	}
}



class AnggotaTableSeeder extends Seeder {

    public function run()
    {
    	/* Bersihin semua data rows yang ada di database */
    	
        DB::table('anggota')->delete();
        
        DB::table('dana_recovery')->delete();
        DB::table('recovery_mentah')->delete();
        
        /* Bikin disini data masternya */

        // Contoh aja kali ya, bikin 3 anggota
        $anggota1 = Anggota::create(array(
        	'nama'=>'Boss Utama',
        	'ktp'=>'159357852654',
        	'email'=>'top1@gmail.com',
        	'hp'=>'08090000555',
        	'alamat'=>'Jl. Raya Dusun Purwokerto RT 002/003, Purwokerto, Ngadiluwih',
        	'kota'=>'Kediri',
        	'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s')
    	));

        $anggota2 = Anggota::create(array(
        	'nama'=>'Sumiati',
        	'ktp'=>'1221064502790001',
        	'email'=>'sumiati@gmail.com',
        	'hp'=>'085677765421',
        	'alamat'=>'Dusun Sumbersari RT 03/02, Megaluh',
        	'kota'=>'Jombang',	
    	));

        $anggota3 = Anggota::create(array(
        	'nama'=>'Azulfiar',
        	'ktp'=>'6101012703910001',
        	'email'=>'azulfiar@gmail.com',
        	'hp'=>'085377132560',
        	'alamat'=>'Dusun Sebambang RT 04, Sebayan',
        	'kota'=>'Sambas',	
    	));

    }

}

class AkunTableSeeder extends Seeder {

	public function run() {
		DB::table('akun')->delete();
		DB::table('sejarah_login')->delete();
        DB::table('rekening_anggota')->delete();
        DB::table('bptp')->delete();
        DB::table('bptp_transfer')->delete();
        DB::table('transaksi_bp')->delete();
        DB::table('transaksi_tp')->delete();

		// Asumsi id yang auto-increment dari 1. 
    	// Anggota (yang beneran) udah ada, bikin id-nya masing-masing 2 or 3
    	$akun1 = Akun::create(array(
    		'id_anggota' => '1',
    		'id_login' => uniqid(),
    		'nick_name' => 'Top_1',
    		'kata_sandi' => Hash::make(DEFAULT_PASSWORD),
    		'kiri' => 1,
    		'kanan' => 12,
    		'is_manager' => 1,
    		'pin' => substr('00000' . mt_rand(0, 999999), -6, 6),
    		'pin_kadaluarsa' => date('Y-m-d H:i:s', strtotime('now+24hour')),
    		'is_aktif' => 1,
    		'is_blokir' => 0,
    		'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s')
		));

		$akun2 = Akun::create(array(
    		'id_anggota' => '2',
    		'id_login' => uniqid(),
    		'nick_name' => 'Sumiati1',
    		'kata_sandi' => Hash::make(DEFAULT_PASSWORD),
    		'kiri' => 2,
    		'kanan' => 7,
    		'id_sponsor' => 1,
    		'id_manager' => 1, 
    		'is_manager' => 0,
    		'pin' => mt_rand(100000, 999999),
    		'pin_kadaluarsa' => date('Y-m-d H:i:s',strtotime('now+24hour')),
    		'is_aktif' => 1,
    		'is_blokir' => 0,
		));

		$akun3 = Akun::create(array(
    		'id_anggota' => '3',
    		'id_login' => uniqid(),
    		'nick_name' => 'Azulfiar1',
    		'kata_sandi' => Hash::make(DEFAULT_PASSWORD),
    		'kiri' => 8,
    		'kanan' => 11,
    		'id_sponsor' => 1,
    		'id_manager' => 1, 
    		'is_manager' => 0,
    		'pin' => mt_rand(100000, 999999),
    		'pin_kadaluarsa' => date('Y-m-d H:i:s',strtotime('now+24hour')),
    		'is_aktif' => 1,
    		'is_blokir' => 0,
		));

		$akun4 = Akun::create(array(
    		'id_anggota' => '2',
    		'id_login' => uniqid(),
    		'nick_name' => 'Sumiati2',
    		'kata_sandi' => Hash::make(DEFAULT_PASSWORD),
    		'kiri' => 3,
    		'kanan' => 4,
    		'id_sponsor' => 2,
    		'id_manager' => 1, 
    		'is_manager' => 0,
    		'pin' => mt_rand(100000, 999999),
    		'pin_kadaluarsa' => date('Y-m-d H:i:s',strtotime('now+24hour')),
    		'is_aktif' => 1,
    		'is_blokir' => 0,
		));

		$akun5 = Akun::create(array(
    		'id_anggota' => '1',
    		'id_login' => uniqid(),
    		'nick_name' => 'Sulistyo2',
    		'kata_sandi' => Hash::make(DEFAULT_PASSWORD),
    		'kiri' => 5,
    		'kanan' => 6,
    		'id_sponsor' => 2,
    		'id_manager' => 1, 
    		'is_manager' => 0,
    		'pin' => mt_rand(100000, 999999),
    		'pin_kadaluarsa' => date('Y-m-d H:i:s',strtotime('now+24hour')),
    		'is_aktif' => 1,
    		'is_blokir' => 0,
		));

		$akun6 = Akun::create(array(
    		'id_anggota' => '3',
    		'id_login' => uniqid(),
    		'nick_name' => 'Azulfiar2',
    		'kata_sandi' => Hash::make(DEFAULT_PASSWORD),
    		'kiri' => 9,
    		'kanan' => 10,
    		'id_sponsor' => 3,
    		'id_manager' => 1, 
    		'is_manager' => 0,
    		'pin' => mt_rand(100000, 999999),
    		'pin_kadaluarsa' => date('Y-m-d H:i:s',strtotime('now+24hour')),
    		'is_aktif' => 1,
    		'is_blokir' => 0,
		));

	}
}

class BankTableSeeder extends Seeder {

	public function run() {
		DB::table('bank')->delete();

		/* Bikin semua nama-nama Bank disini */

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mandiri';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Mutiara Bank';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Negara Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Rakyat Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Tabungan Negara';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BRI Agro ';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = ' Bank Anda (Surabaya)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = ' Bank Artha Graha Internasional';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Bukopin';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Bumi Arta';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Capital Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Central Asia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank CIMB Niaga';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Danamon Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Ekonomi Raharja';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Ganesha';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Hana';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Himpunan Saudara 1906 (Bandung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank ICB Bumiputera';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank ICBC Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Index Selindo';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Internasional Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Maspion (Surabaya)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mayapada';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mega';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mestika Dharma (Medan)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Metro Express';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Nusantara Parahyangan (Bandung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank OCBC NISP';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank of India Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Panin';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Permata';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank QNB Kesawan';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank SBI Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sinarmas';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank UOB Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Anglomas Internasional Bank (Surabaya)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Andara';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Artos Indonesia (Bandung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Bisnis Internasional (Bandung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Tabungan Pensiunan Nasional (Bandung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Centratama Nasional Bank (Surabaya)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Dipo International';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Fama Internasional (Bandung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Harda Internasional';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Ina Perdana';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Jasa Jakarta';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Kesejahteraan Ekonomi';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Liman International';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mayora';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mitraniaga';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Multi Arta Sentosa';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Nationalnobu';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Prima Master Bank';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Pundi Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Royal Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sahabat Purba Danarta';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = ' Bank Sinar Harapan Bali ';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Victoria Internasional';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Yudha Bhakti';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Jambi (Jambi)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Kalsel (Banjarmasin)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Kaltim (Samarinda)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sultra (Kendari)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BPD DIY (Yogyakarta)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Nagari (Padang)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank DKI (Jakarta)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Lampung (Bandar Lampung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Kalteng (Palangka Raya)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BPD Aceh (Banda Aceh)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sulsel (Makassar)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BJB (Bandung)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Kalbar (Pontianak)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Maluku (Ambon)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Bengkulu (Kota Bengkulu)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Jateng (Semarang)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Jatim (Surabaya)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank NTB (Mataram)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank NTT (Kupang)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sulteng (Palu)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sulut (Manado)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BPD Bali (Denpasar)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Papua (Jayapura)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Riau Kepri (Pekanbaru)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sumsel Babel (Palembang)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sumut (Medan)';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank ANZ Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Commonwealth';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Agris';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BNP Paribas Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Chinatrust Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank DBS Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank KEB Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mizuho Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Rabobank International Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Resona Perdania';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Sumitomo Mitsui Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Windu Kentjana International';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Woori Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank of America';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bangkok Bank';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank of China';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Citibank';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Deutsche Bank';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'HSBC';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'JPMorgan Chase';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Royal Bank of Scotland';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Standard Chartered';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'The Bank of Tokyo-Mitsubishi UFJ';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BNI Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Mega Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Muamalat Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Syariah Mandiri';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'BCA Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BJB Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BRI Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Panin Bank Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Syariah Bukopin';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Victoria Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Maybank Syariah Indonesia';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank BTN Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Danamon Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'CIMB Niaga Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'BII Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'OCBC NISP Syariah';
		$bank->save();

		$bank = new Bank;
		$bank->nama_bank = 'Bank Permata Syariah';
		$bank->save();

		/* OK semua nama Bank udah masuk yah! */
	}
	
}
