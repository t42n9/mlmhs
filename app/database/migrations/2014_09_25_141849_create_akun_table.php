<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkunTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('akun', function(Blueprint $table)
		{
			$table->bigIncrements('id');

			$table->bigInteger('id_anggota');
			$table->foreign('id_anggota')->references('id')->on('anggota')
					->onDelete('restrict')
					->onUpdate('restrict');
					
			$table->string('id_login', 13)->unique();
			//$table->string('alias', 50)->unique();
			$table->string('nick_name', 30)->unique();
			$table->string('kata_sandi', 64);
			$table->bigInteger('kiri')->unique();
			$table->bigInteger('kanan')->unique();
			$table->bigInteger('id_sponsor')->default(0);
			$table->bigInteger('id_manager')->default(0);
			$table->tinyInteger('is_manager')->default(0);
			$table->integer('level_manager')->default(0);
			//$table->string('pin', 6)->default('123456');
			//$table->timestamp('pin_kadaluarsa')->default(date('Y-m-d H:m:s'));
			$table->string('pin', 6);
			$table->timestamp('pin_kadaluarsa');
			$table->tinyInteger('is_aktif')->default(0);
			$table->tinyInteger('is_blokir')->default(0);
			$table->tinyInteger('is_main')->default(0);

			$table->timestamps();

			$table->index('id_anggota');
			$table->index('id_sponsor');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('akun', function($t) {
			$t->dropForeign('akun_id_anggota_foreign');
			$t->dropColumn('id_anggota');
		});

		Schema::drop('akun');
	}

}
