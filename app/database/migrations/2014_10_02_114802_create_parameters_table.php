<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parameters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('status')->default(0);
			$table->timestamp('tgl_status')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('created_by', 150)->nullable();
			$table->string('updated_by', 150)->nullable();
			$table->integer('max_akun_per_member')->default(10);
			$table->bigInteger('min_bp')->default(1000000);
			$table->bigInteger('max_bp_reguler')->default(10000000);
			$table->bigInteger('max_bp_vip')->default(30000000);
			$table->decimal('growth_1', 10, 2)->default(0.10);
			$table->decimal('growth_2', 10, 2)->default(1.00);
			$table->integer('growth_off')->default(30);
			$table->tinyInteger('is_autotp')->default(1);
			$table->integer('autotp_after')->default(30);
			$table->bigInteger('fix_bp_leader')->default(10000000);
			$table->integer('min_old_to_leader')->default(60);
			$table->bigInteger('min_omzet_to_leader')->default(100000000);
			$table->integer('max_idle_new_account')->default(48);
			$table->integer('max_idle_old_account')->default(60);
			$table->integer('jml_hidden')->default(127);
			$table->bigInteger('nilai_kelipatan_bp')->default(100000);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parameters');
	}

}
