<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiBpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi_bp', function(Blueprint $table)
		{
			$table->string('bp_kd', 13);
			$table->bigInteger('id_akun_bp')
			$table->foreign('id_akun_bp')->references('id')->on('akun')
					->onDelete('restrict')
					->onUpdate('restrict');


			$table->bigInteger('jml_bp');
			$table->timestamp('bp_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->tinyInteger('bp_canceled')->default(0);
			$table->string('bp_cancel_note', 200)->nullable();
			$table->timestamp('bp_cancel_date')->nullable();

			$table->primary(array('bp_kd'));
			$table->index('bp_date');
			$table->index('bp_canceled');
			$table->index('id_akun_bp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaksi_bp', function($t) {
			$t->dropForeign('transaksi_bp_id_akun_bp_foreign');
			$t->dropColumn('id_akun_bp');
		});

		Schema::drop('transaksi_bp');
	}

}
