<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusLeaderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bonus_leader', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('uraian', 50);
			$table->decimal('bonus_persen', 8, 2);
			
			$table->primary( array('id') );

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bonus_leader');
	}

}
