<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecoveryMentahTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recovery_mentah', function(Blueprint $table)
		{
			$table->string('email', 255);
			$table->bigInteger('nilai');
			$table->string('aktifasi', 6);
			$table->primary(array('email'));

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recovery_mentah');
	}

}
