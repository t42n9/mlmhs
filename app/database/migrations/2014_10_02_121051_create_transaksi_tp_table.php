<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi_tp', function(Blueprint $table)
		{
			$table->string('tp_kd', 13);
			$table->bigInteger('id_akun_tp');
			$table->foreign('id_akun_tp')->references('id')->on('akun')
					->onDelete('restrict')
					->onUpdate('restrict');

			$table->bigInteger('jml_tp');
			$table->timestamp('tp_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->tinyInteger('tp_canceled')->default(0);
			$table->string('tp_cancel_note', 200)->nullable();
			$table->timestamp('tp_cancel_date')->nullable();

			$table->primary(array('tp_kd'));
			$table->index('tp_date');
			$table->index('tp_canceled');
			$table->index('id_akun_tp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaksi_tp', function($t) {
			$t->dropForeign('transaksi_tp_id_akun_tp_foreign');
			$t->dropColumn('id_akun_tp');
		});

		Schema::drop('transaksi_tp');
	}

}
