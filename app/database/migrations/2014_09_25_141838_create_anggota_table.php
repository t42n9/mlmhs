<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnggotaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('anggota', function(Blueprint $table)
		{
			$table->bigIncrements('id');

			$table->string('nama', 50);
			$table->string('ktp', 25)->unique();
			$table->string('email', 45)->unique();
			$table->string('hp', 20)->unique();
			$table->string('alamat', 255)->nullable();
			$table->string('kota', 45)->nullable();
			$table->integer('anggota_type')->default(5);
			$table->string('scan_ktp', 255)->nullable();


			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('anggota');
	}

}
