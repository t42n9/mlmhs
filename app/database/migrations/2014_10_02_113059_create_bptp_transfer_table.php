<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBptpTransferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bptp_transfer', function(Blueprint $table)
		{
			$table->string('bptp_kd', 14);
			$table->foreign('bptp_kd')->references('bptp_kd')->

			$table->string('bukti_transfer', 255);
			$table->bigInteger('jml_transfer');
			$table->bigInteger('id_rek_tp');
			$table->timestamp('transfer_date');
			$table->tinyInteger('bptp_confirmed');
			$table->timestamp('bptp_confirm_date');

			$table->primary(array('bptp_kd'));
			$table->index('id_rek_tp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bptp_transfer');
	}

}
