<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusSponsorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bonus_sponsor', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('uraian', 30);
			$table->bigInteger('bp_min');
			$table->bigInteger('bp_max');
			$table->decimal('bonus_persen', 8, 2);

			$table->primary(array('id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bonus_sponsor');
	}

}
