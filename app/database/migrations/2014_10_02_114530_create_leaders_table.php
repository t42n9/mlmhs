<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leaders', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('leader_nama', 20);
			$table->bigInteger('min_member');
			$table->bigInteger('max_tp_per_week');
			$table->timestamps();

			$table->primary(array('id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leaders');
	}

}
