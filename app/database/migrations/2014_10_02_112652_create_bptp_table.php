<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBptpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bptp', function(Blueprint $table)
		{
			$table->string('bptp_kd', 14);
			$table->string('tp_kd', 13);
			$table->foreign('tp_kd')->references('tp_kd')->on('transaksi_tp')
					->onDelete('restrict')
					->onUpdate('restrict');


			$table->string('bp_kd', 13);
			$table->foreign('bp_kd')->references('bp_kd')->on('transaksi_bp')
					->onDelete('restrict')
					->onUpdate('restrict');

			$table->timestamp('bptp_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->bigInteger('jml_bptp');
			$table->tinyInteger('bptp_canceled');
			$table->string('bptp_cancel_note', 200)->nullable();
			$table->timestamp('bptp_cancel_date')->nullable();
			$table->tinyInteger('bp_akad')->default(0);
			$table->tinyInteger('tp_akad')->default(0);
			$table->timestamp('bp_akad_at')->nullable();
			$table->timestamp('tp_akad_at')->nullable();

			$table->primary(array('bptp_kd'));
			$table->unique(array('bp_kd','tp_kd'));

			$table->index('bptp_canceled');
			$table->index('bptp_at');
			$table->index('bp_kd');
			$table->index('tp_kd');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{	
		Schema::table('bptp', function($t) {
			$t->dropForeign('bptp_bp_kd_foreign');
			$t->dropColumn('bp_kd');

			$t->dropForeign('bptp_tp_kd_foreign');
			$t->dropColumn('tp_kd');
		});

		Schema::drop('bptp');
	}

}
