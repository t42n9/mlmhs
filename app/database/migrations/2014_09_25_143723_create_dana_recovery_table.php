<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDanaRecoveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dana_recovery', function(Blueprint $table)
		{
			$table->bigIncrements('id');

			$table->bigInteger('id_akun');
			$table->string('email_asal', 100);
			$table->bigInteger('nilai');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dana_recovery');
	}

}
