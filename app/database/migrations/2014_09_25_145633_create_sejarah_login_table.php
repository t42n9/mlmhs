<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSejarahLoginTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sejarah_login', function(Blueprint $table)
		{
			//$table->bigIncrements('id');
			$table->bigInteger('id_akun');	
			$table->foreign('id_akun')->references('id')->on('akun')
					->onDelete('cascade')
					->onUpdate('cascade');

			$table->timestamp('waktu');
			$table->string('ip_address', 15)->nullable();
			$table->string('host_name', 100)->nullable();
			$table->string('device', 20)->nullable();
			$table->string('browser', 45)->nullable();
			$table->tinyInteger('is_gagal');

			$table->index('id_akun');
			$table->index('waktu');
			$table->index('is_gagal');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('sejarah_login', function($t) {
			$t->dropForeign('sejarah_login_id_akun_foreign');
			$t->dropColumn('id_akun');
		});

		Schema::drop('sejarah_login');
	}

}
