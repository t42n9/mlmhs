<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekeningAnggotaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rekening_anggota', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->bigInteger('id_akun');
			$table->foreign('id_akun')->references('id')->on('akun')
					->onDelete('cascade')
					->onUpdate('cascade');


			$table->integer('id_bank');
			$table->foreign('id_bank')->references('id')->on('bank')
					->onDelete('restrict')
					->onUpdate('restrict');

			$table->string('no_rekening', 45);
			$table->string('nama_bank', 50)->nullable();
			$table->string('atas_nama', 50)->nullable();
			$table->tinyInteger('is_aktif')->default(1);
			$table->unique( array('id_akun', 'id_bank', 'no_rekening') );
			
			$table->index('id_bank');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rekening_anggota', function($t) {
			$t->dropForeign('rekening_anggota_id_akun_foreign');
			$t->dropColumn('id_akun');

			$t->dropForeign('rekening_anggota_id_bank_foreign');
			$t->dropColumn('id_bank');
		});


		Schema::drop('rekening_anggota');
	}

}
