<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(
    array(
        'https',
        'prefix' => LaravelLocalization::setLocale(),
        'before' => 'LaravelLocalizationRoutes'
    ),
    function()
    {   
        Route::get('/', array('as' => 'landingpage', 'before' => 'logged_in', 'uses' => 'FrontController@Index'));
        
        Route::get ('/login', array('as' => 'login', 'before' => 'logged_in', 'uses' => 'FrontController@Login'));
        Route::post ( '/login', array ( 'as' => 'post_login', 'before' => 'logged_in', 'uses' => 'FrontController@postLogin' )  );

        Route::get ('/dashboard', array('as' => 'dashboard', 'before' => 'auth', 'uses' => 'FrontController@dashboardan') );

        Route::get ( 'logout', array( 'as' => 'logout', 'uses' => 'FrontController@getLogout') );

        Route::get('member', array('as' => 'memberlist', 'before' => 'auth', 'uses' => 'member\MembersController@Index'));
        Route::get('member/register', array('as' => 'regform', 'before' => 'auth', 'uses' => 'member\MembersController@RegisterMember'));
        Route::post('member/regpost', array('as' => 'regpost', 'before' => 'auth', 'uses' => 'member\MembersController@postRegisterMember'));

        /* TICKET */
        //Route::get('ticket', array('as' => 'ticketlist', 'before' => 'auth', function(){ return View::make('members.ticketlist'); }));
        Route::get('ticket', array('as' => 'ticketlist', 'before' => 'auth', 'uses' => 'transaksi\TiketController@Index'));
        Route::post('ticket/buy', array('as' => 'buyticket', 'before' => 'auth', 'uses' => 'transaksi\TiketController@BuyTicket'));
        /* END TICKET */

        /* WALLET */
        Route::get('wallet', array('as' => 'wallet', 'before' => 'auth', 'uses' => 'transaksi\WalletController@Index'));
        Route::get('topup', array('as' => 'topup', 'before' => 'auth', 'uses' => 'transaksi\WalletController@HistoryTopup'));
        Route::get('topup/history', array('as' => 'topuphistory', 'before' => 'auth',  'uses' => 'transaksi\WalletController@HistoryTopup'));
        Route::get('topup/add', array('as' => 'topupform', 'before' => 'auth', 'uses' => 'transaksi\WalletController@GetTopUp'));
        Route::post('topup/add', array('as' => 'posttopup', 'before' => 'auth', 'uses' => 'transaksi\WalletController@PostTopUp'));
        Route::post('topup/confirm', array('as' => 'confirmtopup', 'before' => 'auth', 'uses' => 'transaksi\WalletController@ConfirmTopUp'));

        // Page Admin
        Route::get('admin/wallet', array('as' => 'adminwallet', 'before' => 'auth', 'uses' => 'transaksi\WalletController@AdminIndex'));
        Route::post('admin/topup/acc', array('as' => 'acctopup', 'before' => 'auth', 'uses' => 'transaksi\WalletController@TerimaPembayaran'));
        Route::post('admin/topup/reject', array('as' => 'rejecttopup', 'before' => 'auth', 'uses' => 'transaksi\WalletController@TolakPembayaran'));
        /* END WALLET */

        /*  STRUCTURE */
        Route::get('struktur/{key}', array('before' => 'auth', 'uses' => 'member\MembersController@getMyStructure'));
        Route::get('struktur/{key}/{page}', array('before' => 'auth', 'uses' => 'member\MembersController@getMyStructure'));
        Route::get('struktur/{key}/{cbaris}/{cside}', array('before' => 'auth', 'uses' => 'member\MembersController@getTargetBinary'));
        /*  END STRUCTURE */

        /* testing here */
        //Route::get('testboard/{id}', array('as' => 'memberlist', 'uses' => 'member\MembersController@cariOriginalBoardDariDestination'));
        //Route::get('board/{idMember}', array('as' => 'memberboard', 'uses' => 'member\MembersController@getMyBoardView'));
        //Route::get('board_plain/{idMember}', array('as' => 'memberboard', 'uses' => 'member\MembersController@getMyBoard'));
        //Route::get('board_plain/{idMember}/{page}', array('as' => 'memberboard', 'uses' => 'member\MembersController@getMyBoard'));
        //Route::get('test/isfirstpair/{baris}/{idparent}', array('uses' => 'BaseController@testFirstPair'));
        //Route::get('test/binarypairlist/{kode}', array('uses' => 'BaseController@testBinaryPairList'));
        //Route::get('test/obj', array('uses' => 'BaseController@testObj'));
        /* end testing here */
    }
 );




