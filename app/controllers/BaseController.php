<?php

use memberModel\Members AS Members;

class BaseController extends Controller {

    protected $isLogin = false;

    protected $UserLogin;
    //protected $JumlahBoard = 0;

    protected $member;

    public function __construct() {
        $this->checkLogin();

        if ($this->isLogin == true) {
            View::share('MemberCls', $this->member);
            View::share('User', $this->UserLogin);
            //View::share('JumlahBoard', $this->JumlahBoard);
            View::share('CopyRight', 'MLM-HS &copy; 2016');
        }
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

    private function checkLogin() {

        $this->isLogin  = Auth::check();

        if ($this->isLogin == true) {
            $this->member       = new Members;
            $this->UserLogin    = Auth::user();
        }
        
        return;
    }

    //  testing
    /*
    public function testFirstPair($baris, $idparent) {
        $mm     = new Members;
        $test   = $mm->isBinaryFirstPair($baris, $idparent);
        dd($test);
    }

    public function testBinaryPairList($kode) {
        $mm     = new Members;
        $data   = $mm->getBinaryByKode($kode);
        $test   = $mm->getBinaryBonusPairList($data);
        dd($test);
    }
    */

    public function testObj() {
        $test = new stdClass();
        dd(is_object($test));
    }
    //  end testing
}

