<?php

namespace transaksi;

use financialModel\Wallet AS Wallet;

use View;
use Redirect;
use Input;
use Validator;
use Session;
use DB;

class WalletController extends \BaseController {

    private $CompanyBank = array();

    public function __construct() {
        parent::__construct();

        $this->CompanyBank[1] = (object) array('ID' => 1, 'Nama' => 'BCA', 'Rekening' => '0101010101', 'AtasNama' => 'HS Company');
        $this->CompanyBank[2] = (object) array('ID' => 2, 'Nama' => 'Mandiri', 'Rekening' => '0202020202', 'AtasNama' => 'HS Company');
    }

    public function Index() {
        $wallet = new Wallet;

        $dataWallet     = $wallet->getWalletMember($this->UserLogin->id);
        $dataHistory    = $wallet->getWalletHistory($this->UserLogin->id);

        return View::make('financials.wallet')
                        ->with('dataWallet', $dataWallet)
                        ->with('dataHistory', $dataHistory);
    }

    public function HistoryTopup() {
        $wallet         = new Wallet;

        $Data       = $wallet->getWalletHistoryTopUp($this->UserLogin->id);

        $dataAll        = array();
        $dataWaiting    = array();
        $dataProcessing = array();
        $dataComplete   = array();

        if (!empty($Data)) {
            foreach ($Data as $row) {
                $dataAll[$row->id] = $row;
                if ($row->is_confirmed == 0) {
                    $dataWaiting[$row->id] = $row;
                } else {
                    if ($row->status == $wallet->AccStatus) {
                        $dataComplete[$row->id] = $row;
                    } else {
                        $dataProcessing[$row->id] = $row;
                    }
                }
            }
        }

        return View::make('financials.topuphistory')
                        ->with('dataAll', $dataAll)
                        ->with('dataWaiting', $dataWaiting)
                        ->with('dataProcessing', $dataProcessing)
                        ->with('dataComplete', $dataComplete)
                        ->with('CompanyBank', $this->CompanyBank);
    }
    
    public function GetTopUp() {
        return View::make('financials.topupadd');
    }
    
    public function PostTopUp() {
        if ($this->member->isMember($this->UserLogin) == false) {
            Session::flash('msgerror', 'Anda tidak dapat melakukan penambahan saldo.');
            return Redirect::to('wallet');
        }

        $stuju      = Input::get('accept');
        $jumlah1    = intval(Input::get('amount'));     //  angka yg diinput
        $jumlah2    = intval(Input::get('amount2'));    //  angka setelah ditambah 4 digit random, nilai ini yang akan digunakan sebagai saldo
        $jumlah3    = intval(Input::get('amount3'));    //  4 digit random

        if ($stuju != 1) {
            Session::flash('msgerror', 'Anda belum menyetujui ketentuan - ketentuan untuk menambah saldo.');
            return Redirect::to('wallet');
        }

        $isValidValue = (($jumlah1 + $jumlah3 === $jumlah2) && ($jumlah2 > 50000));
        if ($isValidValue == false) {
            Session::flash('msgerror', 'Nilai yang Anda masukkan dan 4 digit angka acak tidak sesuai dengan nilai yang tertera dilangkah terakhir.');
            return Redirect::to('wallet');
        }

        $wallet     = new Wallet;
        $myWallet   = $wallet->getWalletMember($this->UserLogin->id);

        if (empty($myWallet)) {
            Session::flash('msgerror', 'Anda tidak memiliki rekening wallet.');
            return Redirect::to('wallet');
        }

        $nomor  = $wallet->generateNomorTopUp();
        $data   = array('nomor_topup'   => $nomor,
                        'id_member'     => $this->UserLogin->id,
                        'id_wallet'     => $myWallet->id,
                        'tgl_unik'      => date('Y-m-d'),
                        'digit_unik'    => $jumlah3,
                        'jml_topup'     => $jumlah1,
                        'jml_transfer'  => $jumlah2
                        );

        $proses     = $wallet->createTopUp($data);
        if ($proses->Success == true) {
            return Redirect::to('topup')->with('msgsuccess', 'Pemesanan penambahan saldo berhasil, silahkan lakukan transfer dan konfirmasikan transfer anda.');
        } else {
            return Redirect::to('wallet')->with('msgerror', 'Gagal proses penambahan saldo, silahkan ulangi kembali.');
        }
    }

    public function ConfirmTopUp() {
        $topupID    = Input::get('targetid');
        $asalBank   = Input::get('usebank');
        $asalRek    = Input::get('userek');
        $asalNama   = Input::get('usename');
        $tujuanID   = intval(Input::get('bankdest'));

        if ($asalBank == '' || $asalRek == '' || $asalNama == '') {
            Session::flash('msgerror', 'Informasi Bank, Nomor Rekening dan Pemilik Rekening yang digunakan untuk transfer harus diisi.');
            return Redirect::to('topup/history');
        }

        $AdaTujuan  = array_key_exists($tujuanID, $this->CompanyBank);
        
        if ($AdaTujuan == false) {
            Session::flash('msgerror', 'Bank tujuan tidak tersedia.');
            return Redirect::to('topup/history');
        }

        $wallet     = new Wallet;
        $topupData  = $wallet->getTopUpData($topupID);
        if ($wallet->isValidTopUpData($topupData) == false || $wallet->isValidTopUpOwner($topupData, $this->UserLogin->id) == false) {
            Session::flash('msgerror', 'Data tidak benar.');
            return Redirect::to('topup/history');
        }

        $myWallet   = $wallet->getWalletMember($this->UserLogin->id);
        if (empty($myWallet)) {
            Session::flash('msgerror', 'Anda tidak memiliki rekening wallet.');
            return Redirect::to('topup/history');
        }

        $bankTujuan = $this->CompanyBank[$tujuanID];
        $data       = array('transfer_to'       => $bankTujuan->Nama,
                            'transfer_to_rek'   => $bankTujuan->Rekening,
                            'transfer_to_nama'  => $bankTujuan->AtasNama,
                            'use_bank'          => $asalBank,
                            'use_bank_rek'      => $asalRek,
                            'use_bank_nama'     => $asalNama,
                            'is_confirmed'      => 1,
                            'tgl_confirmed'     => date('Y-m-d H:i:s')
                            );

        $sukseConfirm   = $wallet->ConfirmTopUp($topupData->id, $data);
        if ($sukseConfirm == true) {
            Session::flash('msgsuccess', 'Sukses konfirmasi penambahan saldo.');
        } else {
            Session::flash('msgerror', 'Gagal konfirmasi penambahan saldo.');
        }

        return Redirect::to('topup/history');
    }

    /*  Ruang Admin / operator */
    public function AdminIndex() {
        $wallet = new Wallet;

        $wCompany   = $wallet->getWalletKhusus(false);
        $wSimpanan  = $wallet->getWalletKhusus(true);

        $reqTopUp   = $wallet->getRequestTopUp();
        
        return View::make('financials.admin.wallet')
                        ->with('wCompany', $wCompany)
                        ->with('wSimpanan', $wSimpanan)
                        ->with('reqTopUp', $reqTopUp);
    }

    public function TerimaPembayaran() {
        if ($this->member->isAdmin($this->UserLogin) == false) {
            return Redirect::to('logout');
        }

        $topupID    = Input::get('prosesid');

        $wallet     = new Wallet;
        $topupData  = $wallet->getTopUpDataReadyForProcess($topupID);

        if ($wallet->isValidTopUpData($topupData) == false) {
            Session::flash('msgerror', 'Data tidak benar.');
            return Redirect::to('admin/wallet');
        }

        $mWallet    = $wallet->getWalletMember($topupData->id_member);
        if (empty($mWallet)) {
            Session::flash('msgerror', 'Member tersebut tidak memiliki Wallet.');
            return Redirect::to('admin/wallet');
        }

        $jenis      = $wallet->getConstantValue('jnsTopUp');
        $sumber     = $wallet->getSumberByJenis($jenis);

        $uraianMember   = $wallet->getUraianJenis($jenis, '- No : ' . $topupData->nomor_topup);
        $kaliMember     = $wallet->WalletKali($jenis);
        $nilaiMember    = $topupData->jml_transfer * $kaliMember;

        $data   = array('status'        => $wallet->AccStatus,
                        'tgl_status'    => date('Y-m-d H:i:s'),
                        'status_by'     => $this->UserLogin->id,
                        'ket_status'    => 'Diterima, Uang Sudah Masuk.'
                        );

        $pesanError = 'Gagal Proses Penerimaan Penambahan Saldo.';

        DB::beginTransaction();
        $acc    = $wallet->UpdateStatus($topupData->id, $data);
        if ($acc == true) {
            $okTopUp = $wallet->AddHistory($mWallet, $jenis, $nilaiMember, $sumber, $topupData->id, $uraianMember, true);
            if ($okTopUp->Success == false) {
                DB::rollback();
                Session::flash('msgerror', $pesanError);
                return Redirect::to('admin/wallet');
            }

            DB::commit();
            
            Session::flash('msgsuccess', 'Sukses memproses penambahan saldo Member.');
            return Redirect::to('admin/wallet');
        } else {
            DB::rollback();
            Session::flash('msgerror', $pesanError);
            return Redirect::to('admin/wallet');
        }
    }

    public function TolakPembayaran() {
        if ($this->member->isAdmin($this->UserLogin) == false) {
            return Redirect::to('logout');
        }
        
        $topupID    = Input::get('rejectid');
        $alasan     = Input::get('alasan');

        if ($alasan == '') {
            Session::flash('msgerror', 'Alasan harus diisi.');
            return Redirect::to('admin/wallet');
        }

        $wallet     = new Wallet;
        $topupData  = $wallet->getTopUpDataReadyForProcess($topupID);

        if ($wallet->isValidTopUpData($topupData) == false) {
            Session::flash('msgerror', 'Data tidak benar.');
            return Redirect::to('admin/wallet');
        }

        $data   = array('status'        => $wallet->RejectStatus,
                        'tgl_status'    => date('Y-m-d H:i:s'),
                        'status_by'     => $this->UserLogin->id,
                        'ket_status'    => $alasan
                        );

        $pesanError = 'Gagal Proses Penolakan Penambahan Saldo.';
        $okReject   = $wallet->UpdateStatus($topupData->id, $data);
        if ($okReject == true) {
            Session::flash('msgsuccess', 'Sukses menolak penambahan saldo Member.');
        } else {
            Session::flash('msgerror', $pesanError);
        }

        return Redirect::to('admin/wallet');
    }
    /*  End Ruang Admin / operator */
}

