<?php

namespace transaksi;

use financialModel\Ticket AS Tiket;

use View;
use Redirect;
use Validator;
use Session;
use DB;

class TiketController extends \BaseController {

    public function __construct() {
        parent::__construct();
    }

    public function Index() {
        $myTiket    = new Tiket;
        
        $Data       = $myTiket->getTicketList($this->UserLogin->id);

        $dataAll    = array();
        $dataUsed   = array();
        $dataUnUsed = array();

        if (!empty($Data)) {
            foreach ($Data as $row) {
                $dataAll[$row->id] = $row;
                if ($row->is_used == 1) {
                    $dataUsed[$row->id] = $row;
                } else {
                    $dataUnUsed[$row->id] = $row;
                }
            }
        }

        $Harga  = $myTiket->getHargaTiket();

        return View::make('financials.ticketlist')
                        ->with('dataAll', $dataAll)
                        ->with('dataUsed', $dataUsed)
                        ->with('dataUnUsed', $dataUnUsed)
                        ->with('Harga', $Harga);
    }

    private function FailProses($pesan, $isRollback = true) {
        if ($isRollback == true) DB::rollback();
        Session::flash('msgerror', $pesan);
        return Redirect::to('ticket');
    }

    public function BuyTicket() {
        if ($this->member->isMember($this->UserLogin) == false) {
            Session::flash('msgerror', 'Anda tidak memiliki izin untuk membeli tiket.');
            return Redirect::to('ticket');
        }

        $Wallet     = new \financialModel\Wallet;

        $myWallet   = $Wallet->getWalletMember($this->UserLogin->id);
        if (empty($myWallet)) {
            Session::flash('msgerror', 'Anda tidak memiliki Wallet.');
            return Redirect::to('ticket');
        }

        $myTiket    = new Tiket;

        $Harga      = $Wallet->getConstantValue('HargaTicket');

        if ($myWallet->saldo < $Harga) {
            Session::flash('msgerror', 'Saldo Wallet Anda tidak cukup untuk membeli tiket.');
            return Redirect::to('ticket');
        }

        $nomorTiket = $myTiket->generateNomorTiket();

        $data       = array('id_member'     => $this->UserLogin->id, 
                            'harga_tiket'   => $Harga,
                            'tiket_nomor'   => $nomorTiket);

        $WCompany   = $Wallet->getWalletKhusus(false);

        $jenis      = $Wallet->getConstantValue('jnsBeliTiket');
        $sumber     = $Wallet->getSumberByJenis($jenis);

        $uraianMember   = $Wallet->getUraianJenis($jenis, '- No.Tiket ' . $nomorTiket);
        $kaliMember     = $Wallet->WalletKali($jenis);
        $nilaiMember    = $Harga * $kaliMember;

        $uraianKhusus1  = $Wallet->getUraianJenisKhusus($jenis, 1, '- No.Tiket ' . $nomorTiket);
        $kaliKhusus1    = $Wallet->WalletKaliKhusus($jenis, 1);
        $nilaiKhusus1   = $Harga * $kaliKhusus1;

        $pesanError     = 'Gagal membeli Tiket.';

        DB::beginTransaction();

        $okTiket    = $myTiket->AddTicket($data);
        if ($okTiket->Success == true) {
            $idTiket    = $okTiket->Data->id;

            $okWalletMember = $Wallet->AddHistory($myWallet, $jenis, $nilaiMember, $sumber, $idTiket, $uraianMember, true);
            if ($okWalletMember->Success == false) return $this->FailProses($pesanError, true);

            $okCompany  = $Wallet->AddHistoryKhusus($WCompany, $jenis, $nilaiKhusus1, $sumber, $idTiket, $uraianKhusus1, true);
            if ($okCompany->Success == false) return $this->FailProses($pesanError, true);

            DB::commit();
            
            Session::flash('msgsuccess', 'Sukses membeli Tiket.');
            return Redirect::to('ticket');
        } else {
            return $this->FailProses($pesanError, true);
        }
    }
}

