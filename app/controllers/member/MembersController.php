<?php

namespace member;

use memberModel\Members AS Members;

//use financialModel\WalletSettings AS WalletSettings;

use View;
use Input;
use Session;
use Redirect;
use Validator;
use Hash;
use DB;

class MembersController extends \BaseController {

    private $minNama = 2;
    private $maxNama = 50;

    public function __construct() {
        parent::__construct();
    }

    public function Index() {

        if ($this->UserLogin->member_type == 1) {
            $Data   = $this->member->getMembers();
        } else {
            $Data   = $this->member->getMyMembers($this->UserLogin->id);
        }

        return View::make('members.list')->with('Data', $Data);
    }

    public function RegisterMember() {
        $idMember   = $this->UserLogin->id;

        $myBinary   = $this->member->getMyBinary($this->UserLogin->id);
        $kode       = 'x';
        $mybaris    = 0;
        $myside     = 0;
        $baris      = 0;
        $side       = 0;
        if (!empty($myBinary)) {
            $kode       = $myBinary->kode;
            $mybaris    = $myBinary->baris_urut;
            $myside     = $myBinary->side_urut;
            $baris      = $myBinary->baris_urut;
            $side       = $myBinary->side_urut;
        }
        $test   = $this->member->getBinaryStructure3($kode, $mybaris, $myside, $baris, $side);
        $Binary = null;
        if (!empty($test)) {
            $Binary = $test->Data;
        }

        return View::make('members.add')
                        ->with('Binary', $Binary);
    }

    private function validateRegister($data) {
        $Rules  = array(    'nama'      => 'required|min:' . $this->minNama . '|max:' . $this->maxNama,
                            'email'     => 'required|email|unique:members,email',
                            'password'  => 'required|min:6',
                        );
        $valid  = Validator::make($data, $Rules);
        return $valid;
    }

    private function getVariableBinary($bmode, $baris, $side, $isFirst = false) {
        $result = (object) array('Exists'       => false,
                                'Baris'         => 0, 
                                'Side'          => 0, 
                                'Kaki'          => 0, 
                                'ParentID'      => 0, 
                                'ParentKode'    => '', 
                                'ParentBaris'   => 0, 
                                'ParentSide'    => 0, 
                                'ParentKaki'    => 0);
        if ($isFirst == true) {
            $result->ParentID       = 0;
            $result->ParentKode     = '';
            $result->ParentBaris    = 0;
            $result->ParentSide     = 1;
            $result->ParentKaki     = 1;

            $result->Baris          = 1;
            $result->Side           = 1;
            $result->Kaki           = 1;

            $result->Exists         = true;
        } else {
            if ($bmode == 3) {
                $pBaris                 = $baris - 1;
                $pSide                  = CEIL($side / 2);

                $parent                 = $this->member->getBinaryDataBySide($pBaris, $pSide);

                if (!empty($parent)) {
                    $result->ParentID       = $parent->id;
                    $result->ParentKode     = $parent->kode;
                    $result->ParentBaris    = $parent->baris_urut;
                    $result->ParentSide     = $parent->side_urut;
                    $result->ParentKaki     = $parent->kaki_urut;

                    $result->Baris          = $baris;
                    $result->Side           = $side;
                    $result->Kaki           = ($side % 2 > 0) ? 1 : 2;

                    $result->Exists         = true;
                }
            } else {

                $parent                 = $this->member->getBinaryDataBySide($baris, $side);

                if (!empty($parent)) {
                    $spaceBinary            = $this->member->getBinaryAutoSpace($parent, $bmode);

                    //$result->Side           = $this->member->getBinarySideByParent($spaceBinary->SideNo, $spaceBinary->UsedSide, $bmode);
                    $result->Baris          = $spaceBinary->ChildBaris;
                    $result->Side           = $spaceBinary->ChildSide;
                    //$result->Kaki           = ($result->Side % 2 > 0) ? 1 : 2;
                    $result->Kaki           = $spaceBinary->ChildKaki;

                    $result->ParentID       = $spaceBinary->ID;
                    $result->ParentKode     = $spaceBinary->Kode;
                    $result->ParentBaris    = $spaceBinary->Baris;
                    $result->ParentSide     = $spaceBinary->SideNo;
                    $result->ParentKaki     = $spaceBinary->Kaki;

                    $result->Exists         = $spaceBinary->Exists;
                }
            }
        }

        return $result;
    }

    private function FailProses($pesan, $isRollback = true) {
        if ($isRollback == true) DB::rollback();
        Session::flash('msgerror', $pesan);
        return Redirect::back()->withInput();
    }

    public function postRegisterMember() {
        $nama   = Input::get('nama');
        $email  = Input::get ('email');
        $bmode  = intval(Input::get('bmode'));
        $baris  = intval(Input::get('selbaris'));
        $side   = intval(Input::get('selside'));

        $pesanError     = 'Gagal Register Member baru.';

        // Pake Default Password dulu sementara
        $password = '123456';

        $dataSP = $this->UserLogin;

        $idSponsor  = 0;

        $isAdmin    = $this->member->isAdmin($dataSP);
        $isMember   = $this->member->isMember($dataSP);

        if (!empty($dataSP)) {
            if ($isAdmin == true) {
                $idSponsor   = -1;     //  diremark kalo udh ada data bertipe member
            } elseif ($isMember == true) {
                $idSponsor   = $dataSP->id;
            }
        } else {    // jika tidak ada user
            return Redirect::to('logout');
        }

        $erroran    = array();
            
        if ($idSponsor == 0) {
            Session::flash('msgerror', 'Gagal Register Member baru (Invalid Sponsor)');
            return Redirect::back()->withInput();
        }

        //  kalo member harus punya tiket
        $Tiket      = new \financialModel\Ticket;

        $myTiket    = null;
        if ($isMember == true) {
            $myTiket    = $Tiket->getFreeTicket($idSponsor);
            if (empty($myTiket)) {
                Session::flash('msgerror', 'Gagal Register Member baru. Anda tidak memiliki Tiket untuk mendaftarkan member baru.');
                return Redirect::back()->withInput();
            }
        }

        //  berikut khusus untuk start member
        if ($idSponsor == -1) {
            $bmode  = 1;
            $baris  = 1;
            $side   = 1;
        }

        if ($bmode < 1 || $bmode > 3) {
            $erroran['posisi'] = 'Pilihan Posisi Struktur tidak tersedia.';
        }

        $Data   = array('kode'          => uniqid(), 
                        'email'         => $email, 
                        'password'      => $password, 
                        'member_type'   => 6,
                        'nama'          => $nama, 
                        'id_sponsor'    => $idSponsor, 
                        'status'        => 1, 
                        'status_at'     => date('Y-m-d H:i:s'));

        $valid  = $this->validateRegister($Data);

        if ($valid->fails()) {
            $msg        = $valid->messages();
            if ($msg->has('nama')) {
                $erroran['nama'] = 'Nama harus diisi dengan jumlah karakter (' . $this->minNama . ' - ' . $this->maxNama . ')';
            }
            if ($msg->has('email')) {
                $erroran['email'] = 'Email harus diisi.';
            }

        } else {
            $Data['password'] = Hash::make ( $Data['password'] );
        }

        if (!empty($erroran)) {
            Session::flash('msgerror', 'Gagal Register Member baru.');
            Session::flash('errorData', $erroran);
            return Redirect::back()->withInput();
        }

        //  persiapan variable sebelum proses register, struktur board setelah semua proses berhasil
        //  a.  struktur binary
        $varBinary      = $this->getVariableBinary($bmode, $baris, $side, ($idSponsor == -1));

        if ($varBinary->Exists == false) {
            Session::flash('msgerror', 'Gagal Register Member baru (Invalid Structure)');
            return Redirect::back()->withInput();
        }

        //  b.  struktur matrix
        $spaceMatrix    = $this->member->getMatrixSpace($idSponsor);

        //  c.  wallet dan history-nya
        $Wallet = new \financialModel\Wallet;
        //  ambil wallet perusahaan
        $WCompany   = $Wallet->getWalletKhusus(false);
        //  ambil wallet simpanan
        $WSimpanan  = $Wallet->getWalletKhusus(true);

        //  ambil walet sponsor / yg mendaftarkan
        $WSponsor   = null;
        if ($isMember == true) {
            $WSponsor   = $Wallet->getWalletMember($dataSP->id);
        }

        //  1.  pembagian uang masuk
        $HargaDaftar    = $Wallet->getConstantValue('HargaDaftar');
        $HargaCompany   = $Wallet->getConstantValue('BiayaDaftarForCompany');
        $SaldoAwal      = $Wallet->getConstantValue('SaldoAwalMember');
        $JmlSimpanan    = $HargaDaftar - $HargaCompany - $SaldoAwal;

        //  2.  jenis transaksi untuk member yg didaftarkan, perusahaan dan simpanan
        $jenis          = $Wallet->getConstantValue('jnsSaldoAwal');
        $sumber         = $Wallet->getSumberByJenis($jenis);

        //  3.  nilai tetap untuk member
        //$uraianMember   = $Wallet->getUraianJenis($jenis);
        $kaliMember     = $Wallet->WalletKali($jenis);
        $nilaiMember    = $SaldoAwal * $kaliMember;

        //  4.  nilai tetap untuk perusahaan
        $uraianKhusus1  = $Wallet->getUraianJenisKhusus($jenis, 1, '- ' . $Data['nama']);
        $kaliKhusus1    = $Wallet->WalletKaliKhusus($jenis, 1);
        $nilaiKhusus1   = $HargaCompany * $kaliKhusus1;

        //  5.  nilai tetap untuk simpanan
        $uraianKhusus2  = $Wallet->getUraianJenisKhusus($jenis, 2, '- ' . $Data['nama']);
        $kaliKhusus2    = $Wallet->WalletKaliKhusus($jenis, 2);
        $nilaiKhusus2   = $JmlSimpanan * $kaliKhusus2;

        //  6.  nilai tetap untuk sponsor
        $sponsorJenis   = $Wallet->getConstantValue('jnsTarikSaldoDaftar');
        $sponsorUraian  = $Wallet->getUraianJenis($sponsorJenis, '- ' . $Data['nama']);
        $sponsorKali    = $Wallet->WalletKali($sponsorJenis);
        $sponsorNilai   = $HargaDaftar * $sponsorKali;
        $sponsorSumber  = $Wallet->getSumberByJenis($sponsorJenis);

        //  7.  class bonus
        $Bonus  = new \financialModel\Bonus;

        //  end persiapan

        //  cek wallet sponsor jika yg daftarin bukan admin. admin hanya untuk pertama kali saja
        if ($isMember == true) {
            if (empty($WSponsor) || $WSponsor->saldo < $HargaDaftar) {
                Session::flash('msgerror', 'Gagal Register Member baru. Anda tidak memiliki saldo yang cukup.');
                return Redirect::back()->withInput();
            }
        }

        //  mulai proses
        //  ambil sponsor board
        $sponsorBoard   = $this->member->getSponsorBoardForRegister($idSponsor);

        //  variable untuk proses bonus dari registrasi. apa saja yg dpt bonus dari ini
        $Bonusan    = (object) array('Binary' => array(), 'Matrix' => array(), 'Board1' => array(), 'Board2' => array());

        //  daftarkan member sudah berikut wallet barunya
        DB::beginTransaction();
        //$okDaftar = $this->member->addMember2($Data, $Wallet, $nilaiMember);
        $okDaftar   = $this->member->addMember2($Data);
        if ($okDaftar->Success == true) {
            $memberData = $okDaftar->Data;
            $idMember   = $memberData->id;

            $okWallet   = $Wallet->CreateWalletMember($idMember, $nilaiMember);

            if ($okWallet->Success == false)  return $this->FailProses($pesanError, true);

            //$WMember    = $okDaftar->WalletData;
            $WMember    = $okWallet->Data;
            
            //  Update wallet
            //  1.  Wallet perusahaan
            $okCompany  = $Wallet->AddHistoryKhusus($WCompany, $jenis, $nilaiKhusus1, $sumber, $idMember, $uraianKhusus1, true);
            if ($okCompany->Success == false) return $this->FailProses($pesanError, true);

            //  2.  Wallet simpanan
            $okSimpanan = $Wallet->AddHistoryKhusus($WSimpanan, $jenis, $nilaiKhusus2, $sumber, $idMember, $uraianKhusus2, true);
            if ($okSimpanan->Success == false) return $this->FailProses($pesanError, true);

            if ($isMember == true) {
                $okSponsor = $Wallet->AddHistory($WSponsor, $sponsorJenis, $sponsorNilai, $sponsorSumber, $idMember, $sponsorUraian, true);
                if ($okSponsor->Success == false) return $this->FailProses($pesanError, true);

                $okTiket    = $Tiket->UseTicket($myTiket, $idMember);
                if ($okTiket == false) return $this->FailProses($pesanError, true);
            }
            //  end wallet

            //  struktur
            //  binary
            $Binary = $this->member->AddBinaryMember($idMember, $varBinary);
            if ($Binary->Success == false) return $this->FailProses($pesanError, true);

            //  matrix
            $Matrix = $this->member->AddMatrixMember($spaceMatrix, $idMember);
            if ($Matrix->Success == false) return $this->FailProses($pesanError, true);

            //  board
            $isNewBoard     = false;
            if (empty($sponsorBoard)) {
                if ($idSponsor == -1) {
                    $newBoard       = $this->member->createNewBoard(0, 1);
                    
                    if ($newBoard->Success == false) return $this->FailProses($pesanError, true);
                    
                    $sponsorBoard   = $newBoard->Data;
                    $isNewBoard     = true;
                } else {
                    return $this->FailProses($pesanError, true);
                }
            }

            $parentBoard    = $this->member->getParentBoard($sponsorBoard, $isNewBoard);

            $board          = $this->member->RegisterMemberToBoard($idMember, $sponsorBoard, $parentBoard, 0, true);
            if ($board->Success == false) return $this->FailProses($pesanError, true);
            $BonusExpand    = null;
            if ($board->Penuh == true) {
                $closedBoard    = $board->Board;
                $Sukses         = ($board->Penuh == false) ? true : $this->member->ExpandBoard($closedBoard, $BonusExpand);

                if ($Sukses == false) return $this->FailProses($pesanError, true);
            }
            //  end struktur

            //  bonus, klo bisa bonus diproses jika yang mendaftarkan adalah member (bukan admin). itu pun klo stuju
            //  bonus sponsor
            if ($isMember == true) {
                $okSponsor = $Bonus->ProsesBonusSponsor($Wallet, $memberData, $WSponsor, $WSimpanan);
                if ($okSponsor == false) return $this->FailProses($pesanError, true);
            }
            
            //  bonus pair binary
            $listBonusBinary    = $Binary->ListBonusPair;
            $okBonusBinary      = $Bonus->ProsesBonusBinary($Wallet, $Binary->Data, $memberData, $listBonusBinary, $WSimpanan);
            if ($okBonusBinary == false) return $this->FailProses($pesanError, true);

            //  bonus matrix
            $listMatrix     = $Matrix->BonusList;
            $okMatrix       = $Bonus->ProsesBonusMatrix($Wallet, $Matrix->Data, $memberData, $listMatrix, $WSimpanan);
            if ($okMatrix == false) return $this->FailProses($pesanError, true);

            //  bonus board
            $boardData      = $board->Board;
            $waitingList    = $board->BonusList;
            $okBoard        = $Bonus->ProsesBonusBoard($Wallet, $memberData, $boardData, $waitingList, $BonusExpand, $WSimpanan, $WCompany);
            if ($okBoard == false) return $this->FailProses($pesanError, true);
            //  end bonus

            DB::commit();

            Session::flash('msgsuccess', 'Sukses Register Member baru');
            return Redirect::to('member');
        } else {
            return $this->FailProses($pesanError, true);
        }
    }

    //  menampilkan board per member per history board. board pertama berarti page = 0 atau page = 1, usahakan defaultnya adalah last board
    private function getMemberBoardStructure($memberCls, $idMember, $jmlBoard, $page = 1) {
        if ($jmlBoard > 0) {
            $page   = intval($page);
            $dif    = $jmlBoard - $page;
            $page   = $jmlBoard - $dif - 1;
            $page   = ($page < 0) ? 0 : $page;
        } else {
            $page   = 0;
        }
        return $memberCls->getBoardStructureByMember($idMember, $jmlBoard, $page);
    }

    public function getMyStructure($key, $page = 1) {
        $Data       = null;
        $idMember   = (!empty($this->UserLogin)) ? $this->UserLogin->id : 1;

        $key    = strtoupper($key);

        $sTitle = 'Invalid Data';

        if ($key === 'BOARD') {
            $mInfo  = $this->member->getCountBoardByMember($idMember);    //  jumlah history board per member, last board = jmlboard - 1
            //$Data   = $this->getMemberBoardStructure($this->member, $mInfo->idMember, $mInfo->Jumlah, $page);
            $Data   = $this->getMemberBoardStructure($this->member, $mInfo->idMember, $mInfo->Jumlah, $mInfo->Jumlah);
            $sTitle = 'Struktur Umum';
        } elseif ($key === 'BINARY') {
            $Binary     = $this->member->getMyBinary($this->UserLogin->id);
            $kode       = 'x';
            $mybaris    = 0;
            $myside     = 0;
            $baris      = 0;
            $side       = 0;
            if (!empty($Binary)) {
                $kode   = $Binary->kode;
                $mybaris    = $Binary->baris_urut;
                $myside     = $Binary->side_urut;
                $baris      = $Binary->baris_urut;
                $side       = $Binary->side_urut;
            }
            $Data   = $this->member->getBinaryStructure3($kode, $mybaris, $myside, $baris, $side);
            $sTitle = 'Struktur Binary';
        } elseif ($key === 'MATRIX') {
            $Data   = $this->member->getMatrixStructure($idMember);
            $sTitle = 'Struktur Matrix';
        }

        return View::make('members.structure')
                        ->with('key', $key)
                        ->with('siTitle', $sTitle)
                        ->with('Data', $Data);
    }

    public function getTargetBinary($key, $cbaris, $cside) {
        $Binary     = $this->member->getMyBinary($this->UserLogin->id);
        $kode       = 'x';
        $mybaris    = 0;
        $myside     = 0;
        $baris      = 0;
        $side       = 0;
        if (!empty($Binary)) {
            $kode       = $Binary->kode;
            $mybaris    = $Binary->baris_urut;
            $myside     = $Binary->side_urut;
            $baris      = $cbaris;
            $side       = $cside;
        }
        $Data   = $this->member->getBinaryStructure3($kode, $mybaris, $myside, $baris, $side);
        $sTitle = 'Struktur Binary';
        $key    = 'BINARY';

        return View::make('members.structure')
                        ->with('key', $key)
                        ->with('siTitle', $sTitle)
                        ->with('Data', $Data);
    }

    /*  kumpulan test */
    public function cariOriginalBoardDariDestination($id = 1) {
        $test = $this->member->getSponsorBoardForRegister($id);
        echo("<pre>");
        dd($test);
    }
    /*  end kumpulan test */
}

