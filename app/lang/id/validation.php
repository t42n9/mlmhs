<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

        "recaptcha"            => 'The :attribute field is not correct.',
	"accepted"             => "The :attribute must be accepted.",
	"active_url"           => "The :attribute is not a valid URL.",
	"after"                => "The :attribute must be a date after :date.",
	"alpha"                => "The :attribute may only contain letters.",
	"alpha_dash"           => "The :attribute may only contain letters, numbers, and dashes.",
	"alpha_num"            => ":attribute hanya dapat berisi huruf dan angka.",
	"array"                => "The :attribute must be an array.",
	"before"               => "The :attribute must be a date before :date.",
	"between"              => array(
		"numeric" => "The :attribute must be between :min and :max.",
		"file"    => "The :attribute must be between :min and :max kilobytes.",
		"string"  => "The :attribute must be between :min and :max characters.",
		"array"   => "The :attribute must have between :min and :max items.",
	),
	"boolean"              => "The :attribute field must be true or false",
	"confirmed"            => "The :attribute confirmation does not match.",
	"date"                 => "The :attribute is not a valid date.",
	"date_format"          => "The :attribute does not match the format :format.",
	"different"            => "The :attribute and :other must be different.",
	"digits"               => "The :attribute must be :digits digits.",
	"digits_between"       => "The :attribute must be between :min and :max digits.",
	"email"                => "The :attribute must be a valid email address.",
	"exists"               => "The selected :attribute is invalid.",
	"image"                => "The :attribute must be an image.",
	"in"                   => "The selected :attribute is invalid.",
	"integer"              => "The :attribute must be an integer.",
	"ip"                   => "The :attribute must be a valid IP address.",
	"max"                  => array(
		"numeric" => "The :attribute may not be greater than :max.",
		"file"    => "The :attribute may not be greater than :max kilobytes.",
		"string"  => "The :attribute may not be greater than :max characters.",
		"array"   => "The :attribute may not have more than :max items.",
	),
	"mimes"                => "The :attribute must be a file of type: :values.",
	"min"                  => array(
		"numeric" => "The :attribute must be at least :min.",
		"file"    => "The :attribute must be at least :min kilobytes.",
		"string"  => "The :attribute must be at least :min characters.",
		"array"   => "The :attribute must have at least :min items.",
	),
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => "The :attribute field is required.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "The :attribute field is required when :values is present.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "The :attribute field is required when :values is not present.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "The :attribute and :other must match.",
	"size"                 => array(
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	),
	"unique"               => "The :attribute has already been taken.",
	"url"                  => "The :attribute format is invalid.",
	"timezone"             => "The :attribute must be a valid zone.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
	    'nama' => array(
    	    'required' => 'Nama harus diisi!',
            'min' => 'Nama minimal :min karakter!',
            'max' => 'Nama maksimal :max karakter!',
            'regex' => 'Nama hanya bisa diisi karakter huruf, titik, dan spasi!',
    	),
	    'ktp' => array(
            'required' => 'No. Identitas harus diisi.',
            'max' => 'No Identitas maksimal 25 karakter untuk KTP dan Kartu Keluarga, 15 untuk Paspor.',
            'regex' => 'No. Identitas hanya dapat diisi angka untuk KTP dan KK (kecuali #QQ pada KK jika ada), huruf dengan angka untuk Passport. Hiraukan tanda strip atau titik.',
            'unique' => 'No. Identitas sudah terdaftar untuk member lain.', 
            'min' => 'No. Identitas minimal berisi :min karakter'
    	),
	    'email' => array(
    	    'required' => 'Email harus diisi!',
            'email' => 'Format email tidak valid!',
            'max' => 'Email maksimal :max karakter!',
            'unique' => 'Email sudah digunakan oleh member lain!',
    	),
	    'hp' => array(
    	    'required' => 'Nomor HP harus diisi!',
            'min' => 'Nomor HP minimal :min karakter!',
            'max' => 'Nomor HP maksimal :max karakter!',
            'regex' => 'Nomor HP hanya dapat diisi angka!',
            'unique' => 'Nomor HP sudah digunakan oleh member lain!',
    	),
	    'alamat' => array(
    	    'required' => 'Alamat harus diisi!',
            'max' => 'Alamat maksimal :max karakter!',
            'regex'=>'Format alamat tidak valid. Karakter seperti "\'<>~!@#$%^&* tidak diperbolehkan!',
    	),
	    'kota' => array(
	     	'required' => 'Kota harus diisi!',
            'max' => 'Kota maksimal :max karakter!',
            'regex' => 'Kota hanya dapat diisi karakter huruf dan spasi!',
    	),
	    'kata_sandi' => array(
    	    'required' => 'Kata Sandi tidak boleh kosong!',
    	    'regex' => 'Format kata sandi tidak sesuai! Kata sandi harus mengandung minimal 1 huruf kapital, 1 huruf kecil, dan 1 angka serta memiliki 6 karakter!',
    	),
	    'nick_name' => array(
    	   'required' => 'Nama Akun harus diisi!',
        	'regex' => 'Nama Akun hanya bisa diisi huruf, angka, titik, dan underscore!',
        	'min' => 'Nama Akun minimal :min karakter!',
        	'max' => 'Nama Akun maksimal :max karakter!',
        	'unique' => 'Nama Akun sudah digunakan oleh member lain!',
    	),
	    'password_confirmation' => array(
    	    'required' => 'Konfirmasi Kata Sandi tidak boleh kosong!',
    	),
    	'password' => array(
    	    'required' => 'Kata Sandi tidak boleh kosong!',
    	    'alpha_num' => 'Kata Sandi hanya boleh mengandung Huruf dan Angka!',
    	    'min' => 'Kata Sandi minimal 6 karakter!',
    	    'max' => 'Kata Sandi maksimal 20 karakter!',
    	    'regex' => 'Format kata sandi tidak sesuai! Kata sandi harus memiliki setidaknya satu huruf kecil, satu huruf kapital, <br> dan satu angka serta tanpa karakter spasi dan yang lainnya!',
    	),

    	'id_negara' => array(
    		'required' 	=> 'Negara tidak boleh kosong!',
    		'numeric'	=> 'Negara harus dipilih!',
    	),

    	'pin' => array(
    		'required'  => 'Pin harus diisi!',
	        'regex'     => 'Pin hanya dapat diisi dengan angka!',
	        'min'       => 'Pin minimal harus diisi 6 karakter!',
	        'max'       => 'Pin maksimal harus diisi 6 karakter!'
    	),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
