@section('menu')
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div class="menu_section">
      <h3>Menu</h3>
      <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i> Member <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu" style="display: none">
            <li><a href="/member">Daftar Member</a>
            </li>
            <li><a href="/ticket">Tiket</a>
            </li>
            @if ($MemberCls->isMember($User) == true || $MemberCls->isAdmin($User) == true)
                <li>
                    @if ($MemberCls->isMember($User) == true)
                        <a href="/wallet">Wallet</a>
                    @elseif ($MemberCls->isAdmin($User) == true)
                        <a href="/admin/wallet">Wallet</a>
                    @endif
                </li>
            @endif
          </ul>
        </li>
        <li><a><i class="fa fa-edit"></i> Struktur <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu" style="display: none">
            <? if (!isset($JumlahBoard)){$JumlahBoard = 1;} ?>
            <li><a href="/struktur/board/{{ $JumlahBoard }}">Umum</a>
            </li>
            <li><a href="/struktur/binary">Binary</a>
            </li>
            <li><a href="/struktur/matrix">Matrix</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>

</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
      <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
      <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout" href="/logout">
      <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
    <? if (!isset($CopyRight)) { $CopyRight = 'MLM-HS &copy; '.date('Y'); } ?>
    <div align="center">{{ $CopyRight }}</div>
</div>
<!-- /menu footer buttons -->
@stop