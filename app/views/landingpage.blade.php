<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>MLMHS - Grow your business</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsive HTML5 Bootstrap Portfolio and Business Template">
    <meta name="keywords" content="Responsive, HTML5, Business, Portfolio, Corporate, jQuery Plugins, CSS3, Multipurpose, Corporate, creative, one-page, bootstrap3, owl carousel">

    <meta name="author" content="Coral-Themes">
    
    <!-- make sure to put your favicon.ico in your root directory of your website -->
    <!--<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">-->
    <!-- Latest compiled and minified CSS -->
	
	{{ HTML::style('assets/front/css/bootstrap.min.css') }}
	{{ HTML::style('assets/front/css/styles.css') }}

    @if (App::environment() != 'local')
    <!-- the font used in the template -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    @endif

    {{ HTML::script('assets/front/js/modernizr-2.5.3.min.js') }}
    
    @if (App::environment() != 'local')
          <!-- GOOGLE MAP -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATLU4U4BsP8mUI5gjCSAwYum2Nc3JaCi8&amp;dsensor=false"></script>
    @endif

</head>

<body>
    <!-- page loader -->
  <div id="loader-container">
      <div class="clear-loading loading-effect-3">
	      <div><span></span></div>
      </div>
  </div>
 
<!-- HEADER SECTION -->

<header id="section-header" class="navbar-fixed-top transition page-header page-header-semi-dark">
  <section class="container">
    <div class="navbar-header">
      <!-- LOGO HERE -->
	<a href="#">
	  MLMHS
	</a>
	
	
	<span id="openMenuMobile" class="menu-icon icon-left" data-toggle="collapse" data-target=".navHeaderCollapse">
	      <span class="icon icon-arrows-hamburger1 transition"></span>
	</span>
   </div>

  
  <!-- Collect the nav links for toggling -->
    <nav id="menuLinks" class="collapse navbar-collapse navHeaderCollapse one-page-menu-2">
      <div class="body-overlay"></div>
      <ul class="nav navbar-nav navbar-left transition">
        <li>
	  <a href="#" class="link link-btn menu-link">
	    home
	  </a>
	  
	</li>
	
	<li>
	  <a href="#about" class="link link-btn menu-link">
	    about
	  </a>
	  
	</li>
	
	
	<li>
	  
	  <a href="#posts-num-block" class="link link-btn menu-link">
	    quote
	  </a>
	  
	</li>
	<li>
	  
	  <a href="/login" class="link link-btn menu-link">
	    login
	  </a>
	  
	</li>
	</ul>
    </nav><!-- /.navbar-collapse -->

  <!-- end of menu -->
  </section>
</header>

 
<!-- BODY SECTION -->
<section id="section-body">

    
    <section id="banner-about-minimal2" class="col-xs-12 banner-full parallax" data-stellar-background-ratio="0.5">
	<section class="container text-center">
	    <section class="banner-title-3">
	      <div class="vc-text">
			<p class="lead black vthick text-uppercase">Welcome to your future</p>
		  
		  <h1 class="title text-uppercase black transition">MLM HS</h1>
		  <a href="/login" class="btn-link btn-link-transparent-black">login</a>
		  
	      </div>
	      <a href="#about" class="go-down"><span class="icon icon-arrows-slim-down"></span></a>
	 
	    </section>
	     
	</section>
    </section> 
	
	<section id="about" class="col-xs-12 intro">
      <section class="container text-center">
	
	    <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center margin-bottom">
		<h4 class="lead-title vthick lead-border-bottom border-black">We are a leading agency in creativity and innovation in 2016.</h4>
		
	    </div>
	    
	    <!-- three icons -->
	    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-lg-4 col-lg-offset-0 text-center">
		<h4 class="black"><strong><span class="icon icon-basic-cup opacity-05"></span></strong> Award-winning</h4>

	    </div>
	    
	    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-lg-4 col-lg-offset-0 text-center">
		<h4 class="black"><strong><span class="icon icon-basic-calendar opacity-05"></span></strong> Since 1998</h4>

	    </div>
	    
	    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-lg-4 col-lg-offset-0 text-center">
		<h4 class="black"><strong><span class="icon icon-basic-heart opacity-05"></span></strong> 2M Followers</h4>

	    </div>
	    
      </section>
    </section>

    <!-- design block - number of posts -->
  
  <section id="posts-num-block" class="col-xs-12 main-section-paddings">
    <section class="container">
	
	<div class="col-sm-8 col-md-4 col-md-offset-1 footer-subscribe">
	    <h4 class="lead-title white">Contact Us for a <strong>Free Quote</strong>!</h4>
	    <p class="white">Enter your e-mail address and we will get back to you shortly.</p>
	    <div>
		<a href="#" class="btn-link btn-link-transparent-white white">Get Quote</a>
	    </div>
	  
	</div>
	
	<div id="num-block" class="col-md-5 col-md-offset-1 white">
	    <span id="num-of-posts" class="counter white">64</span> customers & counting...
	</div>
    </section>
  </section>
    
    <section class="col-xs-12 intro">
      <section class="container text-center">
	
	    <div class="col-xs-12 col-sm-12 col-md-4 text-left">
		<h4 class="lead-title vthick border-full">We are a leading agency in creativity and innovation in 2016.</h4>
		
	    </div>
	    <div class="col-xs-12 col-sm-6 col-md-4 text-left">
		<h5 class="lead-title">Our <strong>Mission</strong></h5>
		<p class="lead-border-left border-black">Lorem ipsum dolor sit amet, no duo scaevola maiestatis,
		soleat noluisse delicatissimi ad duo. Laoreet patrioque interpretaris cu est. Mutat ipsum vix id.
		His ea falli aperiam partiendo, eu blandit singulis laboramus vis.</p>
	    </div>
	    <div class="col-xs-12 col-sm-6 col-md-4 text-left">
	      
		<h5 class="lead-title">Our <strong>Objectives</strong></h5>
		<p class="lead-border-left border-black">Lorem ipsum dolor sit amet, no duo scaevola maiestatis,
		soleat noluisse delicatissimi ad duo. Laoreet patrioque interpretaris cu est. Mutat ipsum vix id.
		His ea falli aperiam partiendo, eu blandit singulis laboramus vis.</p>
	    </div>

      </section>
    </section>

<!-- BODY SECTION ENDS HERE -->
 
<!-- FOOTER SECTION -->
<section id="section-footer" class="col-xs-12">

      <section class="col-xs-12 footer-widget widget-social widget-social-h text-center">
	 
	  <ul>
	    <li><a href="#"><i class="fa fa-facebook"></i>Facebook</a></li>
	    <li><a href="#"><i class="fa fa-twitter"></i>Twitter</a></li>
	    <li><a href="#"><i class="fa fa-dribbble"></i>Dribbble</a></li>
	    <li><a href="#"><i class="fa fa-instagram"></i>Instagram</a></li>
	  </ul>
      </section>
	
	
      <footer class="col-xs-12 copyright dark-bg">
	    <div class="col-xs-12">
		  <div>MLM HS &copy; 2016 - All rights reserved. </div>
	    </div>
      </footer>

</section>  


<!-- JS FILES  -->
{{ HTML::script('assets/front/js/jquery-2.1.4.min.js') }}
{{ HTML::script('assets/front/js/jquery-ui.min.js') }}
@if (App::environment() != 'local')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
@endif
{{ HTML::script('assets/front/js/clipboard.min.js') }}
{{ HTML::script('assets/front/js/imagesloaded.pkgd.min.js') }}
{{ HTML::script('assets/front/js/bootstrap.min.js') }}
{{ HTML::script('assets/front/js/classie.js') }}
{{ HTML::script('assets/front/js/waypoints.js') }}
{{ HTML::script('assets/front/js/owl-carousel.min.js') }}
{{ HTML::script('assets/front/js/lightbox.min.js') }}
{{ HTML::script('assets/front/js/jquery.isotope.min.js') }}
{{ HTML::script('assets/front/js/jquery.counterup.min.js') }}
{{ HTML::script('assets/front/js/timeline.js') }}
{{ HTML::script('assets/front/js/jquery.stellar.min.js') }}
{{ HTML::script('assets/front/js/custom.js') }}   

</body>
</html>
