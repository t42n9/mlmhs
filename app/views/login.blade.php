<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>MLMHS - Grow your business</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsive HTML5 Bootstrap Portfolio and Business Template">
    <meta name="keywords" content="Responsive, HTML5, Business, Portfolio, Corporate, jQuery Plugins, CSS3, Multipurpose, Corporate, creative, one-page, bootstrap3, owl carousel">

    <meta name="author" content="Coral-Themes">
    
    <!-- make sure to put your favicon.ico in your root directory of your website -->
    <!--<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">-->
    <!-- Latest compiled and minified CSS -->
	
	{{ HTML::style('assets/front/css/bootstrap.min.css') }}
	{{ HTML::style('assets/front/css/styles.css') }}

    @if (App::environment() != 'local')
    <!-- the font used in the template -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    @endif

    {{ HTML::script('assets/front/js/modernizr-2.5.3.min.js') }}
    
    @if (App::environment() != 'local')
          <!-- GOOGLE MAP -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATLU4U4BsP8mUI5gjCSAwYum2Nc3JaCi8&amp;dsensor=false"></script>
    @endif

</head>

<body>
    <!-- page loader -->
  <div id="loader-container">
      <div class="clear-loading loading-effect-3">
	      <div><span></span></div>
      </div>
  </div>
 
<!-- HEADER SECTION -->

<header id="section-header" class="navbar-fixed-top transition page-header page-header-transparent">
  <section class="container">
    <a href="/">
      <img src="images/logo.png" alt="logo" class="logo" />
    </a>
    
    
    <span id="openMenu" class="menu-icon">
      <i class="icon icon-arrows-hamburger1"></i>
      <span class="menu-name">Menu</span>
    </span>

  </section>
  
  <!-- MENU START -->
    <nav class="menu-container menu-container-top" id="menu">
        
	<div id="closeMenu" class="text-left col-sm-9 col-sm-offset-3 col-md-8 col-md-offset-4">
	  <span class="icon icon-arrows-remove transition"></span>
	</div>
	
	<div class="vc-text">   
	  <!-- first column on the left -->
	  <div class="menu-title col-xs-2 col-sm-3 col-md-4">
	    <span>LOGIN</span>
	     
	  </div>
	  <div class="col-xs-10 col-sm-9 col-md-8 menu-list">
	      <ul class="menu-item-links text-left">
		<li>
		  <a href="/">Home</a>
		</li>
		<li class="menu-social-icons">
		      <a href="contacts.html">
			<i class="fa fa-facebook"></i>
		      </a>
		      <a href="contacts.html">
			<i class="fa fa-twitter"></i>
		      </a>
		      <a href="contacts.html">
			<i class="fa fa-dribbble"></i>
		      </a>
		      <a href="contacts.html">
			<i class="fa fa-github"></i>
		      </a>
		</li>
	      </ul>
	    </div>
	</div>



	</nav>
	</header>

 
<!-- BODY SECTION -->
<section id="section-body">

    
    <section id="login" class="col-xs-12 text-center style-2">
      <section class="container main-section-padding-top">
	
	<div class="col-md-12">
	  
	  
		    <div class="col-xs-12 text-center">
		      <h4 class="lead-title black"><span class="icon icon-basic-pin2 icon-lg"></span> <strong>LET'S START!</strong></h4>
			  <!-- contact form -->
			
			{{ Form::open( [ 'method' => 'POST', 'route' => 'post_login', 'class' => 'form-horizontal contact-form col-xs-12 no-padding' ] ) }}
			    <div id="result"></div>
			      <div class="col-sm-12 no-padding">

				  <div class="form-group no-padding">
				    <input type="text" class="form-control" name="username" id="username" placeholder="Username*">
				  </div>
				  
				  <div class="form-group no-padding">
				      <input type="password" class="form-control" name="password" id="password" placeholder="Password*">
				  </div>

				  
				  <div class="col-sm-12 no-padding">
				      <div class="form-group">
					  <button type="submit" class="btn-link btn-link-solid-black transition">Login</button>
				      </div>
			      </div>

			{{ Form::close() }}
		</div>
      </section>

    </section>    

<!-- BODY SECTION ENDS HERE -->
 
<!-- FOOTER SECTION -->
<section id="section-footer" class="col-xs-12">
      
      
      <section class="col-xs-12 footer-widget widget-social widget-social-h text-center">
	 
	  <ul>
	    <li><a href="#"><i class="fa fa-facebook"></i>Facebook</a></li>
	    <li><a href="#"><i class="fa fa-twitter"></i>Twitter</a></li>
	    <li><a href="#"><i class="fa fa-dribbble"></i>Dribbble</a></li>
	    <li><a href="#"><i class="fa fa-instagram"></i>Instagram</a></li>
	  </ul>
      </section>
	
	
      <footer class="col-xs-12 copyright dark-bg">
	    <div class="col-xs-12">
		  <div>MLM HS &copy; 2016 - All rights reserved. </div>
	    </div>
      </footer>

</section>  


<!-- JS FILES  -->
{{ HTML::script('assets/front/js/jquery-2.1.4.min.js') }}
{{ HTML::script('assets/front/js/jquery-ui.min.js') }}
@if (App::environment() != 'local')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
@endif
{{ HTML::script('assets/front/js/clipboard.min.js') }}
{{ HTML::script('assets/front/js/imagesloaded.pkgd.min.js') }}
{{ HTML::script('assets/front/js/bootstrap.min.js') }}
{{ HTML::script('assets/front/js/classie.js') }}
{{ HTML::script('assets/front/js/waypoints.js') }}
{{ HTML::script('assets/front/js/owl-carousel.min.js') }}
{{ HTML::script('assets/front/js/lightbox.min.js') }}
{{ HTML::script('assets/front/js/jquery.isotope.min.js') }}
{{ HTML::script('assets/front/js/jquery.counterup.min.js') }}
{{ HTML::script('assets/front/js/timeline.js') }}
{{ HTML::script('assets/front/js/jquery.stellar.min.js') }}
{{ HTML::script('assets/front/js/custom.js') }}   

</body>
</html>
