@extends('default')

@section('optioncss')

  {{ HTML::style('assets/gantelella/js/datatables/jquery.dataTables.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/buttons.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/fixedHeader.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/responsive.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/scroller.bootstrap.min.css') }}

@stop

@extends('menu')

@section('content')

 <!-- page content -->
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2>Wallet</h2>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    @if (Session::has('msgerror'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Maaf!</strong> {{ Session::get('msgerror') }}
                        </div>
                        @if (Session::has('errorData'))
                            <?php $erroran = Session::get('errorData'); ?>
                        @endif
                    @endif
                    <!-- top tiles -->
                    <div class="row tile_count">
                        <div class="animated flipInY col-md-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right">
                                <span class="count_top"><i class="fa fa-user"></i> Saldo</span>
                                <div class="count">Rp. {{ number_format($dataWallet->saldo, 0, ',', '.') }}</div>
                            </div>
                        </div>
                    </div>
                    <!-- /top tiles -->

                    <p class="text-muted font-13 m-b-30">
                        <a href="/topup/add" class="btn btn-default" title="Add Saldo">Tambah Saldo</a>
                        <a href="/topup/history" class="btn btn-default" title="History Topup">History Topup</a>
                    </p>
                    <table id="historywallet" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Uraian</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($dataHistory))
                            <?php $nomor = 1; ?>
                                @foreach($dataHistory as $row)
                                    <tr>
                                        <td>{{ $nomor++ }}.</td>
                                        <td>{{ date('d-m-Y H:i', strtotime($row->tgl_history)) }}</td>
                                        <td>{{ $row->keterangan }}</td>
                                        <td align="right">
                                            @if ($row->real_debet >= 0)
                                                -
                                            @else
                                                {{ number_format($row->real_debet, 0, ',', '.') }}
                                            @endif
                                        </td>
                                        <td align="right">
                                            @if ($row->real_kredit <= 0)
                                                -
                                            @else
                                                {{ number_format($row->real_kredit, 0, ',', '.') }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /page content -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#historywallet').dataTable(  );
        });
    </script>
@stop

@section('footjs')

    {{ HTML::script('assets/gantelella/js/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.buttons.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/jszip.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/pdfmake.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/vfs_fonts.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.html5.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.print.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.fixedHeader.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.keyTable.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.responsive.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/responsive.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.scroller.min.js') }}

@stop

