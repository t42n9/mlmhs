@extends('default')

@section('optioncss')

  {{ HTML::style('assets/gantelella/js/datatables/jquery.dataTables.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/buttons.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/fixedHeader.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/responsive.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/scroller.bootstrap.min.css') }}

@stop

@extends('menu')

@section('content')

<div class="modal fade" id="confirm-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <blockquote>
                    Dengan menekan tombol Beli Tiket dibawah maka Anda secara sadar, sehat dan tidak sedang dalam keadaan tertekan untuk dikurangi saldo wallet Anda sesuai harga tiket yang berlaku.
                </blockquote>
                <div align='center'>
                {{ Form::open(array('route' => 'buyticket', 'class' => 'form-horizontal form-label-left')) }}
                {{ Form::button('Beli Tiket', array('class'=>'btn btn-danger btn-lg', 'type'=>'submit')) }}
                {{ Form::close() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

 <!-- page content -->
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2>Daftar Tiket</h2>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    <?php //<p class="text-muted font-13 m-b-30"> ?>
                        <?php //<a href="/ticket/buy" class="btn btn-default text-muted font-13 m-b-30" title="Buy Ticket Register">Buy Ticket</a> ?>
                        <a href="#confirm-dialog" data-toggle="modal" class="btn btn-default">Beli Tiket</a>
                        <span style="display:inline"> Untuk setiap pembelian tiket akan dikenakan biaya sebesar Rp {{ number_format($Harga, 0, ',', '.') }} diambil dari saldo Anda.</span>
                    <?php //</p> ?>
                    <p>&nbsp;</p>

                    @if (Session::has('msgerror'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Error!</strong> {{ Session::get('msgerror') }}
                        </div>
                    @endif

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#listticketall" aria-controls="listticketall" role="tab" data-toggle="tab" id="clickTableAll">All</a></li>
                        <li role="presentation"><a href="#listticketused" aria-controls="listticketused" role="tab" data-toggle="tab" id="clickTableUsed">Sudah dipakai</a></li>
                        <li role="presentation"><a href="#listticketnotused" aria-controls="listticketnotused" role="tab" data-toggle="tab" id="clickTableNotUsed">Belum dipakai</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="listticketall"><br><br>
                            <table id="ticketall" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Tanggal</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($dataAll))
                                        <?php $nomorAll = 1; ?>
                                        @foreach($dataAll as $row)
                                            <tr>
                                                <td>{{ $nomorAll++ }}.</td>
                                                <td>{{ $row->tiket_nomor }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($row->tgl_tiket)) }}</td>
                                                <td align="right">{{ number_format($row->harga_tiket, 0, ',', '.') }}</td>
                                                <td>
                                                    @if ($row->is_used == 1)
                                                        <span class="label label-primary">Used By {{ $row->nama }}</span>
                                                    @else
                                                        <span class="label label-default">Not Used</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="listticketused"><br><br>
                            <table id="ticketused" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Tanggal</th>
                                        <th>Harga</th>
                                        <th>Dipakai oleh?</th>
                                        <th>Dipakai tanggal?</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($dataUsed))
                                        <?php $nomorUsed = 1; ?>
                                        @foreach($dataUsed as $row)
                                            <tr>
                                                <td>{{ $nomorUsed++ }}.</td>
                                                <td>{{ $row->tiket_nomor }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($row->tgl_tiket)) }}</td>
                                                <td align="right">{{ number_format($row->harga_tiket, 0, '.', ',') }}</td>
                                                <td>{{ $row->nama }}</td>
                                                <td>{{ date('d-m-Y', strtotime($row->used_at)) }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="listticketnotused"><br><br>
                            <table id="ticketnotused" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Tanggal</th>
                                        <th>Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($dataUnUsed))
                                        <?php $nomorUnUsed = 1; ?>
                                        @foreach($dataUnUsed as $row)
                                            <tr>
                                                <td>{{ $nomorUnUsed++ }}.</td>
                                                <td>{{ $row->tiket_nomor }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($row->tgl_tiket)) }}</td>
                                                <td align="right">{{ number_format($row->harga_tiket, 0, '.', ',') }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- /page content -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#ticketall').dataTable(  );
            $('#ticketused').dataTable(  );
            $('#ticketnotused').dataTable(  );
        });
    </script>
@stop

@section('footjs')

    {{ HTML::script('assets/gantelella/js/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.buttons.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/jszip.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/pdfmake.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/vfs_fonts.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.html5.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.print.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.fixedHeader.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.keyTable.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.responsive.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/responsive.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.scroller.min.js') }}

@stop

