@extends('default')

@section('optioncss')

  {{ HTML::style('assets/gantelella/js/datatables/jquery.dataTables.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/buttons.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/fixedHeader.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/responsive.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/scroller.bootstrap.min.css') }}

@stop

@extends('menu')

@section('content')

<style type="text/css">
    .table thead th {
        text-align: center;
    }
</style>

<div class="modal fade" id="confirm-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Konfirmasi Pembayaran</h4>
            </div>
            <div class="modal-body" align="center">
                <blockquote>
                    Apakah anda yakin ingin mengkonfirmasi dan sudah memeriksa status pembayaran ini?                     
                </blockquote>
                <button type="button" class="btn btn-primary">Konfirmasi</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

 <!-- page content -->
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2>History Topup</h2>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    @if (Session::has('msgerror'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Maaf!</strong> {{ Session::get('msgerror') }}
                        </div>
                        @if (Session::has('errorData'))
                            <?php $erroran = Session::get('errorData'); ?>
                        @endif
                    @endif
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation"><a href="#listall" aria-controls="listall" role="tab" data-toggle="tab" id="clickAll">Semua</a></li>
                        <li role="presentation"><a href="#listcomplete" aria-controls="listcomplete" role="tab" data-toggle="tab" id="clickcomplete">Tuntas</a></li>
                        <li role="presentation" class="active"><a href="#listwaiting" aria-controls="listwaiting" role="tab" data-toggle="tab" id="clickwaiting">Konfirmasi</a></li>
                        <li role="presentation"><a href="#listprocessing" aria-controls="listprocessing" role="tab" data-toggle="tab" id="clickprocessing">Dalam Proses</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="listall"><br><br>
                            <table id="historyall" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">No.Transaksi</th>
                                        <th rowspan="2">Tanggal</th>
                                        <th colspan="2">Bank</th>
                                        <th rowspan="2">Nilai</th>
                                        <th rowspan="2">Status</th>
                                    </tr>
                                    <tr>
                                        <th>Sumber</th>
                                        <th style="border-right-width:1px">Tujuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Member 1</td>
                                        <td>01455210100425</td>
                                        <td>16 April 2016</td>
                                        <td>BCA</td>
                                        <td>BCA</td>
                                        <td>Rp. 50.256,-</td>
                                        <td>
                                            <span class="btn btn-success btn-sm disabled">Tuntas</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Member 2</td>
                                        <td>01455210100425</td>
                                        <td>16 April 2016</td>
                                        <td>BCA</td>
                                        <td>BCA</td>
                                        <td>Rp. 50.256,-</td>
                                        <td>
                                            <a href="#confirm-dialog" data-toggle="modal" data-id="1" class="btn btn-default btn-sm confirm-payment">Konfirmasi</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Member 3</td>
                                        <td>01455210100425</td>
                                        <td>16 April 2016</td>
                                        <td>BCA</td>
                                        <td>BCA</td>
                                        <td>Rp. 50.256,-</td>
                                        <td>
                                            <span class="btn btn-info btn-sm disabled">Menunggu Konfirmasi</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="listcomplete"><br><br>
                            <table id="historycomplete" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">No.Transaksi</th>
                                        <th rowspan="2">Tanggal</th>
                                        <th colspan="2">Bank</th>
                                        <th rowspan="2">Nilai</th>
                                    </tr>
                                    <tr>
                                        <th>Sumber</th>
                                        <th style="border-right-width:1px">Tujuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Member 1</td>
                                        <td>01455210100425</td>
                                        <td>16 April 2016</td>
                                        <td>BCA</td>
                                        <td>BCA</td>
                                        <td>Rp. 50.256,-</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane active" id="listwaiting"><br><br>
                            <table id="historywaiting" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">No.Transaksi</th>
                                        <th rowspan="2">Tanggal</th>
                                        <th colspan="2">Bank</th>
                                        <th rowspan="2">Nilai</th>
                                        <th rowspan="2">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th>Sumber</th>
                                        <th style="border-right-width:1px">Tujuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Member 1</td>
                                        <td>01455210100425</td>
                                        <td>16 April 2016</td>
                                        <td>BCA</td>
                                        <td>BCA</td>
                                        <td>Rp. 50.256,-</td>
                                        <td>
                                            <a href="#confirm-dialog" data-toggle="modal" data-id="1" class="btn btn-default btn-sm confirm-payment">Konfirmasi</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="listprocessing"><br><br>
                            <table id="historyprocessing" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Member 3</td>
                                        <td>16 April 2016</td>
                                        <td>Rp. 50.256,-</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- /page content -->

@stop

@section('footjs')
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#historyall').dataTable(  );
            $('#historycomplete').dataTable(  );
            $('#historywaiting').dataTable(  );
            $('#historyprocessing').dataTable(  );
        });
        $(document).on("click", ".confirm-payment", function () {
            var myId = $(this).data('id');
            $(".modal-body #topup_id").val( myId );
        });
    </script>

    {{ HTML::script('assets/gantelella/js/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.buttons.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/jszip.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/pdfmake.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/vfs_fonts.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.html5.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.print.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.fixedHeader.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.keyTable.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.responsive.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/responsive.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.scroller.min.js') }}

@stop

