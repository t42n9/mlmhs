@extends('default')

@section('optioncss')

  {{ HTML::style('assets/gantelella/js/datatables/jquery.dataTables.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/buttons.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/fixedHeader.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/responsive.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/scroller.bootstrap.min.css') }}

@stop

@extends('menu')

@section('content')

<style type="text/css">
    .table thead th {
        text-align: center;
    }
</style>

    <div class="modal fade" id="proceed-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {{ Form::open(array('route' => 'acctopup', 'id' => 'myacc')) }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Terima Pembayaran</h4>
                    </div>
                    <div class="modal-body" align="center">
                        {{  Form::hidden('prosesid', '', array('id' => 'prosesid')) }}
                        <blockquote>
                            Apakah anda yakin ingin mengkonfirmasi dan sudah memeriksa status pembayaran ini ?
                        </blockquote>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Proses</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="reject-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {{ Form::open(array('route' => 'rejecttopup', 'id' => 'myreject')) }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Tolak Pembayaran</h4>
                    </div>
                    <div class="modal-body" align="center">
                        {{  Form::hidden('rejectid', '', array('id' => 'rejectid')) }}
                        <blockquote>
                            Apakah anda yakin ingin menolak dan sudah memeriksa status pembayaran ini ?
                        </blockquote>
                        <label for="alasan" class="col-md-12" style="text-align:left">Alasan Penolakan :</label>
                        {{ Form::textarea('alasan', null, array('class' => 'form-control', 'id' => 'alasan', 'maxlength' => 200, 'rows' => 4)) }}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Proses</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

 <!-- page content -->
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2>Wallet</h2>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    @if (Session::has('msgerror'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Maaf!</strong> {{ Session::get('msgerror') }}
                        </div>
                        @if (Session::has('errorData'))
                            <?php $erroran = Session::get('errorData'); ?>
                        @endif
                    @else
                        @if (Session::has('msgsuccess'))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('msgsuccess') }}</strong>
                            </div>
                        @endif 
                    @endif
                    <!-- top tiles -->
                    <div class="row tile_count">
                        <div class="animated flipInY col-md-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right">
                                <span class="count_top"><i class="fa fa-user"></i> Saldo Perusahaan</span>
                                <div class="count">Rp. {{ number_format($wCompany->saldo, 0, ',', '.') }}</div>
                                <a class="count_bottom" href="/admin/wallet/historywallet">Details</a>
                            </div>
                        </div>
                        <div class="animated flipInY col-md-4 tile_stats_count">
                            <div class="left"></div>
                            <div class="right">
                                <span class="count_top"><i class="fa fa-user"></i> Saldo Tabungan</span>
                                <div class="count">Rp. {{ number_format($wSimpanan->saldo, 0, ',', '.') }}</div>
                                <a class="count_bottom" href="/admin/wallet/historytabungan">Details</a>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <!-- /top tiles -->
                    <table id="reqtopup" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">No.Order</th>
                                <th rowspan="2">Tanggal</th>
                                <th rowspan="2">Nama Member</th>
                                <th colspan="2">Bank</th>
                                <th rowspan="2">Jumlah</th>
                                <th rowspan="2">&nbsp;</th>
                            </tr>
                            <tr>
                                <th>Sumber</th>
                                <th style="border-right-width:1px">Tujuan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($reqTopUp))
                            <?php $nomor = 1; ?>
                                @foreach($reqTopUp as $row)
                                    <tr>
                                        <td>{{ $nomor++ }}.</td>
                                        <td>{{ $row->nomor_topup }}</td>
                                        <td>{{ date('d-m-Y H:i', strtotime($row->tgl_confirmed)) }}</td>
                                        <td>{{ $row->nama }}</td>
                                        <td>
                                            {{ strtoupper($row->use_bank) }}<br>
                                            ({{ $row->use_bank_rek }})<br> 
                                            {{ $row->use_bank_nama }}
                                        </td>
                                        <td>
                                            {{ strtoupper($row->transfer_to) }}<br>
                                            ({{ $row->transfer_to_rek }})<br>
                                            {{ $row->transfer_to_nama }}
                                        </td>
                                        <td align="right">{{ number_format($row->jml_transfer, 0, ',', '.') }}</td>
                                        <td>
                                            <a href="#proceed-dialog" data-toggle="modal" data-id="{{ $row->id }}" class="btn btn-success btn-sm ok-payment">Proses</a>
                                            <a href="#reject-dialog" data-toggle="modal" data-id="{{ $row->id }}" class="btn btn-danger btn-sm no-payment">Tolak</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /page content -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#reqtopup').dataTable(  );
        });
        $(document).on("click", ".ok-payment", function () {
            var myId = $(this).data('id');
            $("#proceed-dialog form#myacc #prosesid").val( myId );
        });
        $(document).on("click", ".no-payment", function () {
            var myId = $(this).data('id');
            $("#reject-dialog form#myreject #rejectid").val( myId );
            $("#reject-dialog form#myreject #alasan").val( '' );
        });
        $('form#myreject').submit(function() {
            var t = $('#alasan').val();
            if (t == '') {
                alert('Alasan harus diisi.');
                return false;
            } else {
                return true;
            }
        })
    </script>
@stop

@section('footjs')

    {{ HTML::script('assets/gantelella/js/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.buttons.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/jszip.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/pdfmake.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/vfs_fonts.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.html5.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.print.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.fixedHeader.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.keyTable.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.responsive.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/responsive.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.scroller.min.js') }}

@stop

