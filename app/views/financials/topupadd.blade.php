@extends('default')

@extends('menu')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title"><div class="title_left"><h3>Topup</h3></div></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12" style="width:100%;">
                <div class="x_panel" style="width:100%;">
                    <div class="x_content">
                        <div class="x_title">
                            <h2>Tambah Saldo <small>Ikuti semua instruksinya hingga selesai</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{ Form::open(array('route' => 'posttopup', 'id' => 'myform')) }}
                                <div id="wizard" class="form_wizard wizard_horizontal">
                                    <ul class="wizard_steps">
                                        <li>
                                            <a href="#step-1">
                                                <span class="step_no">1</span>
                                                <span class="step_descr">Langkah Pertama<br /><small>Berapa?</small></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-2">
                                                <span class="step_no">2</span>
                                                <span class="step_descr">Langkah Kedua<br /><small>Pilih bank dan persetujuan</small></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#step-3">
                                                <span class="step_no">3</span>
                                                <span class="step_descr">Langkah Ketiga<br /><small>Konfirmasi Pembayaran</small></span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="step-1">
                                        <h2 class="StepTitle">Langkah Pertama</h2> <p>Berapa saldo yang ingin anda tambahkan?</p>
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputAmount">Jumlah (Dalam Rupiah)</label>
                                            <div class="input-group col-md-7 col-xs-12">
                                                <div class="input-group-addon">Rp</div>
                                                {{  Form::text('amount', '', array('id' => 'amount', 'class' => 'form-control col-md-6 col-xs-12', 
                                                                                'placeholder' => 'Masukkan jumlah tanpa menggunakan titik atau koma, contoh: 1000000')) }}
                                                {{  Form::hidden('amount2', 0, array('id' => 'amount2')) }}
                                                {{  Form::hidden('amount3', 0, array('id' => 'amount3')) }}
                                                <div class="input-group-addon">.00</div>
                                            </div>
                                            <div id="msg_amount"></div>
                                        </div>
                                    </div>
                                    <div id="step-2">
                                        <h2 class="StepTitle">Langkah Kedua</h2> <p>Catat data rekening yang ingin anda tuju.</p>
                                        <!-- start accordion -->
                                        <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel">
                                                <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    <h4 class="panel-title"># BCA >></h4>
                                                </a>
                                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <a href="#">
                                                                    {{ HTML::image('images/logo/bca.png', 'Bank Central Asia', array('class' => 'media-object img-thumbnail')) }}
                                                                </a>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <h4>Bank Central Asia</h4>
                                                                <p>a/n John doe<br>No.Rek : 880080808</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <h4 class="panel-title"># Mandiri >></h4>
                                                </a>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <a href="#">
                                                                    {{ HTML::image('images/logo/mandiri.png', 'Bank Mandiri', array('class' => 'media-object img-thumbnail')) }}
                                                                </a>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <h4>Bank Mandiri</h4>
                                                                <p>a/n John doe<br>No.Rek : 880080808</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end of accordion -->
                                        <div class="clearfix"></div><hr><p>Persetujuan</p>
                                            <label>
                                                {{  Form::checkbox('accept', 1, null, array('id' => 'accept', 'class' => 'flat')) }} Saya setuju dengan ketentuan transaksi berikut ini :
                                            </label>
                                            <ul>
                                                <li>Melunasi tagihan pembayaran setelah melakukan pemesanan.</li>
                                                <li>Melakukan konfirmasi pembayaran sebelum pukul 24.00 WIB hari ini ( {{ date('d-m-Y') }} ), atau akan dianggap batal.</li>
                                            </ul>
                                            <div id="msg_accept"></div>
                                    </div>
                                    <div id="step-3">
                                        <h2 class="StepTitle">Langkah Ketiga</h2> <p>Konfirmasi Pembayaran</p>
                                            <div class="bs-example" data-example-id="simple-jumbotron">
                                            <div class="jumbotron" align="center" id="jumbotron"></div>
                                        </div>
                                        <div id="msg_error"></div>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('footjs')
    
    <script>
        $(document).ready(function() {
            $(":input").inputmask();
        });
        $(document).ready(function() {
            $(document).on("keypress", ":input:not(textarea)", function(event) {
                return event.keyCode != 13;
            });
            // Smart Wizard
            $('#wizard').smartWizard( { includeFinishButton: true, hideButtonsOnDisabled: true, onLeaveStep:leaveAStepCallback, onFinish:onFinishCallback });
            function leaveAStepCallback(obj, context){ return validateSteps(obj.attr('rel'), true); }
            function onFinishCallback(objs, context){ if (validateSteps(1, false) && validateSteps(2, false)) { $('form#myform').submit(); } }
            function validateSteps(step, opt){
                <?php /*var isStepValid = true;
                // validate step 1
                if(step == 1){
                    if(validateStep1() == false ){
                        isStepValid = false; 
                        $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
                    }else{
                        $('#msg_amount').html('').hide();
                        $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
                    }
                }
                // validate step2
                if(step == 2){
                    if(validateStep2() == false ){
                        isStepValid = false; 
                        $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
                    }else{
                        $('#msg_accept').html('').hide();
                        $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
                    }
                }
                */ ?>

                if (typeof opt === 'undefined') { opt = true; }

                var isStepValid = (step == 1 || step == 2);
                if (isStepValid) {
                    isStepValid = (step == 1) ? validateStep1(opt) : validateStep2(opt);
                    $('#wizard').smartWizard('setError',{stepnum:step,iserror:(isStepValid == false)});
                }else{
                    isStepValid = true;
                }

                return isStepValid;
            }

            function validateStep1(opt){
                if (typeof opt === 'undefined') { opt = true; }
                var isValid = true;
                var randomNum = (opt === true) ? Math.floor((Math.random() * 3999) + 100) : parseInt($('#amount3').val());
                var amount = $('#amount').val();
                $('#amount2').val(parseInt(amount));
                $('#amount3').val(randomNum);
                if(!amount){
                    isValid = false;
                    if (opt === true) {$('#msg_amount').html('<p class="text-danger"><strong>Maaf!</strong> Field harus diisi</p>').show();}
                }
                else if(!isNormalInteger(amount)){
                    isValid = false;
                    if (opt === true) {$('#msg_amount').html('<p class="text-danger"><strong>Maaf!</strong> Field harus diisi hanya dengan angka</p>').show();}
                }
                else if(amount < 50000){
                    isValid = false;
                    if (opt === true) {$('#msg_amount').html('<p class="text-danger"><strong>Maaf!</strong> Field harus diisi dengan angka yang berjumlah 50000 atau lebih</p>').show();}
                }
                else{
                    $('#msg_amount').html('').hide();
                }
                randomNum += parseInt(amount);
                var stringNum = number_format(randomNum, 0, ',', '.');
                var stringNum4 = stringNum.substr(-5);
                var stringNum1 = stringNum.slice(0,stringNum.length - 5);
                $('#jumbotron').html('<h1>Rp. ' + stringNum1 + '<span class="text-danger">' + stringNum4 + ',-</span></h1><p>Pastikan jumlah yang anda transfer SAMA dengan angka yang tertera diatas (termasuk 4 angka dibelakang)</p><small>notes : anda akan mendapatkan saldo sebesar Rp. ' + stringNum + ' , jika pemabayaran sudah diterima</small> ').show();
                $('#amount2').val(randomNum);
                return isValid;
            }

            function validateStep2(opt){
                if (typeof opt === 'undefined') { opt = true; }
                var isValid = true;
                if(!($("#accept").is(':checked'))){
                    isValid = false;
                    if (opt === true) {
                        $('#msg_accept').html('<p class="text-danger"><strong>Maaf!</strong> Anda diharuskan untuk menyetujui persetujuan diatas sebelum melanjutkan ke langkah selanjutnya</p>').show();
                    }
                }
                else{
                    $('#msg_accept').html('').hide();
                }
                return isValid;
            }
          
            function isNormalInteger(str) {
                var n = ~~Number(str);
                return String(n) === str && n >= 0;
            }
          
            function number_format(number, decimals, dec_point, thousands_sep) {
                number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
                var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                    s = '',
                    toFixedFix = function (n, prec) {
                                    var k = Math.pow(10, prec);
                                    return '' + (Math.round(n * k) / k).toFixed(prec);
                                };
                    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || '').length < prec) {
                    s[1] = s[1] || '';
                    s[1] += new Array(prec - s[1].length + 1).join('0');
                }
                return s.join(dec);
            }
        });
    </script>

    {{ HTML::script('assets/gantelella/js/input_mask/jquery.inputmask.js') }}
    {{ HTML::script('assets/gantelella/js/wizard/jquery.smartWizard.js') }}

@stop


