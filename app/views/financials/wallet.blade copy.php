@extends('default')

@section('optioncss')

  {{ HTML::style('assets/gantelella/js/datatables/jquery.dataTables.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/buttons.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/fixedHeader.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/responsive.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/scroller.bootstrap.min.css') }}

@stop

@extends('menu')

@section('content')

 <!-- page content -->
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2>Wallet</h2>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    <!-- top tiles -->
                    <div class="row tile_count">
                        <div class="animated flipInY col-md-4 tile_stats_count">
                            <div class="left"></div>
                                <div class="right">
                                    <span class="count_top"><i class="fa fa-user"></i> Saldo</span>
                                    <div class="count">2,500,000.00</div>
                                    <span class="count_bottom">in Rupiahs</span>
                                </div>
                            </div>
                        <div class="animated flipInY col-md-4 tile_stats_count">
                            <div class="left"></div>
                                <div class="right">
                                    <span class="count_top"><i class="fa fa-clock-o"></i> Debit</span>
                                    <div class="count">4,500,000.00</div>
                                    <span class="count_bottom">in Rupiahs</span>
                                </div>
                        </div>
                      <div class="animated flipInY col-md-4 tile_stats_count">
                        <div class="left"></div>
                        <div class="right">
                          <span class="count_top"><i class="fa fa-clock-o"></i> Kredit</span>
                          <div class="count">2,000,000.00</div>
                          <span class="count_bottom">in Rupiahs</span>
                        </div>
                      </div>

                    </div>
                    <!-- /top tiles -->

                    <p class="text-muted font-13 m-b-30"><a href="/topup" class="btn btn-default" title="Add Saldo">Topup Saldo</a></p>
                    <table id="historywallet" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Debit</th>
                                <th>Kredit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>20000</td>
                                <td>15000</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>20000</td>
                                <td>15000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /page content -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#historywallet').dataTable(  );
        });
    </script>
@stop

@section('footjs')

    {{ HTML::script('assets/gantelella/js/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.buttons.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/jszip.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/pdfmake.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/vfs_fonts.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.html5.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.print.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.fixedHeader.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.keyTable.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.responsive.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/responsive.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.scroller.min.js') }}

@stop

