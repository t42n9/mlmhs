@extends('default')

@section('optioncss')

  {{ HTML::style('assets/gantelella/js/datatables/jquery.dataTables.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/buttons.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/fixedHeader.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/responsive.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/scroller.bootstrap.min.css') }}

@stop

@extends('menu')

@section('content')

<style type="text/css">
    .table thead th {
        text-align: center;
    }
</style>

<div class="modal fade" id="confirm-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(array('route' => 'confirmtopup', 'id' => 'myconfirm')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Konfirmasi Pembayaran</h4>
                </div>
                <div class="modal-body">
                    {{  Form::hidden('targetid', '', array('id' => 'targetid')) }}
                    <label for="data_rekening">Bukti Transfer</label>
                    {{  Form::text('usebank', '', array('id' => 'usebank', 'class' => 'form-control', 'placeholder' => 'Bank Asal')) }}
                    {{  Form::text('userek', '', array('id' => 'userek', 'class' => 'form-control', 'placeholder' => 'Nomor Rekening')) }}
                    {{  Form::text('usename', '', array('id' => 'usename', 'class' => 'form-control', 'placeholder' => 'Atas Nama')) }}
                    <hr>
                    <label for="bankdest">Bank Tujuan</label>
                    <?php 
                        $listBank = array(0 => 'Pilih Bank Yang Dituju');
                        foreach($CompanyBank AS $rowbank) {
                            $listBank[$rowbank->ID] = $rowbank->Nama . ' | ' . $rowbank->Rekening . ' | ' . $rowbank->AtasNama;
                        }
                    ?>
                    {{ Form::select('bankdest', $listBank, null, array('id' => 'bankdest', 'class' => 'form-control')) }}
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Konfirmasi</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

 <!-- page content -->
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2>History Topup</h2>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    @if (Session::has('msgerror'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>Maaf!</strong> {{ Session::get('msgerror') }}
                        </div>
                        @if (Session::has('errorData'))
                            <?php $erroran = Session::get('errorData'); ?>
                        @endif
                    @endif
                    <p class="text-muted font-13 m-b-30"><a href="/topup/add" class="btn btn-default" title="Topup Saldo">Tambah Saldo</a></p>

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation"><a href="#listall" aria-controls="listall" role="tab" data-toggle="tab" id="clickAll">Semua</a></li>
                        <li role="presentation"><a href="#listcomplete" aria-controls="listcomplete" role="tab" data-toggle="tab" id="clickcomplete">Tuntas</a></li>
                        <li role="presentation" class="active"><a href="#listwaiting" aria-controls="listwaiting" role="tab" data-toggle="tab" id="clickwaiting">Konfirmasi</a></li>
                        <li role="presentation"><a href="#listprocessing" aria-controls="listprocessing" role="tab" data-toggle="tab" id="clickprocessing">Dalam Proses</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="listall"><br><br>
                            <table id="historyall" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">No.Transaksi</th>
                                        <th rowspan="2">Tanggal</th>
                                        <th colspan="2">Bank</th>
                                        <th rowspan="2">Nilai</th>
                                        <th rowspan="2">Status</th>
                                    </tr>
                                    <tr>
                                        <th>Sumber</th>
                                        <th style="border-right-width:1px">Tujuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($dataAll))
                                        <?php $nomorAll = 1; ?>
                                        @foreach($dataAll as $row)
                                            <tr>
                                                <td>{{ $nomorAll++ }}.</td>
                                                <td>{{ $row->nomor_topup }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($row->tgl_topup)) }}</td>
                                                <td>
                                                    @if ($row->use_bank_rek != '') 
                                                        {{ strtoupper($row->use_bank) }}<br>
                                                        ({{ $row->use_bank_rek }})<br> 
                                                        {{ $row->use_bank_nama }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($row->transfer_to_rek != '') 
                                                        {{ strtoupper($row->transfer_to) }}<br>
                                                        ({{ $row->transfer_to_rek }})<br>
                                                        {{ $row->transfer_to_nama }}
                                                    @endif
                                                </td>
                                                <td align="right">{{ number_format($row->jml_transfer, 0, ',', '.') }}</td>
                                                <td>
                                                    @if ($row->is_confirmed == 0)
                                                        <a href="#confirm-dialog" data-toggle="modal" data-id="{{ $row->id }}" class="btn btn-default btn-sm confirm-payment">Konfirmasi</a>
                                                    @else
                                                        @if ($row->status == 1)
                                                            <span class="btn btn-success btn-sm disabled">Diterima</span>
                                                        @elseif ($row->status == 2)
                                                            <span class="btn btn-danger btn-sm disabled">Dibatalkan</span>
                                                        @elseif ($row->status == 3)
                                                            <span class="btn btn-danger btn-sm disabled">Ditolak</span>
                                                        @elseif ($row->status == 0)
                                                            <span class="btn btn-primary btn-sm disabled">Dalam Proses</span>
                                                        @else
                                                            <span class="btn btn-danger btn-sm disabled">Proses Tidak Dikenal</span>
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="listcomplete"><br><br>
                            <table id="historycomplete" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">No.Transaksi</th>
                                        <th rowspan="2">Tanggal</th>
                                        <th colspan="2">Bank</th>
                                        <th rowspan="2">Nilai</th>
                                    </tr>
                                    <tr>
                                        <th>Sumber</th>
                                        <th style="border-right-width:1px">Tujuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($dataComplete))
                                        <?php $nomorComplete = 1; ?>
                                        @foreach($dataComplete as $row)
                                            <tr>
                                                <td>{{ $nomorComplete++ }}.</td>
                                                <td>{{ $row->nomor_topup }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($row->tgl_topup)) }}</td>
                                                <td>
                                                    {{ strtoupper($row->use_bank) }}<br>
                                                    ({{ $row->use_bank_rek }})<br>
                                                    {{ $row->use_bank_nama }}
                                                </td>
                                                <td>
                                                    {{ strtoupper($row->transfer_to) }}<br>
                                                    ({{ $row->transfer_to_rek }})<br>
                                                    {{ $row->transfer_to_nama }}
                                                </td>
                                                <td align="right">{{ number_format($row->jml_transfer, 0, ',', '.') }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane active" id="listwaiting"><br><br>
                            <table id="historywaiting" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No.Transaksi</th>
                                        <th>Tanggal</th>
                                        <th>Nilai</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($dataWaiting))
                                        <?php $nomorWaiting = 1; ?>
                                        @foreach($dataWaiting as $row)
                                            <tr>
                                                <td>{{ $nomorWaiting++ }}.</td>
                                                <td>{{ $row->nomor_topup }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($row->tgl_topup)) }}</td>
                                                <td align="right">{{ number_format($row->jml_transfer, 0, ',', '.') }}</td>
                                                <td>
                                                    <a href="#confirm-dialog" data-toggle="modal" data-id="{{ $row->id }}" class="btn btn-default btn-sm confirm-payment">Konfirmasi</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="listprocessing"><br><br>
                            <table id="historyprocessing" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">No.Transaksi</th>
                                        <th rowspan="2">Tanggal</th>
                                        <th colspan="2">Bank</th>
                                        <th rowspan="2">Nilai</th>
                                    </tr>
                                    <tr>
                                        <th>Sumber</th>
                                        <th style="border-right-width:1px">Tujuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($dataProcessing))
                                        <?php $nomorProses = 1; ?>
                                        @foreach($dataProcessing as $row)
                                            <tr>
                                                <td>{{ $nomorProses++ }}.</td>
                                                <td>{{ $row->nomor_topup }}</td>
                                                <td>{{ date('d-m-Y H:i', strtotime($row->tgl_topup)) }}</td>
                                                <td>
                                                    {{ strtoupper($row->use_bank) }}<br>
                                                    ({{ $row->use_bank_rek }})<br>
                                                    {{ $row->use_bank_nama }}
                                                </td>
                                                <td>
                                                    {{ strtoupper($row->transfer_to) }}<br>
                                                    ({{ $row->transfer_to_rek }})<br>
                                                    {{ $row->transfer_to_nama }}
                                                </td>
                                                <td align="right">{{ number_format($row->jml_transfer, 0, ',', '.') }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- /page content -->

@stop

@section('footjs')
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#historyall').dataTable(  );
            $('#historycomplete').dataTable(  );
            $('#historywaiting').dataTable(  );
            $('#historyprocessing').dataTable(  );
        });
        $(document).on("click", ".confirm-payment", function () {
            var myId = $(this).data('id');
            $("input#usebank").val('');
            $("input#userek").val('');
            $("input#usename").val('');
            $("select#bankdest").val(0);
            $(".modal-body #targetid").val( myId );
        });
    </script>

    {{ HTML::script('assets/gantelella/js/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.buttons.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/jszip.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/pdfmake.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/vfs_fonts.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.html5.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.print.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.fixedHeader.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.keyTable.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.responsive.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/responsive.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.scroller.min.js') }}

@stop

