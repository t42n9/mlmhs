<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>MLM-HS</title>

  <!-- Bootstrap core CSS -->

  {{ HTML::style('bootstrap/css/bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/fonts/css/font-awesome.min.css') }}
  {{ HTML::style('assets/gantelella/css/animate.min.css') }}
  {{ HTML::style('assets/gantelella/css/custom.css') }}
  {{ HTML::style('assets/gantelella/css/icheck/flat/green.css') }}
  
  {{ HTML::script('assets/gantelella/js/jquery.min.js') }}
  {{ HTML::script('assets/gantelella/js/bootstrap.min.js') }}

  @yield('optioncss')

  @yield('optionjs')
  
</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>MLM-HS</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_info">
              <span>Selamat Datang,</span>
              <h2>{{ (!empty($User)) ? $User->nama : 'Administrator' }}</h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />

          @yield('menu')

        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars" style="cursor:pointer"></i></a><br><br>
            </div>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->

      @yield('content')

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  {{ HTML::script('assets/gantelella/js/progressbar/bootstrap-progressbar.min.js') }}
  {{ HTML::script('assets/gantelella/js/nicescroll/jquery.nicescroll.min.js') }}
  {{ HTML::script('assets/gantelella/js/icheck/icheck.min.js') }}
  {{ HTML::script('assets/gantelella/js/custom.js') }}
  {{ HTML::script('assets/gantelella/js/pace/pace.min.js') }}

  @yield('footjs')

</body>

</html>
