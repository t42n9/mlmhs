<html>
    <head>
    {{ HTML::style('css/jquery.orgchart.css') }}
    {{ HTML::style('bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('assets/gantelella/fonts/css/font-awesome.min.css') }}
    {{ HTML::script('assets/gantelella/js/jquery.min.js') }}
    {{ HTML::script('js/jquery.orgchart.min.js') }}
    <style type="text/css">
        
        div.orgChart{
            margin: 0;
            background-color: #FFF;
            width: 100%;
        }
        
        @media (max-width: 910px){
            *{
                font-size: 1em;
            }
            div.orgChart{
                margin: 0;
                display: none;
            }
            div.alert{
                display: block;
            }
        }
        @media (min-width: 910px){
            div.alert{
                display: none;
            }
        }
        div.orgChart div.node{
            padding: 5px;
            word-wrap: break-word;
            vertical-align: middle;
            border-radius: 0px;
            box-shadow: 0px 0px 0px #000;
            border: 1px solid black;
            background-color: #fff;
        }
        div.orgChart td div.node:hover{
            cursor: pointer;
            box-shadow: 4px 4px 6px #888888;
        }
        div.orgChart tr.lines td.right{
            border-left: 2px solid black;
        }
        div.orgChart tr.lines td.left{
            border-right: 2px solid black;
        }
        div.orgChart tr.lines td.top{
            border-top: 1px solid black;
        }
        /*
        div.orgChart div.node.level0{
            background-color: #FFF;
        }
        div.orgChart div.node.level1{
            background-color: #FFF;
        }
        div.orgChart div.node.level2{
            background-color: #FFF;
        }
        div.orgChart div.node.level3{
            background-color: #FFF;
        }
        */
        .active-node {
            background-color: #4b6 !important;
        }

        .sponsor {
            font-size: 11px;
            display: block;
        }

        .active-sp {
            color: #4b6 !important;
        }        
    </style>
    </head>
    <body>
    <?php //    BOARD ?>
    
    @if (!empty($Data))

        <div class="alert" >Screen size minimum to see tree diagram : 910 x 410</div>

        <div id="board">
            <ul id="board-styles-source" class="hide">

                <li{{ ($Data->Bold == 1) ? ' class="active-node"' : '' }}>{{ $Data->Nama }}
                
                @if (!empty($Data->Child))

                    <ul>

                    @foreach($Data->Child as $dua)

                        <li{{ ($dua->Bold == 1) ? ' class="active-node"' : '' }}>{{ $dua->Nama }}

                        @if (!empty($dua->Child))

                            <ul>

                            @foreach($dua->Child as $tiga)

                                <li{{ ($tiga->Bold == 1) ? ' class="active-node"' : '' }}>{{ $tiga->Nama }}

                                    @if (!empty($tiga->Child))

                                        <ul>

                                        @foreach($tiga->Child as $empat)

                                            <li{{ ($empat->Bold == 1) ? ' class="active-node"' : '' }}>{{ $empat->Nama }}</li>

                                        @endforeach

                                        </ul>

                                    @endif

                                </li>

                            @endforeach
                                
                            </ul>

                        @endif

                        </li>

                    @endforeach

                    </ul>

                @endif
                
                </li>

            </ul>
        </div>

        <script type="text/javascript">
            $(function() {
                $("#board-styles-source").orgChart({container: $("#board")});
            });
        </script>

    @else
        <h1>Tidak Ada Data Board</h1>
    @endif

    <?php //    END BOARD ?>

    <?php //    BINARY ?>

    @if (!empty($Binary))

        <div id="binary">
            <ul id="binary-styles-source" class="hide">

                <li class="active-node">{{ $Binary[0]->nama }}
            
                <?php 
                    $binBaris   = $Binary[0]->baris_urut;
                    $myBinID    = $Binary[0]->id_member;
                    $jmlBin     = count($Binary);
                ?>

                @if ($jmlBin > 1)

                    @for($x = 1; $x < $jmlBin; $x++)

                        @if ($Binary[$x]->baris_urut > $binBaris)
                            
                            <ul>
                                <li>

                        @elseif ($Binary[$x]->baris_urut == $binBaris)
                            
                            </li><li>

                        @elseif ($Binary[$x]->baris_urut < $binBaris)
                            
                            <?php 
                                $jmlTutup = $binBaris - $Binary[$x]->baris_urut;
                            ?>
                            
                            {{ str_repeat('</li></ul>', $jmlTutup) }}
                            <li>
                        
                        @endif

                        <u>{{ $Binary[$x]->nama }}</u>

                        @if ($Binary[$x]->nama_sponsor != '')
                            <div class="sponsor {{ ($Binary[$x]->id_sponsor == $myBinID) ? 'active-sp' : '' }}">Sponsor :<br>{{ $Binary[$x]->nama_sponsor }}</div>
                        @endif

                        @if ($x >= $jmlBin - 1)
                            {{ str_repeat('</li></ul>', $binBaris - 1) }}
                        @endif

                        <?php 
                            $binBaris = $Binary[$x]->baris_urut;
                        ?>

                    @endfor

                    </li>

                @endif

                </li>

            </ul>
        </div>

        <script type="text/javascript">
            $(function() {
                $("#binary-styles-source").orgChart({container: $("#binary")});
            });
        </script>
    @else
        <h1>Tidak Ada Data Binary</h1>
    @endif

    <?php //    END BINARY ?>

    <?php //    MATRIX ?>

    @if (!empty($Matrix))

        <div id="matrix">
            <ul id="matrix-styles-source" class="hide">

                <li class="active-node">{{ $Matrix[0]->nama }}
            
                <?php 
                    $matBaris   = $Matrix[0]->baris_urut;
                    $myMatID    = $Matrix[0]->id_member;
                    $jmlMat     = count($Matrix);
                ?>

                @if ($jmlMat > 1)

                    @for($y = 1; $y < $jmlMat; $y++)

                        @if ($Matrix[$y]->baris_urut > $matBaris)
                            
                            <ul>
                                <li>

                        @elseif ($Matrix[$y]->baris_urut == $matBaris)
                            
                            </li><li>

                        @elseif ($Matrix[$y]->baris_urut < $matBaris)
                            
                            <?php 
                                $jmlTutup = $matBaris - $Matrix[$y]->baris_urut;
                            ?>
                            
                            {{ str_repeat('</li></ul>', $jmlTutup) }}
                            <li>
                        
                        @endif

                        <u>{{ $Matrix[$y]->nama }}</u>

                        @if ($Matrix[$y]->nama_sponsor != '')
                            <div class="sponsor {{ ($Matrix[$y]->id_sponsor == $myMatID) ? 'active-sp' : '' }}">Sponsor :<br>{{ $Matrix[$y]->nama_sponsor }}</div>
                        @endif

                        @if ($y >= $jmlMat - 1)
                            {{ str_repeat('</li></ul>', $matBaris - 1) }}
                        @endif

                        <?php 
                            $matBaris = $Matrix[$y]->baris_urut;
                        ?>

                    @endfor

                    </li>

                @endif

                </li>

            </ul>
        </div>

        <script type="text/javascript">
            $(function() {
                $("#matrix-styles-source").orgChart({container: $("#matrix")});
            });
        </script>
    @else
        <h1>Tidak Ada Data Matrix</h1>
    @endif

    <?php //    END MATRIX ?>
    </body>
</html>


