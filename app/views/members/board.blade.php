@extends('default')

@extends('menu')

@section('content')
    
    <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Plain Page</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12" style="width:100%;">
              <div class="x_panel" style="width:100%;">
                <div class="x_title">
                  <h2>General Board</h2>
                  <div class="clearfix"></div>
                </div>
                <iframe src="/member/board_plain/1" style="width:100%;min-height:400px;"></iframe>
              </div>
            </div>
        </div>

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">MLM-HS &copy; 2016
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

@stop


