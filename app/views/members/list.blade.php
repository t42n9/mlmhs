@extends('default')

@section('optioncss')

  {{ HTML::style('assets/gantelella/js/datatables/jquery.dataTables.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/buttons.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/fixedHeader.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/responsive.bootstrap.min.css') }}
  {{ HTML::style('assets/gantelella/js/datatables/scroller.bootstrap.min.css') }}

@stop

@extends('menu')

@section('content')

 <!-- page content -->
    <div class="right_col" role="main">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2>Daftar Member</h2>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30"><a href="/member/register" class="btn btn-default" title="Add Member">Tambah Member</a></p>
                    <table id="listmember" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Email</th>
                                <th>Nama</th>
                                <?php //<th>Sponsor</th> ?>
                            </tr>
                        </thead>
                        <?php $i = 1; ?>
                        <tbody>
                            @if (!empty($Data))
                                @foreach($Data as $row)
                                <tr>
                                    <td>{{ $i++ }}.</td>
                                    <?php //<td><a href="{{ URL::to('member/board_plain/' . $row->id) }}" target="_blank">{{ $row->kode }}</a></td> ?>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <?php //<td>{{ ($row->id_sponsor == 0) ? '-' : $row->nama_sponsor }}</td> ?>
                                </tr>
                                @endforeach
                            @else
                                <!--tr><td colspan="4">Data Masih Kosong</td></tr-->
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <?php //<td>&nbsp;</td> ?>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      <!-- /page content -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#listmember').dataTable(  );
        });
    </script>
@stop

@section('footjs')

    {{ HTML::script('assets/gantelella/js/datatables/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.bootstrap.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.buttons.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/jszip.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/pdfmake.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/vfs_fonts.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.html5.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/buttons.print.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.fixedHeader.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.keyTable.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.responsive.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/responsive.bootstrap.min.js') }}
    {{ HTML::script('assets/gantelella/js/datatables/dataTables.scroller.min.js') }}

@stop

