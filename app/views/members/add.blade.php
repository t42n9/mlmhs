@extends('default')

@section('optioncss')

    {{ HTML::style('css/jquery.orgchart.css') }}
    {{ HTML::style('assets/css/my-chart.css') }}

@stop

@extends('menu')

@section('content')

    <style type="text/css">
        .notif-error {
            color: red;
            font-size: 11px;
        }
    </style>

    <?php $erroran = array(); ?>

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title"><div class="title_left"><h3>Daftar Baru</h3></div></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12" style="width:100%;">
                <div class="x_panel" style="width:100%;">
                    <div class="x_title">
                        <h2>Tambah Member</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (Session::has('msgerror'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Maaf!</strong> {{ Session::get('msgerror') }}
                            </div>
                            @if (Session::has('errorData'))
                                <?php $erroran = Session::get('errorData'); ?>
                            @endif
                        @endif
                        {{ Form::open(array('route' => 'regpost', 'class' => 'form-horizontal form-label-left')) }}
                            <?php /*
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="spkode">Kode Sponsor <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{  Form::text('spkode', '', array('class' => 'form-control col-md-7 col-xs-12')) }}
                                    @if (isset($erroran['sponsor']))
                                        <p class="notif-error">{{ $erroran['sponsor'] }}</p>
                                    @endif
                                </div>
                            </div>
                            */ ?>
                            <?php //{{ Form::hidden('spkode', (!empty($User)) ? $User->kode : '-1') }} ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{  Form::text('nama', '', array('id' => 'nama', 'class' => 'form-control col-md-7 col-xs-12')) }}
                                    @if (isset($erroran['nama']))
                                        <p class="notif-error">{{ $erroran['nama'] }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Email <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{  Form::email('email', '', array('id' => 'email', 'class' => 'form-control col-md-7 col-xs-12')) }}
                                    @if (isset($erroran['email']))
                                        <p class="notif-error">{{ $erroran['email'] }}</p>
                                    @endif
                                </div>
                            </div>

                            @if (!empty($Binary))

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bmode">Posisi Struktur</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ Form::select('bmode', array(1 => 'Auto Left', 2 => 'Auto Right', 3 => 'Manual'), null, 
                                                    array('id' => 'bmode', 'class' => 'form-control col-md-7 col-xs-12')) }}
                                    @if (isset($erroran['posisi']))
                                        <p class="notif-error">{{ $erroran['posisi'] }}</p>
                                    @endif
                                </div>
                            </div>

                            {{ Form::hidden('selbaris', 0, array('id' => 'selbaris')) }}
                            {{ Form::hidden('selside', 0, array('id' => 'selside')) }}

                            <div class="row">
                                <div id="mychart" class="layer-chart">

                                    <ul id="mychart-styles-source" class="hide">

                                        <?php 
                                            $binBaris   = $Binary[0]->baris_urut;
                                            $myBinID    = $Binary[0]->id_member;
                                            $jmlBin     = count($Binary);
                                        ?>

                                        <li class="active-node nodeku available-auto">
                                            <div class="nama-member"
                                                data-baris="{{ $Binary[0]->baris_urut }}" 
                                                data-side="{{ $Binary[0]->side_urut }}">
                                                {{ $Binary[0]->nama }}
                                            </div>
                                    
                                        @if ($jmlBin > 1)

                                            @for($x = 1; $x < $jmlBin; $x++)

                                                <?php 
                                                    $row            = $Binary[$x];
                                                    $clsAvailable   = '';
                                                    $boleh          = ($row->bisa_auto == 1 || $row->bisa_manual == 1);
                                                    if ($row->bisa_auto == 1) {
                                                        $clsAvailable   = 'available-auto';
                                                    } elseif ($row->bisa_manual == 1) {
                                                        $clsAvailable   = 'available-manual';
                                                    }
                                                    
                                                ?>

                                                @if ($row->baris_urut > $binBaris)
                                                    
                                                    <ul>
                                                        <li class="nodeku {{ $clsAvailable }}">

                                                @elseif ($row->baris_urut == $binBaris)
                                                    
                                                    </li><li class="nodeku {{ $clsAvailable }}">

                                                @elseif ($row->baris_urut < $binBaris)
                                                    
                                                    <?php 
                                                        $jmlTutup = $binBaris - $row->baris_urut;
                                                    ?>
                                                    
                                                    {{ str_repeat('</li></ul>', $jmlTutup) }}
                                                    <li class="nodeku {{ $clsAvailable }}">
                                                
                                                @endif

                                                <div class="nama-member" 
                                                    @if ($boleh == true)
                                                    data-baris="{{ $row->baris_urut }}" 
                                                    data-side="{{ $row->side_urut }}"
                                                    @endif
                                                    >
                                                    {{ $row->nama }}
                                                </div>

                                                <?php /*
                                                @if ($row->nama_sponsor != '')
                                                    <div class="sponsor {{ ($row->id_sponsor == $myBinID) ? 'active-sp' : '' }}">Sponsor :<br>{{ $row->nama_sponsor }}</div>
                                                @endif
                                                */ ?>

                                                @if ($x >= $jmlBin - 1)
                                                    {{ str_repeat('</li></ul>', $binBaris - 1) }}
                                                @endif

                                                <?php 
                                                    $binBaris = $row->baris_urut;
                                                ?>

                                            @endfor

                                            </li>

                                        @endif

                                        </li>

                                    </ul>

                                </div>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $("#mychart-styles-source").orgChart({container: $("#mychart")});

                                    @if (!empty($Binary))
                                    
                                    function applyMode(o) {
                                        $(".nodeku").removeClass('clickable-node');
                                        $(".nodeku").removeClass('available-node');
                                        $(".nodeku").removeClass('active-select-node');
                                        $(".nodeku").unbind('click');
                                        var v = $(o).val();
                                        if (v == 3) {
                                            $(".available-manual").addClass('clickable-node');
                                            $(".available-manual").addClass('available-node');
                                        } else {
                                            $(".available-auto").addClass('clickable-node');
                                            $(".available-auto").addClass('available-node');
                                        }

                                        $("#selbaris").val(0);
                                        $("#selside").val(0);

                                        $(".clickable-node").click(function() {
                                            $(".nodeku").removeClass('active-select-node');
                                            $(this).addClass('active-select-node');

                                            var c = $(this).children('h2').first().children('div.nama-member').first();
                                            if (undefined != c) {
                                                $("#selbaris").val($(c).data('baris'));
                                                $("#selside").val($(c).data('side'));
                                            }
                                        });

                                        $(".clickable-node").first().click();
                                    }

                                    $("#bmode").change(function() {
                                        applyMode(this);
                                    });

                                    applyMode($("#bmode"));

                                    @endif
                                });
                            </script>

                            @endif

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    {{ Form::button('Simpan', array('class'=>'btn btn-default', 'type'=>'submit')) }}
                                    {{ Form::button('Batal', array('class'=>'btn btn-default', 'onclick' => "window.location.href='" . URL::to('member') . "'")) }}
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('footjs')

    {{ HTML::script('js/jquery.orgchart.min.js') }}

@stop


