@extends('default')

@section('optioncss')

    {{ HTML::style('css/jquery.orgchart.css') }}
    {{ HTML::style('assets/css/my-chart.css') }}
    
@stop

@extends('menu')

@section('content')

    <div class="right_col" role="main">
        <div class="page-title"><div class="title_left"><h3>Plain Page</h3></div></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="x_panel" style="width:100%;">
                <h2>{{ $siTitle }}</h2>
            </div>
            <div class="clearfix"></div>

            @if ($key === 'BOARD' || $key === 'BINARY' || $key === 'MATRIX')

            <div class="alert-chart" >Screen size minimum to see tree diagram : 910 x 410</div>

            <div id="mychart" class="layer-chart">

                <?php $AdaData = false; ?>

                @if ($key === 'BOARD')

                    @if (!empty($Data))

                    <?php $AdaData = true; ?>

                    <ul id="mychart-styles-source" class="hide">
                        <li{{ ($Data->Bold == 1) ? ' class="active-node"' : '' }}><div class="nama-member">{{ $Data->Nama }}</div>
                        
                        @if (!empty($Data->Child))

                            <ul>

                            @foreach($Data->Child as $dua)

                                <li{{ ($dua->Bold == 1) ? ' class="active-node"' : '' }}><div class="nama-member">{{ $dua->Nama }}</div>

                                @if (!empty($dua->Child))

                                    <ul>

                                    @foreach($dua->Child as $tiga)

                                        <li{{ ($tiga->Bold == 1) ? ' class="active-node"' : '' }}><div class="nama-member">{{ $tiga->Nama }}</div>

                                            @if (!empty($tiga->Child))

                                                <ul>

                                                @foreach($tiga->Child as $empat)

                                                    <li{{ ($empat->Bold == 1) ? ' class="active-node"' : '' }}><div class="nama-member">{{ $empat->Nama }}</div></li>

                                                @endforeach

                                                </ul>

                                            @endif

                                        </li>

                                    @endforeach
                                        
                                    </ul>

                                @endif

                                </li>

                            @endforeach

                            </ul>

                        @endif
                        
                        </li>

                    </ul>

                    @endif

                @elseif ($key === 'BINARY')

                    @if (!empty($Data->Data))

                    <?php $AdaData = true; ?>

                    <ul id="mychart-styles-source" class="hide">

                        <li {{ ($Data->isTop == true) ? 'class="active-node"' : '' }}>
                            <div class="nama-member">
                                @if ($Data->isTop == false)
                                    @if ($Data->isPriorTop == true)
                                        <a href="/struktur/binary">
                                            <span class="fa fa-arrow-up"></span> {{ $Data->Data[0]->nama }}
                                        </a>
                                    @else 
                                        <a href="/struktur/binary/{{ $Data->PriorBaris }}/{{ $Data->PriorSide }}">
                                            <span class="fa fa-arrow-up"></span> {{ $Data->Data[0]->nama }}
                                        </a>
                                    @endif
                                @else
                                    {{ $Data->Data[0]->nama }}
                                @endif
                            </div>
                    
                        <?php 
                            $Binary     = $Data->Data;
                            $binBaris   = $Binary[0]->baris_urut;
                            $myBinID    = $Binary[0]->id_member;
                            $jmlBin     = count($Binary);
                        ?>

                        @if ($jmlBin > 1)

                            @for($x = 1; $x < $jmlBin; $x++)

                                @if ($Binary[$x]->baris_urut > $binBaris)
                                    
                                    <ul>
                                        <li>

                                @elseif ($Binary[$x]->baris_urut == $binBaris)
                                    
                                    </li><li>

                                @elseif ($Binary[$x]->baris_urut < $binBaris)
                                    
                                    <?php 
                                        $jmlTutup = $binBaris - $Binary[$x]->baris_urut;
                                    ?>
                                    
                                    {{ str_repeat('</li></ul>', $jmlTutup) }}
                                    <li>
                                
                                @endif

                                <div class="nama-member">
                                    @if ($Binary[$x]->baris == 5)
                                        @if ($Binary[$x]->nama != '')
                                            <a href="/struktur/binary/{{ $Binary[$x]->baris_urut }}/{{ $Binary[$x]->side_urut }}">
                                                <span class="fa fa-arrow-down"></span> {{ $Binary[$x]->nama }}
                                            </a>
                                        @else
                                            {{ $Binary[$x]->nama }}
                                        @endif
                                    @else
                                        {{ $Binary[$x]->nama }}
                                    @endif
                                </div>

                                <?php /*
                                @if ($Binary[$x]->nama_sponsor != '')
                                    <div class="sponsor {{ ($Binary[$x]->id_sponsor == $myBinID) ? 'active-sp' : '' }}">Sponsor :<br>{{ $Binary[$x]->nama_sponsor }}</div>
                                @endif
                                */ ?>

                                @if ($x >= $jmlBin - 1)
                                    {{ str_repeat('</li></ul>', $binBaris - 1) }}
                                @endif

                                <?php 
                                    $binBaris = $Binary[$x]->baris_urut;
                                ?>

                            @endfor

                            </li>

                        @endif

                        </li>

                    </ul>

                    @endif

                @elseif ($key === 'MATRIX')

                    @if (!empty($Data))

                    <?php $AdaData = true; ?>

                    <ul id="mychart-styles-source" class="hide">

                        <li class="active-node"><div class="nama-member">{{ $Data[0]->nama }}</div>
                    
                        <?php 
                            $matBaris   = $Data[0]->baris_urut;
                            $myMatID    = $Data[0]->id_member;
                            $jmlMat     = count($Data);
                        ?>

                        @if ($jmlMat > 1)

                            @for($y = 1; $y < $jmlMat; $y++)

                                @if ($Data[$y]->baris_urut > $matBaris)
                                    
                                    <ul>
                                        <li>

                                @elseif ($Data[$y]->baris_urut == $matBaris)
                                    
                                    </li><li>

                                @elseif ($Data[$y]->baris_urut < $matBaris)
                                    
                                    <?php 
                                        $jmlTutup = $matBaris - $Data[$y]->baris_urut;
                                    ?>
                                    
                                    {{ str_repeat('</li></ul>', $jmlTutup) }}
                                    <li>
                                
                                @endif

                                <div class="nama-member">{{ $Data[$y]->nama }}</div>

                                <?php /*
                                @if ($Data[$y]->nama_sponsor != '')
                                    <div class="sponsor {{ ($Data[$y]->id_sponsor == $myMatID) ? 'active-sp' : '' }}">Sponsor :<br>{{ $Data[$y]->nama_sponsor }}</div>
                                @endif
                                */ ?>

                                @if ($y >= $jmlMat - 1)
                                    {{ str_repeat('</li></ul>', $matBaris - 1) }}
                                @endif

                                <?php 
                                    $matBaris = $Data[$y]->baris_urut;
                                ?>

                            @endfor

                            </li>

                        @endif

                        </li>

                    </ul>

                    @endif

                @endif

                @if ($AdaData == true)

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $("#mychart-styles-source").orgChart({container: $("#mychart")});
                        });
                    </script>

                @else
                    <h5>Tidak Ada Data Yang Dapat Ditampilkan.</h5>
                @endif

            </div>

            @else
                <h5>Tidak Ada Data Yang Dapat Ditampilkan.</h5>
            @endif

        </div>

        <footer>
            <div class="copyright-info">
                <p class="pull-right">{{ $CopyRight }}</p>
            </div>
            <div class="clearfix"></div>
        </footer>
    </div>

@stop

@section('footjs')

    {{ HTML::script('js/jquery.orgchart.min.js') }}

@stop

