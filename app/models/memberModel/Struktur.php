<?php

namespace memberModel;

use DB;

class Struktur {

    /*  Struktur Board */

    /*
    board type :
        1   => original
        2   => destination board untuk TOP board
    */

    private $MaxMemberBoard1    = 15;
    private $MaxMemberBoard2    = 7;

    private $startBarisBoard    = 1;
    private $maxBarisBoard1     = 4;
    private $maxBarisBoard2     = 3;

    private $maxKakiBoard       = 2;

    public function getCountBoardByMember($idMember) {
        $result = (object) array('idMember' => 0, 'Jumlah' => 0);
        $data   = DB::table('board_member')
                        ->selectRaw('id_member, COUNT(id) AS jml_board')
                        ->where('id_member', '=', $idMember)
                        ->groupBy('id_member')
                        ->first();
        if (empty($data)) return $result;
        $result->idMember   = $data->id_member;
        $result->Jumlah     = $data->jml_board;

        return $result;
    }

    private function BuildBoardArray($idMember, $totalBoard, $page, $satu, $dua, $tiga, $empat) {
        $result = null;
        if (!empty($satu)) {
            $result = (object) array('MemberID' => $idMember,
                                    'Page'      => $page,
                                    'TotalPage' => $totalBoard,
                                    'ID'        => $satu->id, 
                                    'idBoard'   => $satu->id_board, 
                                    'idMember'  => $satu->id, 
                                    'Nama'      => $satu->nama,
                                    'Bold'      => $satu->bold, 
                                    'Child'     => array());
            if (!empty($dua)) {
                foreach ($dua as $rdua) {
                    if ($rdua->id_parent == $satu->id) {
                        $result->Child[$rdua->id]   = (object) array('ID'       => $rdua->id, 
                                                                    'idBoard'   => $rdua->id_board, 
                                                                    'idMember'  => $rdua->id, 
                                                                    'Nama'      => $rdua->nama, 
                                                                    'Bold'      => $rdua->bold, 
                                                                    'Child'     => array());
                        if (!empty($tiga)) {
                            foreach ($tiga as $rtiga) {
                                if ($rtiga->id_parent == $rdua->id) {
                                    $result->Child[$rdua->id]->Child[$rtiga->id]    = (object) array('ID'       => $rtiga->id, 
                                                                                                    'idBoard'   => $rtiga->id_board, 
                                                                                                    'idMember'  => $rtiga->id, 
                                                                                                    'Nama'      => $rtiga->nama, 
                                                                                                    'Bold'      => $rtiga->bold, 
                                                                                                    'Child'     => array());
                                    if (!empty($empat)) {
                                        foreach ($empat as $rempat) {
                                            if ($rempat->id_parent == $rtiga->id) {
                                                $result->Child[$rdua->id]->Child[$rtiga->id]->Child[$rempat->id]    = (object) array('ID'       => $rempat->id, 
                                                                                                                                    'idBoard'   => $rempat->id_board, 
                                                                                                                                    'idMember'  => $rempat->id, 
                                                                                                                                    'Nama'      => $rempat->nama,
                                                                                                                                    'Bold'      => $rempat->bold);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    public function getBoardStructureByMember($idMember, $totalBoard, $page = 0) {
        $idBoard    = DB::table('board_member')->whereRaw('id_member = ' . $idMember)->orderBy('id')->offset($page)->take(1)->pluck('id_board');
        $satu       = DB::table('board_member')
                            ->join('members', 'members.id', '=', 'board_member.id_member')
                            ->leftJoin(DB::raw('members AS sp'), 'sp.id', '=', 'members.id_sponsor')
                            ->selectRaw("board_member.id, board_member.id_board, board_member.baris_urut, 
                                        board_member.id_parent, board_member.kaki_urut, board_member.id_member, members.nama,
                                        (CASE WHEN board_member.id_member = " . $idMember . " THEN 1 ELSE 0 END) AS bold,
                                        members.id_sponsor, sp.nama AS nama_sponsor")
                            ->where('id_board', '=', $idBoard)
                            ->where('baris_urut', '=', 1)
                            ->first();
        $dua        = DB::table('board_member') 
                            ->join('members', 'members.id', '=', 'board_member.id_member')
                            ->leftJoin(DB::raw('members AS sp'), 'sp.id', '=', 'members.id_sponsor')
                            ->selectRaw("board_member.id, board_member.id_board, board_member.baris_urut, 
                                        board_member.id_parent, board_member.kaki_urut, board_member.id_member, members.nama,
                                        (CASE WHEN board_member.id_member = " . $idMember . " THEN 1 ELSE 0 END) AS bold,
                                        members.id_sponsor, sp.nama AS nama_sponsor")
                            ->where('id_board', '=', $idBoard)
                            ->where('baris_urut', '=', 2)
                            ->orderBy('baris_urut')
                            ->orderBy('id_parent')
                            ->orderBy('kaki_urut')
                            ->get();
        $tiga       = DB::table('board_member')
                            ->join('members', 'members.id', '=', 'board_member.id_member')
                            ->leftJoin(DB::raw('members AS sp'), 'sp.id', '=', 'members.id_sponsor')
                            ->selectRaw("board_member.id, board_member.id_board, board_member.baris_urut, 
                                        board_member.id_parent, board_member.kaki_urut, board_member.id_member, members.nama,
                                        (CASE WHEN board_member.id_member = " . $idMember . " THEN 1 ELSE 0 END) AS bold,
                                        members.id_sponsor, sp.nama AS nama_sponsor")
                            ->where('id_board', '=', $idBoard)
                            ->where('baris_urut', '=', 3)
                            ->orderBy('baris_urut')
                            ->orderBy('id_parent')
                            ->orderBy('kaki_urut')
                            ->get();
        $empat      = DB::table('board_member')
                            ->join('members', 'members.id', '=', 'board_member.id_member')
                            ->leftJoin(DB::raw('members AS sp'), 'sp.id', '=', 'members.id_sponsor')
                            ->selectRaw("board_member.id, board_member.id_board, board_member.baris_urut, 
                                        board_member.id_parent, board_member.kaki_urut, board_member.id_member, members.nama,
                                        (CASE WHEN board_member.id_member = " . $idMember . " THEN 1 ELSE 0 END) AS bold,
                                        members.id_sponsor, sp.nama AS nama_sponsor")
                            ->where('id_board', '=', $idBoard)
                            ->where('baris_urut', '=', 4)
                            ->orderBy('baris_urut')
                            ->orderBy('id_parent')
                            ->orderBy('kaki_urut')
                            ->get();
        
        return  $this->BuildBoardArray($idMember, $totalBoard, $page, $satu, $dua, $tiga, $empat);
    }

    public function getOpenBoards($type = 1) {
        $data   = DB::table('board')
                    ->whereRaw('(jml_member < max_member)')
                    ->where('is_closed', '=', 0);
        if ($type == 1 || $type == 2) {
            $data   = $data->where('board_type', '=', $type);
        }
        return $data->orderBy('board_type')->orderBy('id')->get();
    }

    public function getBoardByMember($idMember) {
        return DB::table('board')
                        ->join('board_member', 'board_member.id_board', '=', 'board.id')
                        ->selectRaw('board.*, COALESCE((SELECT COUNT(id_board) FROM board_member WHERE id_board = board.id), 0) AS total_member')
                        ->where('board_member.id_member', '=', $idMember)
                        ->orderBy('board.id', 'desc')
                        ->first();
    }

    public function getBoardByID($idBoard) {
        return DB::table('board')
                        ->selectRaw('board.*, COALESCE((SELECT COUNT(id_board) FROM board_member WHERE id_board = board.id), 0) AS total_member')
                        ->where('board.id', '=', $idBoard)
                        ->first();
    }

    private function getBoardByFromID($idFrom, $type) {
        return DB::table('board')
                        ->selectRaw('board.*, COALESCE((SELECT COUNT(id_board) FROM board_member WHERE id_board = board.id), 0) AS total_member')
                        ->where('board.id_fromboard', '=', $idFrom)
                        ->where('board.board_type', '=', $type)
                        ->orderBy('board.id')
                        ->get();
    }

    private function getAvailableBoardByInheritance($idFrom, $type) {
        $test   = $this->getBoardByFromID($idFrom, $type);
        $result = (object) array('Ada' => false, 'Data' => null);
        if (empty($test)) return $result;

        $maxMember  = ($type == 2) ? $this->MaxMemberBoard2 : $this->MaxMemberBoard1;

        foreach ($test as $row) {
            if ($row->total_member < $maxMember) {
                $result->Ada    = true;
                $result->Data   = $row;
                break;
            } else {
                $terusCari = $this->getAvailableBoardByInheritance($row->id, $type);
                if ($terusCari->Ada == true) {
                    if ($terusCari->Data->total_member < $maxMember) {
                        $result->Ada    = true;
                        $result->Data   = $$terusCari->Data;
                        break;
                    }
                }
            }
        }
        return $result;
    }

    public function getSponsorBoardForRegister($idSponsor) {
        $curBoard   = $this->getBoardByMember($idSponsor);
        if (empty($curBoard)) return null;

        $type   = $curBoard->board_type;
        if ($type == 2) {
            //  1. cari sponsor tsb ketika menjadi top board
            $topBoard   = DB::table('board')
                                ->join('board_member', 'board_member.id_board', '=', 'board.id')
                                ->selectRaw('board.*')
                                ->where('board_member.id_member', '=', $idSponsor)
                                ->where('board_member.baris_urut', '=', 1)
                                ->where('board.board_type', '=', 1)
                                ->first();
            if (!empty($topBoard)) {
                //  2. cari dimana ada space (sepertinya akan selalu ke kiri hasilnya, hadeeuuuuhhh...)
                $hasil  = $this->getAvailableBoardByInheritance($topBoard->id, 1);
                if ($hasil->Ada == true) $curBoard = $hasil->Data;
            }
        }

        return $curBoard;
    }

    public function getParentBoard($boardData, $isNew = false) {
        $result = (object) array('Ada' => false, 'BoardID' => 0, 'ID' => 0, 'ParentBaris' => 0, 'ForUrutan' => 0, 'NextBaordID' => 0);
        if (empty($boardData)) return $result;
        $idBoard    = $boardData->id;
        $type       = $boardData->board_type;
        $parentData = DB::table('board_member')
                            ->leftJoin(DB::raw('board_member AS bm'), 'bm.id_parent', '=', 'board_member.id')
                            ->selectRaw('board_member.id, board_member.id_board, board_member.id_member, board_member.baris_urut, board_member.id_next_dest, ' 
                                        . $this->maxKakiBoard . ' - COUNT(bm.id) AS sisa_kaki')
                            ->where('board_member.id_board', '=', $idBoard)
                            ->where('board_member.baris_urut', '<', ($type == 2) ? $this->maxBarisBoard2 : $this->maxBarisBoard1)
                            ->groupBy('board_member.id')
                            ->groupBy('board_member.id_board')
                            ->groupBy('board_member.id_member')
                            ->groupBy('board_member.baris_urut')
                            ->groupBy('board_member.id_next_dest')
                            ->having('sisa_kaki', '>', 0)
                            ->orderBy('board_member.baris_urut')
                            ->orderBy('board_member.id_parent')
                            ->orderBy('board_member.kaki_urut')
                            ->first();

        if (empty($parentData)) {
            if ($isNew == true) {
                $result->Ada            = true;
                $result->BoardID        = $idBoard;
                $result->ID             = 0;
                $result->ParentBaris    = 1;
                $result->NextBaordID    = 0;
                $result->ForUrutan      = 1;
            }
        } else {
            $result->Ada            = true;
            $result->BoardID        = $idBoard;
            $result->ID             = $parentData->id;
            $result->ParentBaris    = $parentData->baris_urut + 1;
            $result->NextBaordID    = $parentData->id_next_dest;
            $result->ForUrutan      = $this->maxKakiBoard - $parentData->sisa_kaki + 1;
        }

        return $result;
    }

    public function createNewBoard($fromBoardID, $type = 1) {
        $result = (object) array('Success' => false, 'Data' => null);
        if ($type > 2 || $type < 1) return $result;
        $data   = array('board_type'    => $type,
                        'max_member'    => ($type == 2) ? $this->MaxMemberBoard2 : $this->MaxMemberBoard1,
                        'jml_member'    => 0,
                        'is_closed'     => 0,
                        'id_fromboard'  => $fromBoardID);
        try {
            $id             = DB::table('board')->insertGetId($data);
            $data['id']     = $id;
            $data['total_member'] = 0;
            $result->Data   = (object) $data;
            $result->Success    = true;
        } catch (\Exception $e) {
            dd('Error createNewBoard : ' . $e->getMessage());
            $result->Success    = false;
        }
        return $result;
    }

    private function UpdateBoard($idBoard, $data) {
        if (empty($data) || !is_array($data)) return false;
        try {
            DB::table('board')->where('id', '=', $idBoard)->update($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function getMemberBoard($boardID) {
        return DB::table('board_member')
                        ->where('id_board', '=', $boardID)
                        ->orderBy('baris_urut')
                        ->orderBy('kaki_urut')
                        ->get();
    }

    private function getBoardBonusList($BoardID, $Baris) {
        $data   = DB::table('board_member')
                        ->join('wallet', 'wallet.id_member', '=', 'board_member.id_member')
                        ->selectRaw('board_member.*,
                                    wallet.id AS id_wallet, wallet.nomor AS nomor_wallet, wallet.debet, wallet.kredit, wallet.saldo')
                        ->where('board_member.id_board', '=', $BoardID)
                        ->where('board_member.baris_urut', '<', $Baris)
                        ->get();

        $result = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                $result[$row->id_member] = $row;
            }
        }

        return $result;
    }

    private function AddMemberToBoard($boardData, $Barisan, $Urutan, $memberID, $ParentID, $LastBoardID, $nextDestID, $includeBonusList = false) {
        $result = (object) array('Success' => false, 'Board' => $boardData, 'Penuh' => false, 'BoardMemberID' => 0, 'MemberID' => $memberID, 'BonusList' => null);

        $mBoard     = array('id_board'      => $boardData->id,
                            'id_member'     => $memberID,
                            'baris_urut'    => $Barisan,
                            'id_parent'     => $ParentID,
                            'kaki_urut'     => $Urutan,
                            'id_lastboard'  => $LastBoardID,
                            'id_next_dest'  => $nextDestID);

        try {
            
            $newID  = DB::table('board_member')->insertGetId($mBoard);

            $result->BoardMemberID  = $newID;
            $result->Penuh          = ($boardData->total_member + 1 >= $boardData->max_member);
            
            $updBoard   = array('jml_member'    => $boardData->total_member + 1,
                                'is_closed'     => ($result->Penuh == true) ? 1 : 0);
            
            $boardData->jml_member = $updBoard['jml_member'];
            $boardData->is_closed  = $updBoard['is_closed'];

            if ($result->Penuh == true) {
                $updBoard['closed_at']  = date('Y-m-d H:i:s');
                $boardData->closed_at   = $updBoard['closed_at'];
            }

            DB::table('board')->where('id', '=', $boardData->id)->update($updBoard);

            $result->Board      = $boardData;

            $result->Success    = true;

            if ($includeBonusList === true && $boardData->board_type == 1) $result->BonusList  = $this->getBoardBonusList($boardData->id, $Barisan);
            
        } catch (\Exception $e) {
            //dd('Error AddMemberToBoard : ' . $e->getMessage());
            $result->BoardMemberID  = 0;
            $result->Penuh          = false;
            $result->Success        = false;
        }

        return $result;
    }

    public function RegisterMemberToBoard($memberID, $boardData, $parentBoard, $LastBoardID = 0, $includeBonusList = false) {
        if (empty($boardData) || $parentBoard->Ada == false) return false;
        
        $ParentID   = $parentBoard->ID;
        $Barisan    = $parentBoard->ParentBaris;
        $Urutan     = $parentBoard->ForUrutan;
        $NextBoard  = $parentBoard->NextBaordID;

        return $this->AddMemberToBoard($boardData, $Barisan, $Urutan, $memberID, $ParentID, $LastBoardID, $NextBoard, $includeBonusList);
    }

    public function DeleteBoard($id) {
        DB::table('board')->where('id', '=', $id)->delete();
    }

    public function getRankedMemberByBoard($boardData, $startBarisBoard = 2) {
        if (empty($boardData)) return false;
        
        $idBoard    = $boardData->id;
        
        $result     = DB::table('board')
                            ->join('board_member', 'board_member.id_board', '=', 'board.id')
                            ->join('members', 'members.id', '=', 'board_member.id_member')
                            ->leftJoin(DB::raw('members AS spku'), 'spku.id', '=', 'members.id_sponsor')
                            ->leftJoin(DB::raw('members AS sponsored'), function($join) use($boardData) {
                                $isClosed   = ($boardData->is_closed == 1);
                                $tglStart   = date('Y-m-d H:i:s', strtotime($boardData->created_at));
                                $tglFinish  = ($isClosed == true && !empty($boardData->closed_at)) ? date('Y-m-d H:i:s', strtotime($boardData->closed_at)) : date('Y-m-d H:i:s');
                                $join->on('sponsored.id_sponsor', '=', 'board_member.id_member')
                                    ->where('sponsored.registered_at', '>=', $tglStart)
                                    ->where('sponsored.registered_at', '<=', $tglFinish);
                            })
                            ->selectRaw('board_member.id, board_member.id_board, board.board_type, board_member.id_member, board_member.baris_urut,
                                        board_member.id_parent, board_member.kaki_urut, board_member.id_lastboard, board_member.id_next_dest,
                                        members.nama, members.id_sponsor, spku.nama AS nama_sponsor, members.registered_at,
                                        COUNT(sponsored.id) AS jml_recruited, MAX(COALESCE(sponsored.registered_at, CURRENT_TIMESTAMP)) AS last_recruit')
                            ->where('board.id', '=', $idBoard)
                            ->where('board_member.baris_urut', '>=', $startBarisBoard)
                            ->groupBy('board_member.id')
                            ->groupBy('board_member.id_board')
                            ->orderBy('jml_recruited', 'desc')
                            ->orderBy('last_recruit')
                            ->orderBy('members.registered_at')
                            ->get();

        return $result;
    }

    private function RegisterToBoard2($oldBoardMember, $idMember) {

        $result = (object) array('Success' => false, 'Board' => null, 'Penuh' => false, 'BoardMemberID' => 0, 'MemberID' => $idMember);

        if (empty($oldBoardMember)) return $result;

        $result->Board   = $this->getBoardByID($oldBoardMember->id_next_dest);

        $isNewBoard     = false;
        if (empty($result->Board)) {
            $newBoard       = $this->createNewBoard($oldBoardMember->id_board, 2);
            if ($newBoard->Success == false) return false;
            $result->Board  = $newBoard->Data;
            $isNewBoard     = true;
        }

        $parentBoard = $this->getParentBoard($result->Board, $isNewBoard);

        return $this->RegisterMemberToBoard($idMember, $result->Board, $parentBoard, $oldBoardMember->id_board);
    }

    private function setupExpandingBoard($r, $data, &$var) {
        $kelompoks  = (object) array('Kiri' => array(1, 3, 4, 7, 8, 9, 10), 
                                    'Kanan' => array(2, 5, 6, 11, 12, 13, 14));

        if (!in_array($r, $kelompoks->Kiri) && !in_array($r, $kelompoks->Kanan)) return false;
        
        $posisi     = (in_array($r, $kelompoks->Kanan)) ? 'Kanan' : 'Kiri';
        $group      = $kelompoks->{$posisi};
        $keyIndex   = array_search($r, $group);
        if ($keyIndex === false) return false;

        $keyIndex   = $keyIndex + 1;
        
        if ($keyIndex == 1) {
            $var->{$posisi} = (object) array('idBoardMember'    => 0,
                                            'dataInsert'        => array('id_board'     => 0,
                                                                        'id_member'     => $data->id_member,
                                                                        'baris_urut'    => 1,
                                                                        'id_parent'     => 0,
                                                                        'kaki_urut'     => 1,
                                                                        'id_lastboard'  => $data->id_board,
                                                                        'id_next_dest'  => 0),
                                            'Ranking'           => $r,
                                            'Child'             => array());
        } else {
            $p  = 1;    //  parent
            $k  = 1;    //  kaki (1 = kiri, 2 = kanan)
            $b  = 2;    //  baris
            $idParent   = 1;

            if ($keyIndex == 3) {
                $k = 2;
            } elseif ($keyIndex == 4 || $keyIndex == 5) {
                $p  = 2;
                $b  = 3;
                if ($keyIndex == 5) $k = 2;
            } elseif ($keyIndex == 6 || $keyIndex == 7) {
                $p  = 3;
                $b  = 3;
                if ($keyIndex == 7) $k = 2;
            }
            if ($p == 2) {
                $var->{$posisi}->Child[1]->Child[$k]    = (object) array('idBoardMember'    => 0,
                                                                        'dataInsert'        => array('id_board'     => 0,
                                                                                                    'id_member'     => $data->id_member,
                                                                                                    'baris_urut'    => $b,
                                                                                                    'id_parent'     => $p,
                                                                                                    'kaki_urut'     => $k,
                                                                                                    'id_lastboard'  => $data->id_board,
                                                                                                    'id_next_dest'  => 0),
                                                                        'Ranking'           => $r,
                                                                        'Child'             => array());
            } elseif ($p == 3) {
                $var->{$posisi}->Child[2]->Child[$k]    = (object) array('idBoardMember'    => 0,
                                                                        'dataInsert'        => array('id_board'     => 0,
                                                                                                    'id_member'     => $data->id_member,
                                                                                                    'baris_urut'    => $b,
                                                                                                    'id_parent'     => $p,
                                                                                                    'kaki_urut'     => $k,
                                                                                                    'id_lastboard'  => $data->id_board,
                                                                                                    'id_next_dest'  => 0),
                                                                        'Ranking'           => $r,
                                                                        'Child'             => array());
            } else {
                $var->{$posisi}->Child[$k]  = (object) array('idBoardMember'    => 0,
                                                            'dataInsert'        => array('id_board'     => 0,
                                                                                        'id_member'     => $data->id_member,
                                                                                        'baris_urut'    => $b,
                                                                                        'id_parent'     => $p,
                                                                                        'kaki_urut'     => $k,
                                                                                        'id_lastboard'  => $data->id_board,
                                                                                        'id_next_dest'  => 0),
                                                            'Ranking'           => $r,
                                                            'Child'             => array());
            }
        }
    }

    private function InsertExpandedBoard($newBoard, &$Board) {
        try {
            //  line 1
            $jml                        = 1;
            $data1                      = $Board->dataInsert;
            $data1['id_board']          = $newBoard->id;
            $Board->idBoardMember       = DB::table('board_member')->insertGetId($data1);
            //  line 2a
            $jml++;
            $data2a                     = $Board->Child[1]->dataInsert;
            $data2a['id_board']         = $newBoard->id;
            $data2a['id_parent']        = $Board->idBoardMember;
            $Board->Child[1]->idBoardMember = DB::table('board_member')->insertGetId($data2a);
            //  line 2b
            $jml++;
            $data2b                     = $Board->Child[2]->dataInsert;
            $data2b['id_board']         = $newBoard->id;
            $data2b['id_parent']        = $Board->idBoardMember;
            $Board->Child[2]->idBoardMember = DB::table('board_member')->insertGetId($data2b);
            //  line 3 dari 2a
            if (!empty($Board->Child[1]->Child)) {
                //  line 3aa
                $jml++;
                $data3aa                    = $Board->Child[1]->Child[1]->dataInsert;
                $data3aa['id_board']        = $newBoard->id;
                $data3aa['id_parent']       = $Board->Child[1]->idBoardMember;
                $Board->Child[1]->Child[1]->idBoardMember = DB::table('board_member')->insertGetId($data3aa);
                //  line 3ab
                $jml++;
                $data3ab                    = $Board->Child[1]->Child[2]->dataInsert;
                $data3ab['id_board']        = $newBoard->id;
                $data3ab['id_parent']       = $Board->Child[1]->idBoardMember;
                $Board->Child[1]->Child[2]->idBoardMember = DB::table('board_member')->insertGetId($data3ab);
                //  line 3ba
                $jml++;
                $data3ba                    = $Board->Child[2]->Child[1]->dataInsert;
                $data3ba['id_board']        = $newBoard->id;
                $data3ba['id_parent']       = $Board->Child[2]->idBoardMember;
                $Board->Child[2]->Child[1]->idBoardMember = DB::table('board_member')->insertGetId($data3ba);
                //  line 3bb
                $jml++;
                $data3bb                    = $Board->Child[2]->Child[2]->dataInsert;
                $data3bb['id_board']        = $newBoard->id;
                $data3bb['id_parent']       = $Board->Child[2]->idBoardMember;
                $Board->Child[2]->Child[2]->idBoardMember = DB::table('board_member')->insertGetId($data3bb);
            }
            
            $updBoard   = array('jml_member'    => $jml,
                                'is_closed'     => 0);
            DB::table('board')->where('id', '=', $newBoard->id)->update($updBoard);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function ProsesExpandBoard($oldBoardID, $type, $dataStruktur) {
        $createKiri = $this->createNewBoard($oldBoardID, $type);
        if ($createKiri->Success == false) return false;

        $sukses1    = $this->InsertExpandedBoard($createKiri->Data, $dataStruktur->Kiri);

        $createKanan= $this->createNewBoard($oldBoardID, $type);
        if ($createKanan->Success == false) return false;

        $sukses2    = $this->InsertExpandedBoard($createKanan->Data, $dataStruktur->Kanan);

        return ($sukses1 == true && $sukses2 == true);
    }

    private function UpdateDestOfExpandedBoard($oldTopMemberID, $oldBoardID) {
        $newOldTopBoard = DB::table('board_member')
                                ->join('board', 'board.id', '=', 'board_member.id_board')
                                ->selectRaw('board_member.id, board_member.id_board, board_member.id_member')
                                ->where('board_member.id_member', '=', $oldTopMemberID)
                                ->where('board.board_type', '=', 2)
                                ->whereRaw('(board.max_member - board.jml_member > 0)')
                                ->orderBy('board_member.id', 'desc')
                                ->first();
        if (empty($newOldTopBoard)) return false;
        $idDest = $newOldTopBoard->id_board;
        try {
            DB::table('board_member')
                        ->where('id_lastboard', '=', $oldBoardID)
                        ->where('baris_urut', '=', 1)
                        ->where('id_member', '<>', $oldTopMemberID)
                        ->update(array('id_next_dest' => $idDest));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function getTopBoardData($idBoard, $type) {
        if ($type == 1) {
            return  DB::table('board_member')
                        ->join('members', 'members.id', '=', 'board_member.id_member')
                        ->join('wallet', 'wallet.id_member', '=', 'board_member.id_member')
                        ->selectRaw('board_member.*, members.id_sponsor, members.nama,
                                    wallet.id AS id_wallet, wallet.nomor AS nomor_wallet, wallet.debet, wallet.kredit, wallet.saldo')
                        ->where('board_member.id_board', '=', $idBoard)
                        ->where('board_member.baris_urut', '=', 1)
                        ->first();
        } elseif ($type == 2) {
            return  DB::table('board_member')
                            ->join('members', 'members.id', '=', 'board_member.id_member')
                            ->join('wallet', 'wallet.id_member', '=', 'board_member.id_member')
                            ->leftJoin(DB::table('wallet AS wsp'), 'wsp.id_member', '=', 'members.id_sponsor')
                            ->selectRaw('board_member.*, members.id_sponsor, members.nama,
                                        wallet.id AS id_wallet, wallet.nomor AS nomor_wallet, wallet.debet, wallet.kredit, wallet.saldo,
                                        wsp.id AS id_wallet_sp, wsp.nomor AS nomor_wallet_sp, wsp.debet_sp, wsp.kredit_sp, wsp.saldo_sp')
                            ->where('board_member.id_board', '=', $idBoard)
                            ->where('board_member.baris_urut', '=', 1)
                            ->first();
        }

        return null;
    }

    private function getFirstLineBoard2($idBoard) {
        return DB::table('board_member')
                            ->join('wallet', 'wallet.id_member', '=', 'board_member.id_member')
                            ->selectRaw('board_member.*, 
                                        wallet.id AS id_wallet, wallet.nomor AS nomor_wallet, wallet.debet, wallet.kredit, wallet.saldo')
                            ->where('board_member.id_board', '=', $idBoard)
                            ->where('board_member.baris_urut', '=', 2)
                            ->get();

    }

    public function ExpandBoard($boardData, &$BonusExpand) {

        if (empty($boardData)) return false;

        if ($boardData->is_closed == 0) return false;
        if (empty($boardData->closed_at)) return false;

        $type           = $boardData->board_type;

        $topData        = $this->getTopBoardData($boardData->id, $type);

        if (empty($topData)) return false;

        $rankedData     = $this->getRankedMemberByBoard($boardData, 2);

        if (empty($rankedData)) return false;

        $jmlRow         = count($rankedData);
        
        $minData        = ($type == 2) ? $this->MaxMemberBoard2 : $this->MaxMemberBoard1;

        if ($jmlRow + 1 < $minData) return false;

        //  define bonus object
        if (!is_object($BonusExpand)) {
            $BonusExpand = (object) array('FlyBoard1To2'    => array(),
                                        'FlyBoard2To2'      => array()
                                        );
        }

        //  destination dulu
        $destBoard      = $this->RegisterToBoard2($topData, $topData->id_member);

        if ($destBoard->Success == false) return false;

        if ($type == 1) {       // board 1
            $WalletMember   = (object) array('id'       => $topData->id_wallet,
                                            'id_member' => $topData->id_member,
                                            'nomor'     => $topData->nomor_wallet,
                                            'debet'     => $topData->debet,
                                            'kredit'    => $topData->kredit,
                                            'saldo'     => $topData->saldo);

            $BonusExpand->FlyBoard1To2[]    = (object) array('idMember'     => $topData->id_member,
                                                            'namaMember'    => $topData->nama,
                                                            'idNewBoard'    => $destBoard->Board->id,
                                                            'WalletData'    => $WalletMember);
        } elseif ($type == 2) {
            $WalletMember   = (object) array('id'       => $topData->id_wallet,
                                            'id_member' => $topData->id_member,
                                            'nomor'     => $topData->nomor_wallet,
                                            'debet'     => $topData->debet,
                                            'kredit'    => $topData->kredit,
                                            'saldo'     => $topData->saldo);

            $WalletSponsor  = (object) array('id'       => $topData->id_wallet_sp,
                                            'id_member' => $topData->id_member_sp,
                                            'nomor'     => $topData->nomor_wallet_sp,
                                            'debet'     => $topData->debet_sp,
                                            'kredit'    => $topData->kredit_sp,
                                            'saldo'     => $topData->saldo_sp);

            $firstLineData  = $this->getFirstLineBoard2($boardData->id);

            $WalletFirstLine    = array();
            if (!empty($firstLineData)) {
                foreach ($$firstLineData as $row) {
                    $WalletFirstLine[$row->id_member]   = (object) array('oldBoardID'   => $boardData->id,
                                                                        'WalletData'    => (object) array('id'       => $topData->id_wallet,
                                                                                                        'id_member' => $topData->id_member,
                                                                                                        'nomor'     => $topData->nomor_wallet,
                                                                                                        'debet'     => $topData->debet,
                                                                                                        'kredit'    => $topData->kredit,
                                                                                                        'saldo'     => $topData->saldo)
                                                                        );
                }
            }

            $BonusExpand->FlyBoard2To2[]    = (object) array('idMember'         => $topData->id_member,
                                                            'idNewBoard'        => $destBoard->Board->id,
                                                            'namaMember'        => $topData->nama,
                                                            'idSponsor'         => $topData->id_sponsor,
                                                            'WalletMember'      => $WalletMember,
                                                            'WalletSponsor'     => $WalletSponsor,
                                                            'WalletFirstLine'   => $WalletFirstLine);
        }

        $expandLagi     = $destBoard->Penuh;
        $Lanjut         = true;
        if ($destBoard->Penuh == true) {
            $Lanjut     = $this->ExpandBoard($Wallet, $destBoard->Board, $BonusExpand);
        }

        if ($Lanjut == false) return false;
        
        //  setelah destination
        $i = 1;
        $m = 1;
        $n = 1;

        $dataStruktur   = (object) array('Kiri' => null, 'Kanan' => null);

        foreach ($rankedData as $row) {
            $this->setupExpandingBoard($i, $row, $dataStruktur);
            $i++;
        }

        //  kita urus yg kiri dulu
        $Sukses = $this->ProsesExpandBoard($boardData->id, $type, $dataStruktur);

        if ($Sukses == true) {
            $Sukses = $this->UpdateDestOfExpandedBoard($topData->id_member, $boardData->id);
        }

        return $Sukses;
    }

    /*  End Struktur Board */

    /*  Struktur Binary */

    private $maxKakiBinary  = 2;
/*
    public function getBinaryStructure($idMember) {
        return DB::table(DB::raw('binary_struktur AS anak'))
                        ->join('members', 'members.id', '=', 'anak.id_member')
                        ->leftJoin(DB::raw('members as sp'), 'sp.id', '=', 'members.id_sponsor')
                        ->rightJoin(DB::raw('binary_struktur AS saya'), 'anak.kode', 'LIKE', DB::raw("CONCAT(saya.kode, '%')"))
                        ->selectRaw('anak.id, anak.id_parent, anak.baris_urut, anak.kaki_urut, anak.kode, anak.id_member,
                                    members.nama, members.id_sponsor, sp.nama AS nama_sponsor,
                                    (SELECT COUNT(id) FROM binary_struktur WHERE id_parent = anak.id) AS jml_kaki')
                        ->where('saya.id_member', '=', $idMember)
                        ->orderBy('anak.kode')
                        ->get();
    }
*/
    public function getBinaryStructure2($idMember) {
        $dataBin    = DB::table('binary_struktur')
                            ->where('id_member', '=', $idMember)
                            ->first();

        if (empty($dataBin)) return null;

        $kode       = $dataBin->kode;

        $qBinary    =  "SELECT DISTINCT tmp.baris AS baris_urut, tmp.kaki AS kaki_urut, tmp.side AS side_urut, tmp.kode, bs.id_member, m.nama,
                                bs.id, bs.id_parent, m.id_sponsor, sp.nama AS nama_sponsor,
                                ps.kode AS parent_kode, 
                                (CASE WHEN ps.kode IS NULL THEN 0 ELSE (CASE WHEN bs.kode IS NULL THEN 1 ELSE 0 END) END) AS bisa_manual,
                                (CASE WHEN bs.kode IS NOT NULL THEN 1 ELSE 0 END) AS bisa_auto
                        FROM
                            (SELECT a.baris, a.max_elemen,
                                @nomor := (CASE WHEN @cb < a.baris THEN 1 ELSE @nomor + 1 END) AS side,
                                @cb := a.baris AS tmpbarisan,
                                (CASE WHEN MOD(@nomor, 2) > 0 THEN 1 ELSE 2 END) AS kaki,
                                @kodeku := getTempBinaryCode(a.baris, @nomor) AS kode
                            FROM
                                (SELECT vbinary_baris.baris, vbinary_baris.max_elemen, vbinary_kaki.urut
                                FROM 
                                    vbinary_baris, vbinary_kaki
                                ) AS a
                                LEFT JOIN 
                                (SELECT vbinary_baris.baris, vbinary_kaki.urut
                                FROM 
                                    vbinary_baris, vbinary_kaki
                                ) AS b
                                    ON b.baris <= a.baris
                                LEFT JOIN
                                (SELECT vbinary_baris.baris, vbinary_kaki.urut
                                FROM 
                                    vbinary_baris, vbinary_kaki
                                ) AS c
                                    ON c.baris <= b.baris,
                                (SELECT @nomor := 0, @cb := 0, @kodeku := '') AS v
                            HAVING (side <= a.max_elemen)
                            ORDER BY a.baris, side
                            ) AS tmp
                            LEFT JOIN 
                            binary_struktur AS bs ON bs.kode = tmp.kode
                            LEFT JOIN 
                            binary_struktur AS ps ON ps.kode = REVERSE(SUBSTR(REVERSE(tmp.kode), LOCATE('-', REVERSE(tmp.kode)) + 1))
                            LEFT JOIN members AS m ON m.id = bs.id_member
                            LEFT JOIN members AS sp ON sp.id = m.id_sponsor
                        WHERE
                            (tmp.kode LIKE '" . $kode . "%')
                            AND (tmp.baris <= COALESCE((SELECT MAX(baris_urut) + 1 FROM binary_struktur WHERE kode LIKE '" . $kode . "%'), 0) + 1)

                        ORDER BY tmp.kode";

        return DB::select($qBinary);
    }

    public function getMyBinary($idMember) {
        return DB::table('binary_struktur')
                        ->where('id_member', '=', $idMember)
                        ->first();
    }

    public function getBinaryByKode($kode) {
        return DB::table('binary_struktur')
                        ->where('kode', '=', $kode)
                        ->first();
    }

    public function getBinaryStructure3($OwnerKode, $OwnerBaris, $OwnerSide, $TargetBaris, $TargetSide) {
        $result = (object) array('Data'         => null, 
                                'isTop'         => false, 
                                'PriorBaris'    => $TargetBaris, 
                                'PriorSide'     => $TargetBaris,
                                'isPriorTop'    => false, 
                                'Message'       => '');

        //  cek apakah struktur yg dipilih benar-benar ada di struktur si owner struktur binary ?
        $Target = DB::table('binary_struktur')
                        ->where('kode', 'LIKE', $OwnerKode . '%')
                        ->where('baris_urut', '=', $TargetBaris)
                        ->where('side_urut', '=', $TargetSide)
                        ->first();

        if (empty($Target)) {
            $result->Message    = 'Data tidak ditemukan.';
        } else {

            $result->isTop      = ($OwnerKode == $Target->kode);

            $sql    = "SELECT tempb.baris, tempb.side, tempb.baris_urut, tempb.side_urut, tempb.kaki_urut, bs.id, tempb.kode, 
                            bs.id_member, mb.nama,
                                bs.id, bs.id_parent, mb.id_sponsor, sp.nama AS nama_sponsor,
                                ps.kode AS parent_kode, 
                                (CASE WHEN ps.kode IS NULL THEN 0 ELSE (CASE WHEN bs.kode IS NULL THEN 1 ELSE 0 END) END) AS bisa_manual,
                                (CASE WHEN bs.kode IS NOT NULL THEN 1 ELSE 0 END) AS bisa_auto
                        FROM
                            (SELECT
                                @s := (CASE WHEN @b < qb1.br THEN 1 ELSE @s + 1 END) AS side,
                                @b := qb1.br AS baris,
                                qb1.ms AS max_side,
                                @tb := CAST((@vb - 1 + qb1.br) AS UNSIGNED) AS baris_urut,
                                @tts := (CASE WHEN qb1.br = 1 THEN @tts 
                                         ELSE 
                                            (CASE WHEN @s = 1 THEN @tts * 2 - 1 ELSE @tts END) 
                                         END) AS tmp_side,
                                @ts := CAST((CASE WHEN @s = 1 THEN @tts ELSE @ts + 1 END) AS UNSIGNED) AS side_urut,
                                CAST((CASE WHEN MOD(@ts, 2) > 0 THEN 1 ELSE 2 END) AS UNSIGNED) AS kaki_urut,
                                @kodeku := getTempBinaryCode(@tb, @ts) AS kode
                            FROM
                                (SELECT q.br, k.kk, POW(2, q.br - 1) AS ms
                                 FROM
                                    (SELECT 1 AS br UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5) AS q,
                                    (SELECT 1 AS kk UNION ALL SELECT 2) AS k
                                ) AS qb1
                                LEFT JOIN
                                (SELECT q.br, k.kk
                                 FROM
                                    (SELECT 1 AS br UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5) AS q,
                                    (SELECT 1 AS kk UNION ALL SELECT 2) AS k
                                ) AS qb2 ON qb2.br <= qb1.br,
                                (SELECT @b := 0, @s := 0, @vb := $TargetBaris, @vs := $TargetSide, @tb := $TargetBaris, @tts := $TargetSide, @ts := 0, @kodeku := '') AS pr
                            WHERE (@s < qb1.ms)
                            ) AS tempb
                            LEFT JOIN
                            binary_struktur AS bs ON bs.baris_urut = tempb.baris_urut AND bs.side_urut = tempb.side_urut
                            LEFT JOIN 
                            binary_struktur AS ps ON ps.kode = REVERSE(SUBSTR(REVERSE(tempb.kode), LOCATE('-', REVERSE(tempb.kode)) + 1))
                            LEFT JOIN
                            members AS mb ON mb.id = bs.id_member
                            LEFT JOIN members AS m ON m.id = bs.id_member
                            LEFT JOIN members AS sp ON sp.id = mb.id_sponsor
                        ORDER BY tempb.kode";

            $result->Data   = DB::select($sql);

            if (empty($result->Data)) {
                $result->Message = 'Data tidak ditemukan.';
            } else {
                if ($result->isTop == false) {
                    $splitKode  = explode('-', $Target->kode);
                    $PriorBaris = $Target->baris_urut - 4;
                    $PriorKode  = $splitKode[0];
                    for ($i = 1; $i <= $PriorBaris; $i++) {
                        $PriorKode .= '-' . $splitKode[$i];
                    }
                    
                    $dataPrior  = DB::table('binary_struktur')->where('kode', '=', $PriorKode)->first();
                    if (!empty($dataPrior)) {
                        $result->PriorBaris = $dataPrior->baris_urut;
                        $result->PriorSide  = $dataPrior->side_urut;
                        $result->isPriorTop = ($dataPrior->baris_urut == $OwnerBaris && $dataPrior->side_urut == $OwnerSide);
                    }
                }
            }
        }

        return $result;
    }

    public function getBinaryDataBySide($baris, $side) {
        return DB::table('binary_struktur')
                        ->where('baris_urut', '=', $baris)
                        ->where('side_urut', '=', $side)
                        ->first();
    }
/*
    public function getBinarySpaceByParent($idSponsor, $Direction = 1) {
        $result = (object) array('ID' => 0, 'Kode' => '', 'Baris' => 0, 'Kaki' => 0, 'SideNo' => 0, 'UsedSide' => '');

        $data   = DB::table('binary_struktur')
                        ->where('id_member', '=', $idSponsor)
                        ->first();

        if (empty($data)) return $result;
        $test   = DB::table(DB::raw('binary_struktur AS sp'))
                        ->selectRaw("sp.id, sp.id_parent, sp.baris_urut, sp.kaki_urut, sp.kode, sp.side_urut, COUNT(ch.id) AS jml_kaki,
                                    (SELECT GROUP_CONCAT(x.side_urut, ',') FROM binary_struktur AS x WHERE x.id_parent = sp.id) AS used_side
                                    ")
                        ->leftJoin(DB::raw('binary_struktur AS ch'), 'ch.id_parent', '=', 'sp.id')
                        ->whereRaw("(sp.kode LIKE '" . $data->kode . "%')")
                        ->groupBy('sp.id')
                        ->groupBy('sp.kode')
                        ->having('jml_kaki', '<', $this->maxKakiBinary)
                        ->orderBy('sp.baris_urut')
                        ->orderBy('sp.side_urut', ($Direction == 2) ? 'desc' : 'asc')
                        ->orderBy('jml_kaki', 'desc')
                        ->first();
        
        if (empty($test)) return $result;

        $result->ID         = $test->id;
        $result->Baris      = $test->baris_urut;
        $result->Kaki       = $test->kaki_urut;
        $result->Kode       = $test->kode;
        $result->SideNo     = $test->side_urut;
        $result->UsedSide   = $test->used_side;

        return $result;
    }
*/
    public function getBinaryAutoSpace($startData, $Direction = 1) {
        $result = (object) array('Exists'       => false,
                                'ID'            => 0, 
                                'Kode'          => '', 
                                'Baris'         => 0, 
                                'Kaki'          => 0, 
                                'SideNo'        => 0, 
                                //'UsedSide'  => '',
                                'ChildBaris'    => 0, 
                                'ChildSide'     => 0, 
                                'ChildKaki'     => 0);

        if (empty($startData)) return $result;

        $kode   = $startData->kode;
        $baris  = $startData->baris_urut;
        $side   = $startData->side_urut;
        $kaki   = ($Direction == 2) ? 2 : 1;

        //  (SELECT GROUP_CONCAT(x.side_urut, ',') FROM binary_struktur AS x WHERE x.id_parent = bs.id) AS used_side

        $sql    = "SELECT tmp.baris_urut, tmp.side_urut, bs.kaki_urut, bs.kode, bs.id, bs.id_parent, cs.kode AS child_kode,
                        tmp.child_baris, tmp.child_side, tmp.child_kaki
                    FROM 
                        (SELECT b.baris AS baris_urut,
                            @s2 := (CASE WHEN b.baris = @b1 THEN @s1 ELSE @s2 * 2 - (CASE WHEN @k = 1 THEN 1 ELSE 0 END) END) AS side_urut,
                            b.baris + 1 AS child_baris,
                            (@s2 * 2 - (CASE WHEN @k = 1 THEN 1 ELSE 0 END)) AS child_side, @k AS child_kaki
                        FROM 
                            vbinary_baris AS b,
                            (SELECT @b1 := $baris, @s1 := $side, @s2 := $side, @k := $kaki) AS tmp
                        WHERE (b.baris >= @b1)
                        ) AS tmp
                        LEFT JOIN binary_struktur AS bs ON bs.baris_urut = tmp.baris_urut AND bs.side_urut = tmp.side_urut
                        LEFT JOIN binary_struktur AS cs ON cs.baris_urut = tmp.child_baris AND cs.side_urut = tmp.child_side
                    WHERE (bs.kode IS NOT NULL) AND (cs.kode IS NULL)
                    ORDER BY tmp.baris_urut
                    LIMIT 1";

        $Data   = DB::select($sql);
        
        if (empty($Data)) return $result;

        $result->Exists     = true;

        $result->ID         = $Data[0]->id;
        $result->Baris      = $Data[0]->baris_urut;
        $result->Kaki       = $Data[0]->kaki_urut;
        $result->Kode       = $Data[0]->kode;
        $result->SideNo     = $Data[0]->side_urut;
        //$result->UsedSide   = $Data[0]->used_side;

        $result->ChildBaris = $Data[0]->child_baris;
        $result->ChildSide  = $Data[0]->child_side;
        $result->ChildKaki  = $Data[0]->child_kaki;

        return $result;
    }

    private function getBinaryKode($parentKode, $Baris, $Kaki) {
        return (($parentKode != '') ? $parentKode : 'B') . '-' . $Baris . '.' . $Kaki;
    }

/*
    //  Direction = 1 left, 2 right
    public function getBinarySideByParent($parentSide, $UsedSide, $Direction = 1) {
        if ($parentSide <= 0) return 1;
        
        $maxSide    = $parentSide * $this->maxKakiBinary;
        $minSide    = $maxSide - $this->maxKakiBinary + 1;
        
        if ($UsedSide == '') return ($Direction == 1) ? $minSide : $maxSide;

        $UsedArray  = explode(',', $UsedSide);
        $result     = $minSide;

        $ketemu = true;
        $titik  = ($Direction == 1) ? $minSide - 1 : $maxSide + 1;
        $akhir  = ($Direction == 1) ? $maxSide : $minSide;
        
        while ($ketemu == true && $titik <> $akhir) {
            if ($Direction == 1) {
                $titik++;
            } else {
                $titik--;
            }
            if (!in_array($titik, $UsedArray)) {
                $result = $titik;
                $ketemu = false;
                break;
            } else {
                $ketemu = true;
            }
        }

        return $result;
    }
*/

    private function isBinaryFirstPair($Baris, $idParent) {
        $data   = DB::table('binary_struktur')
                        ->selectRaw('baris_urut, id_parent, COUNT(id_parent) AS jml_per_parent')
                        ->where('baris_urut', '=', $Baris)
                        ->groupBy('baris_urut')
                        ->groupBy('id_parent')
                        ->having('jml_per_parent', '=', 2)
                        ->get();

        if (empty($data)) return false;
        if (count($data) > 1) return false;
        return ($data[0]->id_parent == $idParent);
    }

    private function getBinaryBonusPairList($binaryData) {
        if (empty($binaryData)) return null;

        $Kode   = $binaryData->kode;
        $Baris  = $binaryData->baris_urut;
        $Parent = $binaryData->id_parent;

        if ($this->isBinaryFirstPair($Baris, $Parent) == false) return null;

        $vSql   = "(SELECT vb.baris,
                        @kode := REVERSE(SUBSTR(REVERSE(@kode), LOCATE('-', REVERSE(@kode)) + 1)) AS kode,
                        @urutan := @urutan + 1 AS urut_garis,
                        (CASE WHEN MOD(@urutan, 2) > 0 THEN 1 ELSE 0 END) AS is_ganjil
                    FROM 
                        vbinary_baris AS vb,
                        (SELECT @kode := '$Kode', @urutan := 0, @actbaris := $Baris) AS tmp
                    WHERE (vb.baris < @actbaris)
                    ORDER BY vb.baris DESC
                    ) AS lurus";

        return DB::table('binary_struktur')
                        ->join(DB::raw($vSql), 'lurus.kode', '=', 'binary_struktur.kode')
                        ->join('wallet', 'wallet.id_member', '=', 'binary_struktur.id_member')
                        ->selectRaw('binary_struktur.id, binary_struktur.kode, binary_struktur.baris_urut, 
                                    binary_struktur.side_urut, binary_struktur.kaki_urut, binary_struktur.id_member, binary_struktur.id_parent,
                                    lurus.urut_garis, lurus.is_ganjil,
                                    wallet.id AS id_wallet, wallet.nomor AS nomor_wallet, wallet.debet, wallet.kredit, wallet.saldo')
                        ->orderBy('lurus.urut_garis')
                        ->get();
    }

    public function AddBinaryMember($idMember, $varBinary) {
        $result = (object) array('Success' => false, 'Data' => null, 'ListBonusPair' => null);

        if ($varBinary->Kaki > $this->maxKakiBinary || $varBinary->Kaki < 1) return $result;
        $data   = array('id_parent'     => $varBinary->ParentID,
                        'baris_urut'    => $varBinary->Baris,
                        'kaki_urut'     => $varBinary->Kaki,
                        'side_urut'     => $varBinary->Side,
                        'kode'          => $this->getBinaryKode($varBinary->ParentKode, $varBinary->Baris, $varBinary->Kaki),
                        'id_member'     => $idMember);

        try {
            $idBinary   = DB::table('binary_struktur')->insertGetId($data);

            $data['id']         = $idBinary;
            $result->Data       = (object) $data;
            $result->Success    = true;

            $result->ListBonusPair  = $this->getBinaryBonusPairList($result->Data);
        } catch (\Exception $e) {
            //dd('Error AddBinaryMember : ' . $e->getMessage());
            $result->Success    = false;
        }

        return $result;
    }

    /*  End Struktur Binary */

    /*  Struktur Matrix */

    private $maxKakiMatrix  = 5;
/*
    private function getMatrixDeepest($idMember) {
        $result = (object) array('Row' => 1, 'Deepest' => 2, 'Distance' => 1, 'MaxKaki' => $this->maxKakiMatrix);
        $data   = DB::table(DB::raw('matrix_struktur AS saya'))
                        ->leftJoin(DB::raw('matrix_struktur AS anak'), 'anak.kode', 'LIKE', DB::raw("CONCAT(saya.kode, '%')"))
                        ->selectRaw('saya.id, saya.baris_urut, (MAX(COALESCE(anak.baris_urut, 1)) + 1) AS terdalam')
                        ->where('saya.id_member', '=', $idMember)
                        ->groupBy('saya.id')
                        ->groupBy('saya.baris_urut')
                        ->first();

        if (empty($data)) return $result;
        $result->Row        = $data->baris_urut;
        $result->Deepest    = $data->terdalam;
        $result->Distance   = $result->Deepest - $result->Row;

        return $result;
    }
*/
    public function getMatrixStructure($idMember) {
        return DB::table(DB::raw('matrix_struktur AS anak'))
                                                ->join('members', 'members.id', '=', 'anak.id_member')
                                                ->leftJoin(DB::raw('members as sp'), 'sp.id', '=', 'members.id_sponsor')
                                                ->rightJoin(DB::raw('matrix_struktur AS saya'), 'anak.kode', 'LIKE', DB::raw("CONCAT(saya.kode, '%')"))
                                                ->selectRaw('anak.id, anak.id_parent, anak.baris_urut, anak.kaki_urut, anak.kode, anak.id_member,
                                                            members.nama, members.id_sponsor, sp.nama AS nama_sponsor')
                                                ->where('saya.id_member', '=', $idMember)
                                                ->orderBy('anak.kode')
                                                ->get();
    }

    public function getMatrixSpace($idSponsor) {
        $result = (object) array('ID' => 0, 'Kode' => '', 'Baris' => 0, 'Kaki' => 0, 'SideNo' => 0, 'UsedSide' => '');

        $data   = DB::table('matrix_struktur')
                        ->where('id_member', '=', $idSponsor)
                        ->first();

        if (empty($data)) return $result;
        $test   = DB::table(DB::raw('matrix_struktur AS sp'))
                        ->selectRaw("sp.id, sp.id_parent, sp.baris_urut, sp.kaki_urut, sp.kode, sp.side_urut, COUNT(ch.id) AS jml_kaki,
                                    (SELECT GROUP_CONCAT(x.side_urut, ',') FROM matrix_struktur AS x WHERE x.id_parent = sp.id) AS used_side
                                    ")
                        ->leftJoin(DB::raw('matrix_struktur AS ch'), 'ch.id_parent', '=', 'sp.id')
                        ->whereRaw("(sp.kode LIKE '" . $data->kode . "%')")
                        ->groupBy('sp.id')
                        ->groupBy('sp.kode')
                        ->having('jml_kaki', '<', $this->maxKakiMatrix)
                        ->orderBy('sp.baris_urut')
                        ->orderBy('jml_kaki')
                        ->orderBy('sp.side_urut')
                        ->first();
        
        if (empty($test)) return $result;

        $result->ID         = $test->id;
        $result->Baris      = $test->baris_urut;
        $result->Kaki       = $test->jml_kaki;
        $result->Kode       = $test->kode;
        $result->SideNo     = $test->side_urut;
        $result->UsedSide   = $test->used_side;

        return $result;
    }

    private function getMatrixKode($parentKode, $Baris, $Kaki) {
        return (($parentKode != '') ? $parentKode : 'M') . '-' . $Baris . '.' . $Kaki;
    }

    private function getMatrixSideByParent($parentSide, $UsedSide) {
        if ($parentSide <= 0) return 1;
        
        $maxSide    = $parentSide * $this->maxKakiMatrix;
        $minSide    = $maxSide - $this->maxKakiMatrix + 1;
        
        if ($UsedSide == '') return $minSide;

        $UsedArray  = explode(',', $UsedSide);
        $result     = $minSide;

        $ketemu = true;
        $titik  = $minSide - 1;
        $akhir  = $maxSide;
        
        while ($ketemu == true && $titik <> $akhir) {
            $titik++;
            if (!in_array($titik, $UsedArray)) {
                $result = $titik;
                $ketemu = false;
                break;
            } else {
                $ketemu = true;
            }
        }

        return $result;
    }

    private function getMatrixKakiBySide($Side) {
        $kaki   = $this->maxKakiMatrix;
        $bawah  = floor($Side / $kaki);

        return ($bawah * $kaki == $Side) ? $kaki : $Side - ($bawah * $kaki);
    }

    private function getMatrixBonusList($matrixData) {
        if (empty($matrixData)) return null;

        $Kode   = $matrixData->kode;
        $Baris  = $matrixData->baris_urut;
        $Parent = $matrixData->id_parent;

        $vSql   = "(SELECT mbaris.baris_urut,
                        @kode := REVERSE(SUBSTR(REVERSE(@kode), LOCATE('-', REVERSE(@kode)) + 1)) AS kode,
                        @urutan := @urutan + 1 AS urut_garis
                    FROM
                        (SELECT DISTINCT baris_urut FROM matrix_struktur) AS mbaris,
                        (SELECT @kode := '$Kode', @urutan := 0, @actbaris := $Baris) AS tmp
                    WHERE (mbaris.baris_urut < @actbaris)
                    ORDER BY mbaris.baris_urut DESC
                    ) AS lurus";

        return DB::table('matrix_struktur')
                        ->join(DB::raw($vSql), 'lurus.kode', '=', 'matrix_struktur.kode')
                        ->join('wallet', 'wallet.id_member', '=', 'matrix_struktur.id_member')
                        ->selectRaw('matrix_struktur.id, matrix_struktur.kode, matrix_struktur.baris_urut, 
                                    matrix_struktur.side_urut, matrix_struktur.kaki_urut, matrix_struktur.id_member, matrix_struktur.id_parent,
                                    lurus.urut_garis, 
                                    wallet.id AS id_wallet, wallet.nomor AS nomor_wallet, wallet.debet, wallet.kredit, wallet.saldo')
                        ->orderBy('lurus.urut_garis')
                        ->get();
    }

    public function AddMatrixMember($space, $idMember) {
        $result = (object) array('Success' => false, 'Data' => null, 'BonusList' => null);

        if ($space->Kaki + 1 > $this->maxKakiMatrix) return $result;

        $Side   = $this->getMatrixSideByParent($space->SideNo, $space->UsedSide);

        $kaki   = $this->getMatrixKakiBySide($Side);

        $data   = array('id_parent'     => $space->ID,
                        'baris_urut'    => $space->Baris + 1,
                        'kaki_urut'     => $kaki,
                        'side_urut'     => $Side,
                        'kode'          => $this->getMatrixKode($space->Kode, $space->Baris + 1, $kaki),
                        'id_member'     => $idMember);

        try {
            $idMatrix   = DB::table('matrix_struktur')->insertGetId($data);

            $data['id']         = $idMatrix;
            $result->Data       = (object) $data;
            $result->Success    = true;

            $result->BonusList  = $this->getMatrixBonusList($result->Data);
        } catch (\Exception $e) {
            //dd('Error AddMatrixMember : ' . $e->getMessage());
            $result->Success    = false;
        }

        return $result;
    }

    /*  End Struktur Matrix */
}



