<?php

namespace memberModel;

use DB;
use Hash;

class Members extends \memberModel\Struktur {

    /*
    status : 
        -1  => semua, di database tidak ada
        0   => baru register belum aktif
        1   => aktif
        2   => blokir atau sebangsanya
    */
    public function getMembers($status = -1) {
        $data   = DB::table('members')
                        ->leftJoin(DB::raw('members AS sponsor'), 'sponsor.id', '=', 'members.id_sponsor')
                        ->selectRaw('members.*, sponsor.nama AS nama_sponsor')
                        ->where('members.member_type', '<>', 1);
        if ($status >= 0) $data = $data->where('members.status', '=', $status);
        return $data->orderBy(DB::raw("(CASE WHEN members.status = 2 THEN 'b' WHEN members.status = 0 THEN 'c' ELSE 'a' END)"))
                    ->orderBy('members.id')
                    ->get();
    }

    public function getMyMembers($yourID) {
        return DB::table('members')
                    ->leftJoin(DB::raw('members AS sponsor'), 'sponsor.id', '=', 'members.id_sponsor')
                    ->selectRaw('members.*, sponsor.nama AS nama_sponsor')
                    ->whereRaw("(members.id_sponsor = " . $yourID . " OR members.id = " . $yourID . ")")
                    ->where('members.member_type', '=', 6)
                    ->orderBy(DB::raw("(CASE WHEN members.status = 2 THEN 'b' WHEN members.status = 0 THEN 'c' ELSE 'a' END)"))
                    ->orderBy('members.id')
                    ->get();
    }

    public function getMember($val, $kolom = 'id') {
        return DB::table('members')->where($kolom, '=', $val)->first();
    }

    public function isActive($memberData) {
        if (empty($memberData)) return false;
        return ($memberData->status == 1);
    }

    public function isPendingRegister($memberData) {
        if (empty($memberData)) return false;
        return ($memberData->status == 0);
    }

    public function isBlokir($memberData) {
        if (empty($memberData)) return false;
        return ($memberData->status == 2);
    }

    public function isAdmin($memberData) {
        if (empty($memberData)) return false;
        return ($memberData->member_type == 1);
    }

    public function isMember($memberData) {
        if (empty($memberData)) return false;
        return ($memberData->member_type == 6);
    }
/*
    //public function addMember($data, $spaceBinary, $spaceMatrix, $chooseBinarySide) {
    public function addMember($sponsorData, $data, $varBinary, $spaceMatrix) {
        $result = (object) array('Success' => false, 'Board' => null, 'Penuh' => false, 'BoardMemberID' => 0, 'MemberID' => 0);
        if (empty($data)) return $result;
        
        $idSponsor      = $data['id_sponsor'];
        $sponsorBoard   = $this->getSponsorBoardForRegister($idSponsor);

        $isNewBoard     = false;
        if (empty($sponsorBoard)) {
            if ($idSponsor == -1) {
                $newBoard       = $this->createNewBoard(0, 1);
                if ($newBoard->Success == false) return $result;
                $sponsorBoard   = $newBoard->Data;
                $isNewBoard     = true;
            } else {
                return $result;
            }
        }

        $parentBoard    = $this->getParentBoard($sponsorBoard, $isNewBoard);

        //DB::beginTransaction();
        try {
            $id     = DB::table('members')->insertGetId($data);

            //$binary = $this->AddBinaryMember($spaceBinary, $id, $chooseBinarySide);
            $binary = $this->AddBinaryMember($id, $varBinary);
            $matrix = $this->AddMatrixMember($spaceMatrix, $id);
            $result = $this->RegisterMemberToBoard($id, $sponsorBoard, $parentBoard);
            
            $result->Success = ($result->Success == true && $binary == true && $matrix == true);

            if ($result->Success == true) {
        //        DB::commit();
            } else {
        //        DB::rollback();
                if ($isNewBoard == true) $this->DeleteBoard($sponsorBoard->id);
            }
        } catch (\Exception $e) {
        //    DB::rollback();
            if ($isNewBoard == true) $this->DeleteBoard($sponsorBoard->id);
            //dd('Error addMember : ' . $e->getMessage());
        }

        return $result;
    }
*/
    public function DeleteMember($idMember) {
        DB::table('members')->where('id', '=', $idMember)->delete();
        return;
    }

    public function addMember2($data) {

        $result = (object) array('Success' => false, 'Data' => null);
        
        if (empty($data)) return $result;
        if (!is_array($data)) return $result;

        if ($data['id_sponsor'] == -1) $data['id_sponsor'] = 0;
        
        try {
            $idMember   = DB::table('members')->insertGetId($data);

            $data['id']         = $idMember;
            $result->Data       = (object) $data;
            $result->Success    = true;
        } catch (\Exception $e) {
            //dd('Error addMember2 : ' . $e->getMessage());
            $result->Success    = false;
        }

        return $result;
    }

    //  khusus cek ada admin apa belum, jika belum buat. selanjutnya buat user I. setelah itu jgn dipake lagi
/*
    public function CheckAdmin() {
        $Adm    = DB::table('members')->where('member_type', '=', 1)->first();
        if (empty($Adm)) {
            $pwd    = Hash::make ('HS2016coy');
            $data   = array('kode'          => uniqid(), 
                            'email'         => 'hs2016@gmail.com', 
                            'password'      => $pwd, 
                            'member_type'   => 1, 
                            'nama'          => 'Administrator', 
                            'id_sponsor'    => 0, 
                            'status'        => 1, 
                            'status_at'     => date('Y-m-d H:i:s'));
            try {
                DB::table('members')->insert($data);
                return true;
            } catch (\Exception $e) {
                dd('Error add admin : ' . $e->getMessage());
                return false;
            }
        } else {
            dd('sudah ada cuy');
        }
    }
*/
}

