<?php

namespace financialModel;

use DB;

class WalletHistorySettings {

    protected $HargaTicket                  = 10000;
    protected $HargaDaftar                  = 900000;
    protected $SaldoAwalMember              = 240000;
    protected $BiayaDaftarForCompany        = 100000;

    //  jenis :
    //  kredit
    protected $jnsSaldoAwal                 = 1;
    protected $jnsTopUp                     = 2;
    protected $jnsBonusRegister             = 3;
    protected $jnsBonusBinaryGanjil         = 4;
    protected $jnsBonusBinaryGenap          = 5;
    protected $jnsBonusMatrix               = 6;
    protected $jnsBonusFlyFromBoard1To2     = 7;
    protected $jnsBonusBoardWaitingList     = 8;
    protected $jnsBonusFlyFromBoard2To2     = 9;
    protected $jnsBonusSponsorFlyBoard2     = 10;
    protected $jnsBonusDownline1FlyBoard2   = 11;
    //  debet
    protected $jnsBeliTiket                 = 12;
    protected $jnsTarikSaldoDaftar          = 13;
    protected $jnsBeliPulsa                 = 14;
    protected $jnsWithdraw                  = 15;
    //  end jenis

    //  sumber :
    protected $sumberSaldoAwal              = 1;
    protected $sumberTopUp                  = 2;
    protected $sumberBeliTiketDaftar        = 3;
    protected $sumberBonusMember            = 4;
    protected $sumberFlyBoard               = 5;
    protected $sumberBinary                 = 6;
    protected $sumberMatrix                 = 7;
    protected $sumberTarikSaldoDaftar       = 8;
    protected $sumberWithdraw               = 9;

    private $dataJenis = array();

    private function DataJenisLoaded() {
        return !empty($this->dataJenis);
    }

    private function initDataJenis() {
        $data   = DB::table('transaksi_jenis')->orderBy('id')->get();
        $this->dataJenis = array();
        foreach ($data as $row) {
            $this->dataJenis[$row->id] = $row;
        }
        return;
    }

    //  Bagian Perusahaan dan Simpanan
    public function WalletKaliKhusus($jenis, $idKhusus) {
        if ($idKhusus !== 1 && $idKhusus !== 2) return 0;

        if ($this->DataJenisLoaded() == false) $this->initDataJenis();

        return (!empty($this->dataJenis) && array_key_exists($jenis, $this->dataJenis)) ? $this->dataJenis[$jenis]->{'kali_khusus' . $idKhusus} : 0;
    }

    public function getUraianJenisKhusus($jenis, $idKhusus, $optionalTXT = '') {
        if ($idKhusus !== 1 && $idKhusus !== 2) return 0;
        
        if ($this->DataJenisLoaded() == false) $this->initDataJenis();

        $result = (!empty($this->dataJenis) && array_key_exists($jenis, $this->dataJenis)) ? $this->dataJenis[$jenis]->{'uraian_khusus' . $idKhusus} : '';

        if ($result != '') {
            if ($optionalTXT != '') $result .= ' ' . $optionalTXT;
        } else {
            $result = 'Jenis Kategori Tidak Diakui';
        }

        return $result;
    }
    //  End Bagian Perusahaan dan Simpanan

    //  Bagian Member
    public function WalletKali($jenis) {
        if ($this->DataJenisLoaded() == false) $this->initDataJenis();

        return (!empty($this->dataJenis) && array_key_exists($jenis, $this->dataJenis)) ? $this->dataJenis[$jenis]->kali : 0;
    }

    public function getUraianJenis($jenis, $optionalTXT = '') {
        if ($this->DataJenisLoaded() == false) $this->initDataJenis();

        $result = (!empty($this->dataJenis) && array_key_exists($jenis, $this->dataJenis)) ? $this->dataJenis[$jenis]->uraian : '';

        if ($result != '') {
            if ($optionalTXT != '') $result .= ' ' . $optionalTXT;
        } else {
            $result = 'Jenis Kategori Tidak Diakui';
        }

        return $result;
    }
    //  End Bagian Member

    //  Bagian Umum
    public function getSumberByJenis($jenis) {
        if ($this->DataJenisLoaded() == false) $this->initDataJenis();

        return (!empty($this->dataJenis) && array_key_exists($jenis, $this->dataJenis)) ? $this->dataJenis[$jenis]->sumber : 0;
    }

    public function getDataSumber() {
        return DB::table('transaksi_sumber')->orderBy('id')->get();
    }
    //  End Bagian Umum
}


/*

Untuk sumber dan jenis sudah ada dalam database di table transaksi_sumber (sumber) dan transaksi_jenis (jenis), tujuannya untuk pengikat setiap transaksi dalam bentuk relasi.
Harap untuk tidak merubah data-data tersebut. jika dirubah, maka variable constant di atas harus disesuaikan.

Sumber :
1   Saldo Awal, id sumber null
2   Top Up, id sumber null
3   Beli Tiket, id sumbernya adalah id tiket
4   Bonus yang diperoleh dari member, id sumbernya adalah id member yang mengalirkan bonus
5   Bonus yang diperoleh dari fly board, id sumbernya adalah id board member
6   Bonus yang diperoleh dari Binary, id sumbernya adalah id binary
7   Bonus yang diperoleh dari Matrix, id sumbernya adalah id matrix
8   Pengambilan Saldo untuk member yang didaftarkan, id sumbernya adalah id member yang didaftarkan
9   Pembelian Pulsa, id sumbernya adalan id pembelian
10  Withdraw, id sumbernya adalah id withdraw

History :

Jenis :
    Kredit :
    1   Saldo Member Baru / Saldo Awal
    2   Top Up Saldo
    3   Bonus Mendaftarkan Member Baru / Bonus Sponsor
    4   Bonus Struktur Binary Baris Ganjil Setelah Terdapat 1 Pair Perbaris Struktur
    5   Bonus Struktur Binary Baris Genap Setelah Terdapat 1 Pair Perbaris Struktur
    6   Bonus Struktur Matrix
    7   Bonus Fly Dari Board I Ke Board II
    8   Bonus Waiting List di Board I
    9   Bonus Fly Boss Di Board II Ke Board II Yang Lain
    10  Bonus Sebagai Sponsor Dari Member Yang Mengalamai Fly Boss Board II
    11  Bonus Sebagai Downline Pertama Dalam Struktur Binary Dari Member Yang Mengalamai Fly Boss Board II

    Debet :
    12  Pembelian Tiket Untuk Mendaftarkan Member Baru
    13  Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan
    14  Pembelian Pulsa
    15  Penarikan Saldo Langsung / Withdraw

*/