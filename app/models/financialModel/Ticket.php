<?php

namespace financialModel;

use DB;

class Ticket extends \financialModel\WalletHistorySettings {

    public function getFreeTicket($idMember) {
        return DB::table('tiket_register')
                        ->where('id_member', '=', $idMember)
                        ->where('is_used', '=', 0)
                        ->first();
    }

    public function getTicketList($idMember) {
        return DB::table('tiket_register')
                        ->leftJoin('members', 'members.id', '=', 'tiket_register.used_by_member')
                        ->selectRaw('tiket_register.*, members.nama')
                        ->where('id_member', '=', $idMember)
                        ->orderBy('tiket_register.id')
                        ->get();
    }

    public function getTicketByID($id) {
        return DB::table('tiket_register')
                        ->where('id', '=', $id)
                        ->first();
    }

    public function isUsed($tiketData) {
        if (empty($tiketData)) return true;
        return ($tiketData->is_used == 1);
    }

    public function UsedByMember($tiketData, $memberCls) {
        if (empty($tiketData)) return null;
        if ($this->isUsed($tiketData) == false) return null;
        return $memberCls->getMember($tiketData->used_by_member);
    }

    public function getHargaTiket() {
        return $this->HargaTicket;
    }

    public function UseTicket(&$tiketData, $idMemberUsed) {
        if (empty($tiketData)) return false;
        $data   = array('is_used'           => 1,
                        'used_at'           => date('Y-m-d H:i:s'),
                        'used_by_member'    => $idMemberUsed);
        try {
            DB::table('tiket_register')->where('id', '=', $tiketData->id)->update($data);
            $tiketData->is_used         = $data['is_used'];
            $tiketData->used_at         = $data['used_at'];
            $tiketData->used_by_member  = $data['used_by_member'];
            return true;
        } catch (\Exception $e) {
            //dd('Error Tiket->UseTicket : ' . $e->getMessage());
            return false;
        }
    }

    public function generateNomorTiket() {
        $result = '';
        for ($i = 1; $i <= 13; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function AddTicket($data) {
        $result = (object) array('Success' => false, 'Data' => null, 'FailMessage' => '');

        try {
            $idTiket = DB::table('tiket_register')->insertGetId($data);

            $data['id'] = $idTiket;

            $result->Data           = (object) $data;
            $result->Success        = true;
        } catch (\Exception $e) {
            //dd('Error Tiket->AddTicket : ' . $e->getMessage());
            $result->FailMessage    = 'Gagal menambahkan tiket.';
        }

        return $result;
    }
}

