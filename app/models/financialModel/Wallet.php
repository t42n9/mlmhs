<?php

namespace financialModel;

use DB;

class Wallet extends \financialModel\WalletHistorySettings {

    public function getConstantValue($key) {
        return $this->$key;
    }

    //  Bagian Khusus
    public function getWalletKhusus($isSimpanan = false) {
        return DB::table('wallet_khusus')
                    ->where('id', '=', ($isSimpanan === true) ? 2 : 1)
                    ->first();
    }

    public function getWalletHistoryKhusus($isSimpanan = false) {
        return DB::table(DB::raw('(SELECT @total := 0) AS v, wallet_khusus'))
                    ->join('wallet_khusus_history', 'wallet_khusus_history.id_wallet', '=', 'wallet_khusus.id')
                    ->selectRaw('wallet_khusus_history.id AS id_history, wallet_khusus_history.id_wk AS id_wallet, wallet_khusus_history.tgl_history,
                                wallet_khusus_history.id_jenis, wallet_khusus_history.keterangan, 
                                wallet_khusus.debet, wallet_khusus.kredit, wallet_khusus.saldo,
                                wallet_khusus_history.nilai,
                                (CASE WHEN wallet_khusus_history.nilai < 0 THEN wallet_khusus_history.nilai ELSE 0 END) AS real_debet,
                                (CASE WHEN wallet_khusus_history.nilai >= 0 THEN wallet_khusus_history.nilai ELSE 0 END) AS real_kredit,
                                @total := @total + wallet_khusus_history.nilai AS real_saldo')
                    ->where('wallet_khusus.id', '=', ($isSimpanan === true) ? 2 : 1)
                    ->orderBy('wallet_khusus_history.id', 'desc')
                    ->get();
    }

    private function UpdateSaldoKhusus(&$khususData, $nilai) {
        $data   = array('debet'     => $khususData->debet + (($nilai < 0) ? $nilai : 0),
                        'kredit'    => $khususData->kredit + (($nilai > 0) ? $nilai : 0));

        $data['saldo']  = $data['debet'] + $data['kredit'];

        try {
            DB::table('wallet_khusus')->where('id', '=', $khususData->id)->update($data);

            $khususData->debet  = $data['debet'];
            $khususData->kredit = $data['kredit'];
            $khususData->saldo  = $data['saldo'];

            return true;
        } catch (\Exception $e) {
            //dd('Error Wallet->UpdateSaldoKhusus : ' . $e->getMessage());
            return false;
        }
    }

    public function AddHistoryKhusus(&$khususData, $jenis, $nilai, $sumber, $sumberID, $keterangan, $updateSaldo = true) {
        $result = (object) array('Success' => false, 'FailMessage' => 'Data Wallet tidak ada.');

        if (empty($khususData)) return $result;

        if ($nilai == 0) {
            $result->Success        = true;
            $result->FailMessage    = '';
            return $result;
        }
        
        $data   = array('id_wk'         => $khususData->id,
                        'id_jenis'      => $jenis,
                        'nilai'         => $nilai,
                        'id_sumber'     => $sumber,
                        'sumber_id'     => $sumberID,
                        'keterangan'    => $keterangan);

        try {
            DB::table('wallet_khusus_history')->insert($data);

            $okSaldo    = ($updateSaldo === true) ? $this->UpdateSaldoKhusus($khususData, $nilai) : true;

            $result->Success        = $okSaldo;
        } catch (\Exception $e) {
            //dd('Error Wallet->AddHistoryKhusus : ' . $e->getMessage());
            $result->Success        = false;
        }

        if ($result->Success == true) {
            $result->FailMessage    = '';
        } else {
            $result->FailMessage    = 'Gagal proses pencatatan transaksi.';
        }

        return $result;
    }

    //  End Bagian Khusus

    //  Bagian Member
    public function getWalletMember($idMember) {
        return DB::table('wallet')
                    ->where('id_member', '=', $idMember)
                    ->first();
    }

    public function getWalletMemberByID($id) {
        return DB::table('wallet')
                    ->where('id', '=', $id)
                    ->first();
    }

    public function getWalletHistory($idMember) {
        return DB::table(DB::raw('(SELECT @total := 0) AS v, wallet'))
                    ->join('wallet_history', 'wallet_history.id_wallet', '=', 'wallet.id')
                    ->selectRaw('wallet.nomor, wallet_history.id_wallet, wallet_history.id AS id_history, wallet_history.tgl_history,
                                wallet_history.id_jenis, wallet_history.keterangan, 
                                wallet.debet, wallet.kredit, wallet.saldo,
                                wallet_history.nilai,
                                (CASE WHEN wallet_history.nilai < 0 THEN wallet_history.nilai ELSE 0 END) AS real_debet,
                                (CASE WHEN wallet_history.nilai >= 0 THEN wallet_history.nilai ELSE 0 END) AS real_kredit,
                                @total := @total + wallet_history.nilai AS real_saldo')
                    ->where('wallet.id_member', '=', $idMember)
                    ->orderBy('wallet_history.id', 'desc')
                    ->get();
    }

    private function UpdateSaldoMember(&$walletData, $nilai) {
        $data   = array('debet'     => $walletData->debet + (($nilai < 0) ? $nilai : 0),
                        'kredit'    => $walletData->kredit + (($nilai > 0) ? $nilai : 0));

        $data['saldo']  = $data['debet'] + $data['kredit'];

        try {
            DB::table('wallet')->where('id', '=', $walletData->id)->update($data);

            $walletData->debet  = $data['debet'];
            $walletData->kredit = $data['kredit'];
            $walletData->saldo  = $data['saldo'];

            return true;
        } catch (\Exception $e) {
            //dd('Error Wallet->UpdateSaldoMember : ' . $e->getMessage());
            return false;
        }
    }

    public function AddHistory(&$walletData, $jenis, $nilai, $sumber, $sumberID, $keterangan, $updateSaldo = true) {
        $result = (object) array('Success' => false, 'FailMessage' => 'Data Wallet tidak ada.');

        if (empty($walletData)) return $result;

        if ($nilai == 0) {
            $result->Success        = true;
            $result->FailMessage    = '';
            return $result;
        }

        $result->FailMessage = 'Saldo Wallet tidak cukup.';
        if ($nilai < 0) {
            if ($walletData->saldo + $nilai < 0) return $result;
        }

        $data   = array('id_wallet'     => $walletData->id,
                        'id_jenis'      => $jenis,
                        'nilai'         => $nilai,
                        'id_sumber'     => $sumber,
                        'sumber_id'     => $sumberID,
                        'keterangan'    => $keterangan);

        try {
            DB::table('wallet_history')->insert($data);

            $okSaldo    = ($updateSaldo === true) ? $this->UpdateSaldoMember($walletData, $nilai) : true;

            $result->Success        = $okSaldo;
        } catch (\Exception $e) {
            //dd('Error Wallet->AddHistory : ' . $e->getMessage());
            $result->Success        = false;
        }

        if ($result->Success == true) {
            $result->FailMessage    = '';
        } else {
            $result->FailMessage    = 'Gagal proses pencatatan transaksi.';
        }

        return $result;
    }

    private function generateNomorWalletMember() {
        $result = '';
        for ($i = 1; $i <= 13; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function CreateWalletMember($idMember, $nilai) {
        $jenis  = $this->jnsSaldoAwal;
        $kali   = $this->WalletKali($jenis);
        
        $data   = array('id_member' => $idMember,
                        'nomor'     => $this->generateNomorWalletMember(),
                        'debet'     => 0,
                        'kredit'    => 0,
                        'saldo'     => 0);

        $result = (object) array('Success' => false, 'Data' => null, 'FailMessage' => '');

        try {
            $id = DB::table('wallet')->insertGetId($data);

            $data['id'] = $id;
            $walletData = (object) $data;
            $sumber     = $this->getSumberByJenis($jenis);
            $keterangan = $this->getUraianJenis($jenis);

            $okHistory  = $this->AddHistory($walletData, $jenis, $nilai, $sumber, 0, $keterangan, true);

            $result->Success = $okHistory->Success;

            if ($result->Success == true) {
                $result->Data = $walletData;
            } else {
                $result->FailMessage = $okHistory->FailMessage;
            }
        } catch (\Exception $e) {
            //dd('Error Wallet->CreateWalletMember : ' . $e->getMessage());
            $result->FailMessage    = 'Gagal proses pencatatan transaksi.';
        }

        return $result;
    }
    //  End Bagian Member

    //  Top Up
    public $FreeStatus      = 0; 
    public $AccStatus       = 1; 
    public $CancelStatus    = 2; 
    public $RejectStatus    = 3; 
    //  Bagian Member
    public function getWalletHistoryTopUp($idMember) {
        return DB::table('wallet_topup')
                        ->where('id_member', '=', $idMember)
                        ->whereRaw("((status = " . $this->AccStatus . ")
                                    OR (status = " . $this->FreeStatus . " AND is_confirmed = 1) 
                                    OR (status = " . $this->FreeStatus . " AND is_confirmed = 0 AND tgl_unik = DATE(CURRENT_TIMESTAMP)))")
                        ->orderBy('id')
                        ->get();
    }

    public function getTopUpData($idTopUp) {
        return DB::table('wallet_topup')
                        ->where('id', '=', $idTopUp)
                        ->first();
    }

    public function isCanceledTopUp($topupData) {
        if (empty($topupData)) return true;
        return ($topupData->status == $this->CancelStatus);
    }

    public function isRejectedTopUp($topupData) {
        if (empty($topupData)) return true;
        return ($topupData->status == $this->RejectStatus);
    }

    public function isValidTopUpData($topupData) {
        if (empty($topupData)) return false;
        return ($topupData->status == $this->FreeStatus || $topupData->status == $this->AccStatus);
    }

    public function isValidTopUpOwner($topupData, $idMember) {
        if (empty($topupData)) return false;
        return ($topupData->id_member == $idMember);
    }

    public function generateNomorTopUp() {
        $result = '';
        for ($i = 1; $i <= 15; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function createTopUp($Data) {
        $result = (object) array('Success' => false, 'Data' => null, 'FailMessage' => '');

        try {
            $idTopUp = DB::table('wallet_topup')->insertGetId($Data);

            $Data['id']             = $idTopUp;

            $result->Data           = (object) $Data;
            $result->Success        = true;
        } catch (\Exception $e) {
            $result->FailMessage    = 'Gagal melakukan penambahan saldo.';
        }

        return $result;
    }

    public function ConfirmTopUp($idTopUp, $data) {
        try {
            DB::table('wallet_topup')->where('id', '=', $idTopUp)->update($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    //  End Bagian Member

    //  Bagian Admin / Operator
    public function getTopUpDataReadyForProcess($idTopUp) {
        return DB::table('wallet_topup')
                        ->where('id', '=', $idTopUp)
                        ->where('is_confirmed', '=', 1)
                        ->where('status', '=', $this->FreeStatus)
                        ->first();
    }

    public function getRequestTopUp() {
        return DB::table('wallet_topup')
                    ->join('members', 'members.id', '=', 'wallet_topup.id_member')
                    ->selectRaw('wallet_topup.*, members.nama')
                    ->where('wallet_topup.is_confirmed', '=', 1)
                    ->where('wallet_topup.status', '=', $this->FreeStatus)
                    ->orderBy('wallet_topup.tgl_confirmed')
                    ->get();
    }

    public function UpdateStatus($idTopUp, $data) {
        try {
            DB::table('wallet_topup')->where('id', '=', $idTopUp)->update($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    //  End Bagian Admin / Operator
    //  End Top Up
}


