<?php

namespace financialModel;

use DB;

class Bonus extends \financialModel\BonusSettings {

    public function ProsesBonusSponsor($WalletCls, $memberData, &$WalletSponsor, &$WalletSimpanan) {
        if (empty($WalletSimpanan) || empty($memberData)) return false;

        $jenisSponsor   = $WalletCls->getConstantValue('jnsBonusRegister');
        $sumberSponsor  = $WalletCls->getSumberByJenis($jenisSponsor);
        $uraianSponsor  = $WalletCls->getUraianJenis($jenisSponsor, '- ' . $memberData->nama);
        $kaliSponsor    = $WalletCls->WalletKali($jenisSponsor);

        $nilaiBonus     = $this->BonusSponsor * $kaliSponsor;

        $simpananUraian = $WalletCls->getUraianJenisKhusus($jenisSponsor, 2, '- ' . $memberData->nama);
        $simpananKali   = $WalletCls->WalletKaliKhusus($jenisSponsor, 2);

        $nilaiSimpanan  = $this->BonusSponsor * $simpananKali;

        $sumberID   = $memberData->id;

        $okSponsor  = $WalletCls->AddHistory($WalletSponsor, $jenisSponsor, $nilaiBonus, $sumberSponsor, $sumberID, $uraianSponsor, true);
        $okSimpanan = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisSponsor, $nilaiSimpanan, $sumberSponsor, $sumberID, $simpananUraian, true);

        return ($okSponsor == true && $okSimpanan == true);
    }

    public function ProsesBonusBinary($WalletCls, $binaryData, $memberData, $listPairData, &$WalletSimpanan) {
        if (empty($WalletSimpanan) || empty($binaryData) || empty($memberData)) return false;

        if (empty($listPairData)) return true;
        
        $jenisGanjil    = $WalletCls->getConstantValue('jnsBonusBinaryGanjil');
        $sumberGanjil   = $WalletCls->getSumberByJenis($jenisGanjil);
        $uraianGanjil   = $WalletCls->getUraianJenis($jenisGanjil, '- ' . $memberData->nama);
        $kaliGanjil     = $WalletCls->WalletKali($jenisGanjil);

        $simpananUraianGanjil   = $WalletCls->getUraianJenisKhusus($jenisGanjil, 2, '- ' . $memberData->nama);
        $simpananKaliGanjil     = $WalletCls->WalletKaliKhusus($jenisGanjil, 2);
        
        $jenisGenap     = $WalletCls->getConstantValue('jnsBonusBinaryGenap');
        $sumberGenap    = $WalletCls->getSumberByJenis($jenisGenap);
        $uraianGenap    = $WalletCls->getUraianJenis($jenisGenap, '- ' . $memberData->nama);
        $kaliGenap      = $WalletCls->WalletKali($jenisGenap);

        $simpananUraianGenap    = $WalletCls->getUraianJenisKhusus($jenisGenap, 2, '- ' . $memberData->nama);
        $simpananKaliGenap      = $WalletCls->WalletKaliKhusus($jenisGenap, 2);

        $nilaiSimpananGanjil    = 0;
        $nilaiSimpananGenap     = 0;

        $bonusGanjil    = $this->BonusBinaryGanjil;
        $bonusGenap     = $this->BonusBinaryGenap;

        $sumberID       = $binaryData->id;
        $sukses         = true;

        foreach ($listPairData as $row) {
            $isGanjil   = ($row->is_ganjil == 1);
            $WMember    = (object) array('id'       => $row->id_wallet,
                                        'id_member' => $row->id_member,
                                        'nomor'     => $row->nomor_wallet,
                                        'debet'     => $row->debet,
                                        'kredit'    => $row->kredit,
                                        'saldo'     => $row->saldo);

            if ($isGanjil == true) {
                $nilaiSimpananGanjil    += $bonusGanjil;
                $nilai                  = $bonusGanjil * $kaliGanjil;

                $okGanjil   = $WalletCls->AddHistory($WMember, $jenisGanjil, $nilai, $sumberGanjil, $sumberID, $uraianGanjil, true);
                if ($okGanjil->Success == false) {
                    $sukses = false;
                    break;
                }
            } else {
                $nilaiSimpananGenap += $bonusGenap;
                $nilai              = $bonusGenap * $kaliGenap;

                $okGenap    = $WalletCls->AddHistory($WMember, $jenisGenap, $nilai, $sumberGenap, $sumberID, $uraianGenap, true);
                if ($okGenap->Success == false) {
                    $sukses = false;
                    break;
                }
            }
        }

        if ($sukses == true) {
            $nilaiSimpananGanjil    = $nilaiSimpananGanjil * $simpananKaliGanjil;
            $okSimpananGanjil       = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisGanjil, $nilaiSimpananGanjil, $sumberGanjil, $sumberID, $simpananUraianGanjil, true);

            $nilaiSimpananGenap     = $nilaiSimpananGenap * $simpananKaliGenap;
            $okSimpananGenap        = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisGenap, $nilaiSimpananGenap, $sumberGenap, $sumberID, $simpananUraianGenap, true);

            $sukses = ($okSimpananGanjil->Success == true && $okSimpananGenap->Success == true);
        }

        return $sukses;
    }

    public function ProsesBonusMatrix($WalletCls, $matrixData, $memberData, $listBonusMatrix, &$WalletSimpanan) {
        if (empty($WalletSimpanan) || empty($matrixData) || empty($memberData)) return false;

        if (empty($listBonusMatrix)) return true;

        $jenisMatrix    = $WalletCls->getConstantValue('jnsBonusMatrix');
        $sumberMatrix   = $WalletCls->getSumberByJenis($jenisMatrix);
        $uraianMatrix   = $WalletCls->getUraianJenis($jenisMatrix, '- ' . $memberData->nama);
        $kaliMatrix     = $WalletCls->WalletKali($jenisMatrix);

        $simpananUraian = $WalletCls->getUraianJenisKhusus($jenisMatrix, 2, '- ' . $memberData->nama);
        $simpananKali   = $WalletCls->WalletKaliKhusus($jenisMatrix, 2);

        $nilaiSimpanan  = 0;
        $sumberID       = $matrixData->id;
        $sukses         = true;

        foreach ($listBonusMatrix as $row) {
            $nilai  = 0;
            $urut   = intval($row->urut_garis);
            if (array_key_exists($urut, $this->BonusMatrix)) {
                $nilai          = $this->BonusMatrix[$urut];
                $nilaiSimpanan  += $nilai;
            }
            $bonusMatrix    = $nilai * $kaliMatrix;
            $uraian         = $uraianMatrix . ' (Baris Ke- ' . $row->urut_garis . ')';

            $WMember    = (object) array('id'       => $row->id_wallet,
                                        'id_member' => $row->id_member,
                                        'nomor'     => $row->nomor_wallet,
                                        'debet'     => $row->debet,
                                        'kredit'    => $row->kredit,
                                        'saldo'     => $row->saldo);

            $okMatrix   = $WalletCls->AddHistory($WMember, $jenisMatrix, $bonusMatrix, $sumberMatrix, $sumberID, $uraian, true);
            if ($okMatrix->Success == false) {
                $sukses = false;
                break;
            }
        }

        if ($sukses == true) {
            $nilaiSimpanan  = $nilaiSimpanan * $simpananKali;
            $okSimpanan     = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisMatrix, $nilaiSimpanan, $sumberMatrix, $sumberID, $simpananUraian, true);

            $sukses = $okSimpanan->Success;
        }

        return $sukses;
    }

    public function ProsesBonusBoard($WalletCls, $memberData, $boardData, $waitingList, $FlyList, &$WalletSimpanan, &$WalletCompany) {
        if (empty($WalletSimpanan) || empty($boardData) || empty($memberData)) return false;
        if (empty($waitingList) && empty($FlyList)) return true;

        //  bonus waiting list
        $suksesBoard1           = true;

        if (!empty($waitingList)) {
            $jenisBoard1    = $WalletCls->getConstantValue('jnsBonusBoardWaitingList');
            $sumberBoard1   = $WalletCls->getSumberByJenis($jenisBoard1);
            $uraianBoard1   = $WalletCls->getUraianJenis($jenisBoard1, '- ' . $memberData->nama);
            $kaliBoard1     = $WalletCls->WalletKali($jenisBoard1);

            $nilaiBoard1    = $this->BonusNewMemberBoard1;

            $simpananUraianBoard1   = $WalletCls->getUraianJenisKhusus($jenisBoard1, 2, '- ' . $memberData->nama);
            $simpananKaliBoard1     = $WalletCls->WalletKaliKhusus($jenisBoard1, 2);

            $nilaiSimpananBoard1    = 0;
            $sumberIDBoard1         = $boardData->id;

            foreach ($waitingList as $row1) {
                $WMember    = (object) array('id'       => $row1->id_wallet,
                                            'id_member' => $row1->id_member,
                                            'nomor'     => $row1->nomor_wallet,
                                            'debet'     => $row1->debet,
                                            'kredit'    => $row1->kredit,
                                            'saldo'     => $row1->saldo);
                
                $nilaiSimpananBoard1    += $nilaiBoard1;
                $nilai1                 = $nilaiBoard1 * $kaliBoard1;
                $okBoard1   = $WalletCls->AddHistory($WMember, $jenisBoard1, $nilai1, $sumberBoard1, $sumberIDBoard1, $uraianBoard1, true);
                if ($okBoard1->Success == false) {
                    $suksesBoard1 = false;
                    break;
                }
            }

            if ($suksesBoard1 == true) {
                $nilaiSimpananBoard1    = $nilaiSimpananBoard1 * $simpananKaliBoard1;                
                $okSimpanan1            = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisBoard1, $nilaiSimpananBoard1, $sumberBoard1, $sumberIDBoard1, $simpananUraianBoard1, true);

                $suksesBoard1 = $okSimpanan1->Success;
            }
        }
        if ($suksesBoard1 == false) return false;

        if (empty($FlyList)) return $suksesBoard1;

        //  bonus fly 1
        $suksesFly1 = true;

        if (!empty($FlyList->FlyBoard1To2)) {
            if (empty($WalletCompany)) return false;

            $jenisFly1  = $WalletCls->getConstantValue('jnsBonusFlyFromBoard1To2');
            $sumberFly1 = $WalletCls->getSumberByJenis($jenisFly1);
            $uraianFly1 = $WalletCls->getUraianJenis($jenisFly1);
            $kaliFly1   = $WalletCls->WalletKali($jenisFly1);

            $nilaiFly1  = $this->BonusFlayBoard1;

            $simpananUraianFly1 = $WalletCls->getUraianJenisKhusus($jenisFly1, 2);
            $simpananKaliFly1   = $WalletCls->WalletKaliKhusus($jenisFly1, 2);

            $nilaiSimpananFly1  = 0;

            $companyUraianMargin  = $WalletCls->getUraianJenisKhusus($jenisFly1, 1);
            $companyKaliMargin    = $WalletCls->WalletKaliKhusus($jenisFly1, 1);

            $nilaiCompanyMargin   = $this->BonusCompanyMargin * $companyKaliMargin;

            foreach ($FlyList->FlyBoard1To2 as $row2) {
                $sumberIDFlay1      = $row2->idNewBoard;
                $WMember            = $row2->WalletData;
                
                $nilai2             = $nilaiFly1 * $kaliFly1;
                
                $okFly1     = $WalletCls->AddHistory($WMember, $jenisFly1, $nilai2, $sumberFly1, $sumberIDFlay1, $uraianFly1, true);
                if ($okFly1->Success == false) {
                    $suksesFly1 = false;
                    break;
                }

                $nilaiSimpananFly1  = $nilaiFly1 * $simpananKaliFly1;
                $uraianSimpananFly1 = $simpananUraianFly1 . ' - ' . $row2->namaMember . ' (1 to 2)';

                $okSimpananFly1     = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisFly1, $nilaiSimpananFly1, $sumberFly1, $sumberIDFlay1, $uraianSimpananFly1, true);
                if ($okSimpananFly1->Success == false) {
                    $suksesFly1 = false;
                    break;
                }

                $uraianCompanyMargin = $companyUraianMargin . ' - ' . $row2->namaMember . ' (1 to 2)';

                $okCompany  = $WalletCls->AddHistoryKhusus($WalletCompany, $jenisFly1, $nilaiCompanyMargin, $sumberFly1, $sumberIDFlay1, $uraianCompanyMargin, true);
                if ($okCompany->Success == false) {
                    $suksesFly1 = false;
                    break;
                }
            }
        }

        if ($suksesFly1 == false) return false;

        //  bonus fly 2
        $suksesFly2 = true;

        if (!empty($FlyList->FlyBoard2To2)) {
            $jenisFly2  = $WalletCls->getConstantValue('jnsBonusFlyFromBoard2To2');
            $sumberFly2 = $WalletCls->getSumberByJenis($jenisFly2);
            $uraianFly2 = $WalletCls->getUraianJenis($jenisFly2);
            $kaliFly2   = $WalletCls->WalletKali($jenisFly2);

            $nilaiFly2  = $this->BonusFlayBoard2 * $kaliFly2;

            $simpananUraianFly2 = $WalletCls->getUraianJenisKhusus($jenisFly2, 2);
            $simpananKaliFly2   = $WalletCls->WalletKaliKhusus($jenisFly2, 2);

            $nilaiSimpananFly2  = $this->BonusFlayBoard2 * $simpananKaliFly2;

            $jenisSponsor   = $WalletCls->getConstantValue('jnsBonusSponsorFlyBoard2');
            $sumberSponsor  = $WalletCls->getSumberByJenis($jenisSponsor);
            $uraianSponsor  = $WalletCls->getUraianJenis($jenisSponsor);
            $kaliSponsor    = $WalletCls->WalletKali($jenisSponsor);

            $nilaiSponsor   = $this->BonusSponsorFlayBoard2 * $kaliSponsor;

            $simpananUraianSponsor  = $WalletCls->getUraianJenisKhusus($jenisSponsor, 2);
            $simpananKaliSponsor    = $WalletCls->WalletKaliKhusus($jenisSponsor, 2);

            $nilaiSimpananSponsor   = $this->BonusSponsorFlayBoard2 * $simpananKaliSponsor;

            $jenisFirstLine   = $WalletCls->getConstantValue('jnsBonusSponsorFlyBoard2');
            $sumberFirstLine  = $WalletCls->getSumberByJenis($jenisFirstLine);
            $uraianFirstLine  = $WalletCls->getUraianJenis($jenisFirstLine);
            $kaliFirstLine    = $WalletCls->WalletKali($jenisFirstLine);

            $nilaiFirstLine   = $this->BonusFirstLineFlayBoard2 * $kaliSponsor;

            $simpananUraianFirstLine  = $WalletCls->getUraianJenisKhusus($jenisFirstLine, 2);
            $simpananKaliFirstLine    = $WalletCls->WalletKaliKhusus($jenisFirstLine, 2);

            $nilaiSimpananFirstLine   = $this->BonusFirstLineFlayBoard2 * $simpananKaliFirstLine;

            foreach ($FlyList->FlyBoard2To2 as $row3) {
                //  fly
                $sumberIDFlay2      = $row3->idNewBoard;
                $WMember            = $row3->WalletMember;
                
                $okFly1     = $WalletCls->AddHistory($WMember, $jenisFly2, $nilaiFly2, $sumberFly2, $sumberIDFlay2, $uraianFly1, true);
                if ($okFly1->Success == false) {
                    $suksesFly2 = false;
                    break;
                }

                $uraianSimpananFly2 = $simpananUraianFly2 . ' - ' . $row3->namaMember . ' (2 to 2)';

                $okSimpananFly2     = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisFly2, $nilaiSimpananFly2, $sumberFly2, $sumberIDFlay2, $uraianSimpananFly2, true);
                if ($okSimpananFly2->Success == false) {
                    $suksesFly2 = false;
                    break;
                }

                //  sponsor dari yg fly
                $sumberIDSponsor    = $row3->idNewBoard;
                $WSponsor           = $row3->WalletSponsor;
                $sponsorUraian      = $uraianSponsor .  ' - ' . $row3->namaMember . ' (2 to 2)';

                $okSponsor  = $WalletCls->AddHistory($WSponsor, $jenisSponsor, $nilaiSponsor, $sumberSponsor, $sumberIDSponsor, $sponsorUraian, true);
                if ($okSponsor->Success == false) {
                    $suksesFly2 = false;
                    break;
                }

                $uraianSimpananSponsor = $simpananUraianSponsor . ' - ' . $row3->namaMember . ' (2 to 2)';

                $okSimpananSponsor  = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisSponsor, $nilaiSimpananSponsor, $sumberSponsor, $sumberIDSponsor, $uraianSimpananSponsor, true);
                if ($okSimpananSponsor->Success == false) {
                    $suksesFly2 = false;
                    break;
                }

                //  first line yg ditinggalkan di board 2 sebelumnya
                if (!empty($row3->WalletFirstLine)) {
                    foreach ($row3->WalletFirstLine as $key => $value) {
                        $sumberIDFirstLine  = $value->oldBoardID;
                        $WFirstLine         = $value->WalletData;

                        $firstLineUraian      = $uraianFirstLine .  ' - ' . $row3->namaMember . ' (2 to 2)';

                        $okFirstLine  = $WalletCls->AddHistory($WFirstLine, $jenisFirstLine, $nilaiFirstLine, $sumberFirstLine, $sumberIDFirstLine, $firstLineUraian, true);
                        if ($okFirstLine->Success == false) {
                            $suksesFly2 = false;
                            break;
                        }

                        $uraianSimpananFirstLine    = $simpananUraianFirstLine . ' - ' . $row3->namaMember . ' (2 to 2)';
                        $okSimpananFisrtLine        = $WalletCls->AddHistoryKhusus($WalletSimpanan, $jenisFirstLine, $nilaiSimpananFirstLine, 
                                                                                    $sumberFirstLine, $sumberIDFirstLine, $uraianSimpananFirstLine, true);
                        if ($okSimpananFisrtLine->Success == false) {
                            $suksesFly2 = false;
                            break;
                        }
                    }
                }

                if ($suksesFly2 == false) break;
            }
        }

        if ($suksesFly2 == false) return false;

        return true;
    }
}


