<?php

namespace financialModel;

use DB;

class BonusSettings {

    //  sponsor
    protected $BonusSponsor         = 60000;
    
    //  binary
    protected $BonusBinaryGanjil    = 35000;
    protected $BonusBinaryGenap     = 75000;

    //  matrix
    protected $BonusMatrix  = array(1   => 5000,
                                    2   => 4000,
                                    3   => 3000,
                                    4   => 2000,
                                    5   => 1000,
                                    6   => 1000,
                                    7   => 2000,
                                    8   => 3000,
                                    9   => 4000,
                                    10  => 5000);

    //  board 1
    protected $BonusFlayBoard1          = 500000;
    protected $BonusNewMemberBoard1     = 5000;     //  waiting list
    protected $BonusCompanyMargin       = 200000;

    //  board 2
    protected $BonusFlayBoard2          = 4000000;
    protected $BonusSponsorFlayBoard2   = 250000;
    protected $BonusFirstlineFlayBoard2 = 100000;
}


