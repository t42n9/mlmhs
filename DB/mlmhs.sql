-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2016 at 07:37 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mlmhs`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `getTempBinaryCode`(`baris` INT, `side` INT) RETURNS varchar(200) CHARSET utf8 COLLATE utf8_unicode_ci
    NO SQL
BEGIN

DECLARE awal INTEGER;
DECLARE urut INTEGER;
DECLARE kaki INTEGER;
DECLARE pside INTEGER;
DECLARE pkaki INTEGER;

DECLARE hasil VARCHAR(200);

IF (baris < 1) THEN
	SET hasil = 'NOTHING';
ELSEIF (baris = 1) THEN
	SET hasil = 'B-1.1';
ELSE
	SET kaki = 1;
    IF (MOD(side, 2) > 0) THEN
    	SET kaki = 1;
    ELSE 
    	SET kaki = 2;
    END IF;
    
	SET hasil = CONCAT('-', baris, '.', kaki);
    SET pside = side;
    SET awal = 1;
    SET urut = baris - 1;
    WHILE (urut > awal) DO
    	SET pside = CEILING(pside / 2);
        IF (MOD(pside, 2) > 0) THEN
    		SET kaki = 1;
    	ELSE 
    		SET kaki = 2;
    	END IF;
        SET hasil = CONCAT('-', urut, '.', kaki, hasil);
        SET urut = urut - 1;
    END WHILE;
    SET hasil = CONCAT('B-1.1', hasil);
END IF;

RETURN hasil;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `binary_struktur`
--

CREATE TABLE IF NOT EXISTS `binary_struktur` (
`id` bigint(20) NOT NULL,
  `kode` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `id_parent` bigint(20) NOT NULL,
  `baris_urut` int(11) NOT NULL,
  `kaki_urut` smallint(4) NOT NULL,
  `side_urut` int(11) NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `binary_struktur`
--

INSERT INTO `binary_struktur` (`id`, `kode`, `id_parent`, `baris_urut`, `kaki_urut`, `side_urut`, `id_member`, `created_at`) VALUES
(1, 'B-1.1', 0, 1, 1, 1, 2, '2016-04-19 07:09:29'),
(2, 'B-1.1-2.1', 1, 2, 1, 1, 3, '2016-04-19 07:12:15'),
(3, 'B-1.1-2.2', 1, 2, 2, 2, 4, '2016-04-19 07:13:10'),
(4, 'B-1.1-2.1-3.1', 2, 3, 1, 1, 5, '2016-04-19 07:15:34'),
(5, 'B-1.1-2.1-3.2', 2, 3, 2, 2, 6, '2016-04-19 07:16:18'),
(6, 'B-1.1-2.2-3.1', 3, 3, 1, 3, 7, '2016-04-19 07:17:09'),
(7, 'B-1.1-2.2-3.2', 3, 3, 2, 4, 8, '2016-04-19 07:17:33'),
(9, 'B-1.1-2.1-3.1-4.1', 4, 4, 1, 1, 10, '2016-04-20 05:49:10'),
(10, 'B-1.1-2.1-3.1-4.2', 4, 4, 2, 2, 11, '2016-04-20 05:52:01'),
(11, 'B-1.1-2.1-3.2-4.1', 5, 4, 1, 3, 12, '2016-04-20 06:05:09'),
(12, 'B-1.1-2.1-3.2-4.2', 5, 4, 2, 4, 13, '2016-04-20 06:05:30'),
(13, 'B-1.1-2.2-3.1-4.1', 6, 4, 1, 5, 14, '2016-04-20 06:06:43'),
(14, 'B-1.1-2.2-3.1-4.2', 6, 4, 2, 6, 15, '2016-04-20 06:07:05'),
(15, 'B-1.1-2.2-3.2-4.1', 7, 4, 1, 7, 16, '2016-04-20 06:11:46'),
(17, 'B-1.1-2.2-3.2-4.2', 7, 4, 2, 8, 18, '2016-04-20 06:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
`id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `board_type` smallint(2) NOT NULL COMMENT '1=original board, 2=dest board',
  `max_member` int(11) NOT NULL,
  `jml_member` int(11) NOT NULL DEFAULT '0',
  `id_fromboard` int(11) NOT NULL,
  `is_closed` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=open, 1=closed',
  `closed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`id`, `created_at`, `board_type`, `max_member`, `jml_member`, `id_fromboard`, `is_closed`, `closed_at`) VALUES
(1, '2016-04-19 07:09:29', 1, 15, 15, 0, 1, '2016-04-20 06:13:53'),
(2, '2016-04-20 06:13:53', 2, 7, 1, 1, 0, NULL),
(3, '2016-04-20 06:13:53', 1, 15, 7, 1, 0, NULL),
(4, '2016-04-20 06:13:53', 1, 15, 7, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `board_member`
--

CREATE TABLE IF NOT EXISTS `board_member` (
`id` bigint(20) NOT NULL,
  `id_board` int(11) NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `baris_urut` smallint(4) NOT NULL,
  `id_parent` bigint(20) NOT NULL,
  `kaki_urut` smallint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_lastboard` int(11) NOT NULL DEFAULT '0',
  `id_next_dest` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `board_member`
--

INSERT INTO `board_member` (`id`, `id_board`, `id_member`, `baris_urut`, `id_parent`, `kaki_urut`, `created_at`, `id_lastboard`, `id_next_dest`) VALUES
(1, 1, 2, 1, 0, 1, '2016-04-19 07:09:29', 0, 0),
(2, 1, 3, 2, 1, 1, '2016-04-19 07:12:15', 0, 0),
(3, 1, 4, 2, 1, 2, '2016-04-19 07:13:10', 0, 0),
(4, 1, 5, 3, 2, 1, '2016-04-19 07:15:34', 0, 0),
(5, 1, 6, 3, 2, 2, '2016-04-19 07:16:18', 0, 0),
(6, 1, 7, 3, 3, 1, '2016-04-19 07:17:09', 0, 0),
(7, 1, 8, 3, 3, 2, '2016-04-19 07:17:33', 0, 0),
(9, 1, 10, 4, 4, 1, '2016-04-20 05:49:10', 0, 0),
(10, 1, 11, 4, 4, 2, '2016-04-20 05:52:01', 0, 0),
(11, 1, 12, 4, 5, 1, '2016-04-20 06:05:09', 0, 0),
(12, 1, 13, 4, 5, 2, '2016-04-20 06:05:30', 0, 0),
(13, 1, 14, 4, 6, 1, '2016-04-20 06:06:43', 0, 0),
(14, 1, 15, 4, 6, 2, '2016-04-20 06:07:05', 0, 0),
(15, 1, 16, 4, 7, 1, '2016-04-20 06:11:46', 0, 0),
(17, 1, 18, 4, 7, 2, '2016-04-20 06:13:53', 0, 0),
(18, 2, 2, 1, 0, 1, '2016-04-20 06:13:53', 1, 0),
(19, 3, 3, 1, 0, 1, '2016-04-20 06:13:53', 1, 2),
(20, 3, 5, 2, 19, 1, '2016-04-20 06:13:53', 1, 0),
(21, 3, 6, 2, 19, 2, '2016-04-20 06:13:53', 1, 0),
(22, 3, 10, 3, 20, 1, '2016-04-20 06:13:53', 1, 0),
(23, 3, 11, 3, 20, 2, '2016-04-20 06:13:53', 1, 0),
(24, 3, 12, 3, 21, 1, '2016-04-20 06:13:53', 1, 0),
(25, 3, 13, 3, 21, 2, '2016-04-20 06:13:53', 1, 0),
(26, 4, 4, 1, 0, 1, '2016-04-20 06:13:53', 1, 2),
(27, 4, 7, 2, 26, 1, '2016-04-20 06:13:53', 1, 0),
(28, 4, 8, 2, 26, 2, '2016-04-20 06:13:53', 1, 0),
(29, 4, 14, 3, 27, 1, '2016-04-20 06:13:53', 1, 0),
(30, 4, 15, 3, 27, 2, '2016-04-20 06:13:53', 1, 0),
(31, 4, 16, 3, 28, 1, '2016-04-20 06:13:53', 1, 0),
(32, 4, 18, 3, 28, 2, '2016-04-20 06:13:53', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `matrix_struktur`
--

CREATE TABLE IF NOT EXISTS `matrix_struktur` (
`id` bigint(20) NOT NULL,
  `id_parent` bigint(20) NOT NULL,
  `baris_urut` int(11) NOT NULL,
  `kaki_urut` smallint(4) NOT NULL,
  `side_urut` int(11) NOT NULL,
  `kode` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `matrix_struktur`
--

INSERT INTO `matrix_struktur` (`id`, `id_parent`, `baris_urut`, `kaki_urut`, `side_urut`, `kode`, `id_member`, `created_at`) VALUES
(1, 0, 1, 1, 1, 'M-1.1', 2, '2016-04-19 07:09:29'),
(2, 1, 2, 1, 1, 'M-1.1-2.1', 3, '2016-04-19 07:12:15'),
(3, 1, 2, 2, 2, 'M-1.1-2.2', 4, '2016-04-19 07:13:10'),
(4, 2, 3, 1, 1, 'M-1.1-2.1-3.1', 5, '2016-04-19 07:15:34'),
(5, 2, 3, 2, 2, 'M-1.1-2.1-3.2', 6, '2016-04-19 07:16:18'),
(6, 3, 3, 1, 6, 'M-1.1-2.2-3.1', 7, '2016-04-19 07:17:09'),
(7, 3, 3, 2, 7, 'M-1.1-2.2-3.2', 8, '2016-04-19 07:17:33'),
(9, 4, 4, 1, 1, 'M-1.1-2.1-3.1-4.1', 10, '2016-04-20 05:49:10'),
(10, 4, 4, 2, 2, 'M-1.1-2.1-3.1-4.2', 11, '2016-04-20 05:52:01'),
(11, 5, 4, 1, 6, 'M-1.1-2.1-3.2-4.1', 12, '2016-04-20 06:05:09'),
(12, 5, 4, 2, 7, 'M-1.1-2.1-3.2-4.2', 13, '2016-04-20 06:05:30'),
(13, 6, 4, 1, 26, 'M-1.1-2.2-3.1-4.1', 14, '2016-04-20 06:06:43'),
(14, 6, 4, 2, 27, 'M-1.1-2.2-3.1-4.2', 15, '2016-04-20 06:07:05'),
(15, 7, 4, 1, 31, 'M-1.1-2.2-3.2-4.1', 16, '2016-04-20 06:11:46'),
(17, 7, 4, 2, 32, 'M-1.1-2.2-3.2-4.2', 18, '2016-04-20 06:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
`id` bigint(20) NOT NULL,
  `kode` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member_type` smallint(4) NOT NULL DEFAULT '6',
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_sponsor` bigint(20) NOT NULL,
  `registered_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` smallint(2) NOT NULL DEFAULT '0',
  `status_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `kode`, `email`, `password`, `member_type`, `nama`, `id_sponsor`, `registered_at`, `status`, `status_at`) VALUES
(1, '570d2bcd3844d', 'hs2016@gmail.com', '$2y$10$4IuS4wyfYZD6XtTtlZHF6.6KkOYCl91yIErtTn5LjBNT.Fl4RAi7C', 1, 'Administrator', 0, '2016-04-12 17:09:33', 1, '2016-04-12 17:09:33'),
(2, '5715d9a8f1aeb', 'member1@gmail.com', '$2y$10$SvfDpoOkqkptXx24RJrR6.DauzZi4EIyOFGKYdg.hIlcB7pi3myyu', 6, 'Member 1', 0, '2016-04-19 07:09:29', 1, '2016-04-19 07:09:28'),
(3, '5715da4f83618', 'member2@gmail.com', '$2y$10$Sc0GF135L0Rds6Pwr8QkleN.5B.Sjdr5GxkXNqJF4b7ygsX7SMkZ2', 6, 'Member 2', 2, '2016-04-19 07:12:15', 1, '2016-04-19 07:12:15'),
(4, '5715da861aab9', 'member3@gmail.com', '$2y$10$6LcnOUWmPTqt8/9dkJ00DejKY9nIlwUFfodWmS5n0PARHYDcUS0AK', 6, 'Member 3', 2, '2016-04-19 07:13:10', 1, '2016-04-19 07:13:10'),
(5, '5715db1666286', 'member4@gmail.com', '$2y$10$KbsvzcCEd5CgvaN4pYukwePG86HC1Ks.zX0KNaoxSLWBNT/jLuxsm', 6, 'Member 4', 3, '2016-04-19 07:15:34', 1, '2016-04-19 07:15:34'),
(6, '5715db425f605', 'member5@gmail.com', '$2y$10$pnDVvpOeVClZnYYp/hra8uAQE9td6zvuIZu/W9gE7bOYvjJVbAAGG', 6, 'Member 5', 3, '2016-04-19 07:16:18', 1, '2016-04-19 07:16:18'),
(7, '5715db75373a6', 'member6@gmail.com', '$2y$10$FFL0xUkK342MVDqpJliP5.hfVt/xXUMyuSV.VgILeJ8FXEd04RhoK', 6, 'Member 6', 4, '2016-04-19 07:17:09', 1, '2016-04-19 07:17:09'),
(8, '5715db8d1bdec', 'member7@gmail.com', '$2y$10$B6t54z46sllXoe2C4kGzbOmIODe6ZDfGd.GKZr7Lhq7jXa9SCuRX6', 6, 'Member 7', 4, '2016-04-19 07:17:33', 1, '2016-04-19 07:17:33'),
(10, '571718562e56a', 'member8@gmail.com', '$2y$10$XXPlDX3emZ8IZvDawCDpEO3cIHqAu4DA0lxdAIFLlNNM8L9kyqvIe', 6, 'Member 8', 5, '2016-04-20 05:49:10', 1, '2016-04-20 05:49:10'),
(11, '571719017b976', 'member9@gmail.com', '$2y$10$gOEb2lmpr1es9lEZlDBTGu83mOmoPIYIniWyhP5/8EZy.tNz6KU0e', 6, 'Member 9', 5, '2016-04-20 05:52:01', 1, '2016-04-20 05:52:01'),
(12, '57171c153c14b', 'member10@gmail.com', '$2y$10$sdA65SFl.BYoC2BsqkItBed4H54swkjkhb2W3wt4Jqjcts7SJej2e', 6, 'Member 10', 6, '2016-04-20 06:05:09', 1, '2016-04-20 06:05:09'),
(13, '57171c2a9720b', 'member11@gmail.com', '$2y$10$ecuCqvMlynAAE4/iZcRT9.6BgsAsaJA6hW4Jvwv9LjWAs9SGI97Se', 6, 'Member 11', 6, '2016-04-20 06:05:30', 1, '2016-04-20 06:05:30'),
(14, '57171c72e502f', 'member12@gmail.com', '$2y$10$c9ls7Enx6LyGThted6HiPOnNxgQfBDo3r0CuLBYNf3ib6VCIg1.D6', 6, 'Member 12', 7, '2016-04-20 06:06:43', 1, '2016-04-20 06:06:42'),
(15, '57171c8992459', 'member13@gmail.com', '$2y$10$DUPyjwjPmG.PraOZLXophejbdbYrdk5.WA.vcD57w4/bfuJuGl2Sa', 6, 'Member 13', 7, '2016-04-20 06:07:05', 1, '2016-04-20 06:07:05'),
(16, '57171da2772ac', 'member14@gmail.com', '$2y$10$e6wkrnr1/6cL0gb62PJqZezSmUPVlG75GZ/hm4lkKnZWOop8mpD.u', 6, 'Member 14', 8, '2016-04-20 06:11:46', 1, '2016-04-20 06:11:46'),
(18, '57171e21a9801', 'member15@gmail.com', '$2y$10$6gkYTJKrAaM9oDob32QeOexC4b6Qp06.9Qxia.b1NGYNkKaQrLW2y', 6, 'Member 15', 8, '2016-04-20 06:13:53', 1, '2016-04-20 06:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `tiket_register`
--

CREATE TABLE IF NOT EXISTS `tiket_register` (
`id` bigint(20) NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `tiket_nomor` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_tiket` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `harga_tiket` int(11) NOT NULL DEFAULT '0',
  `is_used` tinyint(4) NOT NULL DEFAULT '0',
  `used_at` timestamp NULL DEFAULT NULL,
  `used_by_member` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tiket_register`
--

INSERT INTO `tiket_register` (`id`, `id_member`, `tiket_nomor`, `tgl_tiket`, `harga_tiket`, `is_used`, `used_at`, `used_by_member`) VALUES
(1, 5, '8087479713833', '2016-04-19 08:27:37', 10000, 1, '2016-04-20 05:49:10', 10),
(2, 5, '3499035169443', '2016-04-19 08:27:39', 10000, 1, '2016-04-20 05:52:01', 11),
(3, 6, '0307215524068', '2016-04-20 06:04:47', 10000, 1, '2016-04-20 06:05:09', 12),
(4, 6, '7989094991823', '2016-04-20 06:04:49', 10000, 1, '2016-04-20 06:05:30', 13),
(5, 7, '4080790955183', '2016-04-20 06:06:10', 10000, 1, '2016-04-20 06:06:43', 14),
(6, 7, '2826532536547', '2016-04-20 06:06:12', 10000, 1, '2016-04-20 06:07:05', 15),
(7, 8, '9548173001570', '2016-04-20 06:11:22', 10000, 1, '2016-04-20 06:11:46', 16),
(8, 8, '3536689998027', '2016-04-20 06:11:24', 10000, 1, '2016-04-20 06:13:53', 18);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_jenis`
--

CREATE TABLE IF NOT EXISTS `transaksi_jenis` (
  `id` int(4) NOT NULL,
  `uraian` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `kali` smallint(2) NOT NULL,
  `sumber` smallint(2) NOT NULL,
  `uraian_khusus1` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kali_khusus1` smallint(2) NOT NULL DEFAULT '0',
  `uraian_khusus2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kali_khusus2` smallint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transaksi_jenis`
--

INSERT INTO `transaksi_jenis` (`id`, `uraian`, `kali`, `sumber`, `uraian_khusus1`, `kali_khusus1`, `uraian_khusus2`, `kali_khusus2`) VALUES
(1, 'Saldo Awal Member Baru', 1, 1, 'Pendaftaran Member Baru', 1, 'Pendaftaran Member Baru', 1),
(2, 'Pengisian Saldo / Top Up', 1, 2, NULL, 0, NULL, 0),
(3, 'Bonus Mendaftarkan Member Baru', 1, 4, NULL, 0, 'Bonus Mendaftarkan Member Baru', -1),
(4, 'Bonus Struktur Binary Ganjil', 1, 6, NULL, 0, 'Bonus Struktur Binary Ganjil', -1),
(5, 'Bonus Struktur Binary Genap', 1, 6, NULL, 0, 'Bonus Struktur Binary Genap', -1),
(6, 'Bonus Member Baru Di Struktur Matrix', 1, 7, NULL, 0, 'Bonus Member Baru Di Struktur Matrix', -1),
(7, 'Bonus Fly Dari Board I Ke Board II', 1, 5, 'Margin Fly Board I Ke Board II', 1, 'Bonus Fly Dari Board I Ke Board II', -1),
(8, 'Bonus Waiting List Di Board I', 1, 4, NULL, 0, 'Bonus Waiting List Di Board I', -1),
(9, 'Bonus Fly Di Board II Ke Board II Yang Lain', 1, 5, NULL, 0, 'Bonus Fly Di Board II Ke Board II Yang Lain', -1),
(10, 'Bonus Sponsor Dari Member Yang Mengalamai Fly Board II', 1, 5, NULL, 0, 'Bonus Sponsor Dari Member Yang Mengalamai Fly Board II', -1),
(11, 'Bonus Downline Baris Pertama Dalam Struktur Binary Dari Member Yang Mengalamai Fly Board II', 1, 5, NULL, 0, 'Bonus Downline Baris Pertama Dalam Struktur Binary Dari Member Yang Mengalamai Fly Board II', -1),
(12, 'Pembelian Tiket Untuk Mendaftarkan Member Baru', -1, 3, 'Penjualan Tiket Untuk Mendaftarkan Member Baru', 1, NULL, 0),
(13, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan', -1, 8, NULL, 0, NULL, 0),
(14, 'Pembelian Pulsa', -1, 9, 'Penjualan Pulsa', 1, NULL, 0),
(15, 'Penarikan Saldo Langsung / Withdraw', -1, 10, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_sumber`
--

CREATE TABLE IF NOT EXISTS `transaksi_sumber` (
  `id` int(4) NOT NULL,
  `uraian` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transaksi_sumber`
--

INSERT INTO `transaksi_sumber` (`id`, `uraian`) VALUES
(1, 'Saldo Awal, id sumbernya null'),
(2, 'Top Up, id sumbernya null'),
(3, 'Beli Tiket, id sumbernya adalah id tiket'),
(4, 'Bonus yang diperoleh dari member, id sumbernya adalah id member yang mengalirkan bonus'),
(5, 'Bonus yang diperoleh dari fly board, id sumbernya adalah id board member'),
(6, 'Bonus yang diperoleh dari Binary, id sumbernya adalah id binary'),
(7, 'Bonus yang diperoleh dari Matrix, id sumbernya adalah id matrix'),
(8, 'Pengambilan Saldo untuk member yang didaftarkan, id sumbernya adalah id member yang didaftarkan'),
(9, 'Pembelian Pulsa, id sumbernya adalan id pembelian'),
(10, 'Withdraw, id sumbernya adalah id withdraw');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vbinary_baris`
--
CREATE TABLE IF NOT EXISTS `vbinary_baris` (
`baris` bigint(20)
,`pbaris` bigint(12)
,`max_elemen` double
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vbinary_kaki`
--
CREATE TABLE IF NOT EXISTS `vbinary_kaki` (
`urut` bigint(20)
);
-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE IF NOT EXISTS `wallet` (
`id` bigint(20) NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `nomor` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `debet` bigint(20) NOT NULL DEFAULT '0',
  `kredit` bigint(20) NOT NULL DEFAULT '0',
  `saldo` bigint(20) NOT NULL DEFAULT '0',
  `tgl_wallet` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `id_member`, `nomor`, `debet`, `kredit`, `saldo`, `tgl_wallet`) VALUES
(1, 2, '9739792167628', 0, 535000, 535000, '2016-04-19 07:09:29'),
(2, 3, '4557241792972', 0, 40000, 40000, '2016-04-19 07:12:15'),
(3, 4, '2777916017187', 0, 40000, 40000, '2016-04-19 07:13:10'),
(4, 5, '8165984331354', -1820000, 2040000, 220000, '2016-04-19 07:15:34'),
(5, 6, '0478098967516', -1820000, 2040000, 220000, '2016-04-19 07:16:18'),
(6, 7, '9157032659433', -1820000, 2644815, 824815, '2016-04-19 07:17:09'),
(7, 8, '5081174525419', -1820000, 2040000, 220000, '2016-04-19 07:17:33'),
(9, 10, '3789814573464', 0, 240000, 240000, '2016-04-20 05:49:10'),
(10, 11, '5693911577523', 0, 240000, 240000, '2016-04-20 05:52:01'),
(11, 12, '2322818302365', 0, 240000, 240000, '2016-04-20 06:05:09'),
(12, 13, '1297940988291', 0, 240000, 240000, '2016-04-20 06:05:30'),
(13, 14, '0608481686826', 0, 240000, 240000, '2016-04-20 06:06:43'),
(14, 15, '9166429525553', 0, 240000, 240000, '2016-04-20 06:07:05'),
(15, 16, '7858634940046', 0, 240000, 240000, '2016-04-20 06:11:46'),
(17, 18, '0815396598903', 0, 240000, 240000, '2016-04-20 06:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_history`
--

CREATE TABLE IF NOT EXISTS `wallet_history` (
`id` bigint(20) NOT NULL,
  `id_wallet` bigint(20) NOT NULL,
  `tgl_history` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_jenis` int(4) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `id_sumber` int(4) NOT NULL,
  `sumber_id` bigint(20) NOT NULL DEFAULT '0',
  `keterangan` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=131 ;

--
-- Dumping data for table `wallet_history`
--

INSERT INTO `wallet_history` (`id`, `id_wallet`, `tgl_history`, `id_jenis`, `nilai`, `id_sumber`, `sumber_id`, `keterangan`) VALUES
(1, 4, '2016-04-19 07:15:34', 1, 2000000, 1, 0, 'Saldo Awal Member Baru'),
(2, 5, '2016-04-19 07:16:18', 1, 2000000, 1, 0, 'Saldo Awal Member Baru'),
(3, 6, '2016-04-19 07:17:09', 1, 2000000, 1, 0, 'Saldo Awal Member Baru'),
(4, 7, '2016-04-19 07:17:33', 1, 2000000, 1, 0, 'Saldo Awal Member Baru'),
(5, 4, '2016-04-19 08:27:37', 12, -10000, 3, 1, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 8087479713833'),
(6, 4, '2016-04-19 08:27:39', 12, -10000, 3, 2, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 3499035169443'),
(13, 9, '2016-04-20 05:49:10', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(14, 4, '2016-04-20 05:49:10', 13, -900000, 8, 10, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 8'),
(15, 4, '2016-04-20 05:49:10', 3, 60000, 4, 10, 'Bonus Mendaftarkan Member Baru - Member 8'),
(16, 4, '2016-04-20 05:49:10', 6, 5000, 7, 9, 'Bonus Member Baru Di Struktur Matrix - Member 8 (Baris Ke- 1)'),
(17, 2, '2016-04-20 05:49:10', 6, 4000, 7, 9, 'Bonus Member Baru Di Struktur Matrix - Member 8 (Baris Ke- 2)'),
(18, 1, '2016-04-20 05:49:10', 6, 3000, 7, 9, 'Bonus Member Baru Di Struktur Matrix - Member 8 (Baris Ke- 3)'),
(19, 1, '2016-04-20 05:49:10', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(20, 2, '2016-04-20 05:49:10', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(21, 3, '2016-04-20 05:49:10', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(22, 4, '2016-04-20 05:49:10', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(23, 5, '2016-04-20 05:49:10', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(24, 6, '2016-04-20 05:49:10', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(25, 7, '2016-04-20 05:49:10', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(26, 10, '2016-04-20 05:52:01', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(27, 4, '2016-04-20 05:52:01', 13, -900000, 8, 11, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 9'),
(28, 4, '2016-04-20 05:52:01', 3, 60000, 4, 11, 'Bonus Mendaftarkan Member Baru - Member 9'),
(29, 4, '2016-04-20 05:52:01', 4, 35000, 6, 10, 'Bonus Struktur Binary Ganjil - Member 9'),
(30, 2, '2016-04-20 05:52:01', 5, 75000, 6, 10, 'Bonus Struktur Binary Genap - Member 9'),
(31, 1, '2016-04-20 05:52:01', 4, 35000, 6, 10, 'Bonus Struktur Binary Ganjil - Member 9'),
(32, 4, '2016-04-20 05:52:01', 6, 5000, 7, 10, 'Bonus Member Baru Di Struktur Matrix - Member 9 (Baris Ke- 1)'),
(33, 2, '2016-04-20 05:52:01', 6, 4000, 7, 10, 'Bonus Member Baru Di Struktur Matrix - Member 9 (Baris Ke- 2)'),
(34, 1, '2016-04-20 05:52:01', 6, 3000, 7, 10, 'Bonus Member Baru Di Struktur Matrix - Member 9 (Baris Ke- 3)'),
(35, 1, '2016-04-20 05:52:01', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(36, 2, '2016-04-20 05:52:01', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(37, 3, '2016-04-20 05:52:01', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(38, 4, '2016-04-20 05:52:01', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(39, 5, '2016-04-20 05:52:01', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(40, 6, '2016-04-20 05:52:01', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(41, 7, '2016-04-20 05:52:01', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(42, 5, '2016-04-20 06:04:47', 12, -10000, 3, 3, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 0307215524068'),
(43, 5, '2016-04-20 06:04:49', 12, -10000, 3, 4, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 7989094991823'),
(44, 11, '2016-04-20 06:05:09', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(45, 5, '2016-04-20 06:05:09', 13, -900000, 8, 12, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 10'),
(46, 5, '2016-04-20 06:05:09', 3, 60000, 4, 12, 'Bonus Mendaftarkan Member Baru - Member 10'),
(47, 5, '2016-04-20 06:05:09', 6, 5000, 7, 11, 'Bonus Member Baru Di Struktur Matrix - Member 10 (Baris Ke- 1)'),
(48, 2, '2016-04-20 06:05:09', 6, 4000, 7, 11, 'Bonus Member Baru Di Struktur Matrix - Member 10 (Baris Ke- 2)'),
(49, 1, '2016-04-20 06:05:09', 6, 3000, 7, 11, 'Bonus Member Baru Di Struktur Matrix - Member 10 (Baris Ke- 3)'),
(50, 1, '2016-04-20 06:05:09', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(51, 2, '2016-04-20 06:05:09', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(52, 3, '2016-04-20 06:05:09', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(53, 4, '2016-04-20 06:05:09', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(54, 5, '2016-04-20 06:05:09', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(55, 6, '2016-04-20 06:05:09', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(56, 7, '2016-04-20 06:05:09', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(57, 12, '2016-04-20 06:05:30', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(58, 5, '2016-04-20 06:05:30', 13, -900000, 8, 13, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 11'),
(59, 5, '2016-04-20 06:05:30', 3, 60000, 4, 13, 'Bonus Mendaftarkan Member Baru - Member 11'),
(60, 5, '2016-04-20 06:05:30', 6, 5000, 7, 12, 'Bonus Member Baru Di Struktur Matrix - Member 11 (Baris Ke- 1)'),
(61, 2, '2016-04-20 06:05:30', 6, 4000, 7, 12, 'Bonus Member Baru Di Struktur Matrix - Member 11 (Baris Ke- 2)'),
(62, 1, '2016-04-20 06:05:30', 6, 3000, 7, 12, 'Bonus Member Baru Di Struktur Matrix - Member 11 (Baris Ke- 3)'),
(63, 1, '2016-04-20 06:05:30', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(64, 2, '2016-04-20 06:05:30', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(65, 3, '2016-04-20 06:05:30', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(66, 4, '2016-04-20 06:05:30', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(67, 5, '2016-04-20 06:05:30', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(68, 6, '2016-04-20 06:05:30', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(69, 7, '2016-04-20 06:05:30', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(70, 6, '2016-04-20 06:06:10', 12, -10000, 3, 5, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 4080790955183'),
(71, 6, '2016-04-20 06:06:12', 12, -10000, 3, 6, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 2826532536547'),
(72, 13, '2016-04-20 06:06:43', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(73, 6, '2016-04-20 06:06:43', 13, -900000, 8, 14, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 12'),
(74, 6, '2016-04-20 06:06:43', 3, 60000, 4, 14, 'Bonus Mendaftarkan Member Baru - Member 12'),
(75, 6, '2016-04-20 06:06:43', 6, 5000, 7, 13, 'Bonus Member Baru Di Struktur Matrix - Member 12 (Baris Ke- 1)'),
(76, 3, '2016-04-20 06:06:43', 6, 4000, 7, 13, 'Bonus Member Baru Di Struktur Matrix - Member 12 (Baris Ke- 2)'),
(77, 1, '2016-04-20 06:06:43', 6, 3000, 7, 13, 'Bonus Member Baru Di Struktur Matrix - Member 12 (Baris Ke- 3)'),
(78, 1, '2016-04-20 06:06:43', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(79, 2, '2016-04-20 06:06:43', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(80, 3, '2016-04-20 06:06:43', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(81, 4, '2016-04-20 06:06:43', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(82, 5, '2016-04-20 06:06:43', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(83, 6, '2016-04-20 06:06:43', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(84, 7, '2016-04-20 06:06:43', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(85, 14, '2016-04-20 06:07:05', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(86, 6, '2016-04-20 06:07:05', 13, -900000, 8, 15, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 13'),
(87, 6, '2016-04-20 06:07:05', 3, 60000, 4, 15, 'Bonus Mendaftarkan Member Baru - Member 13'),
(88, 6, '2016-04-20 06:07:05', 6, 5000, 7, 14, 'Bonus Member Baru Di Struktur Matrix - Member 13 (Baris Ke- 1)'),
(89, 3, '2016-04-20 06:07:05', 6, 4000, 7, 14, 'Bonus Member Baru Di Struktur Matrix - Member 13 (Baris Ke- 2)'),
(90, 1, '2016-04-20 06:07:05', 6, 3000, 7, 14, 'Bonus Member Baru Di Struktur Matrix - Member 13 (Baris Ke- 3)'),
(91, 1, '2016-04-20 06:07:05', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(92, 2, '2016-04-20 06:07:05', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(93, 3, '2016-04-20 06:07:05', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(94, 4, '2016-04-20 06:07:05', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(95, 5, '2016-04-20 06:07:05', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(96, 6, '2016-04-20 06:07:05', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(97, 7, '2016-04-20 06:07:05', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(98, 7, '2016-04-20 06:11:22', 12, -10000, 3, 7, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 9548173001570'),
(99, 7, '2016-04-20 06:11:24', 12, -10000, 3, 8, 'Pembelian Tiket Untuk Mendaftarkan Member Baru - No.Tiket 3536689998027'),
(100, 15, '2016-04-20 06:11:46', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(101, 7, '2016-04-20 06:11:46', 13, -900000, 8, 16, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 14'),
(102, 7, '2016-04-20 06:11:46', 3, 60000, 4, 16, 'Bonus Mendaftarkan Member Baru - Member 14'),
(103, 7, '2016-04-20 06:11:46', 6, 5000, 7, 15, 'Bonus Member Baru Di Struktur Matrix - Member 14 (Baris Ke- 1)'),
(104, 3, '2016-04-20 06:11:46', 6, 4000, 7, 15, 'Bonus Member Baru Di Struktur Matrix - Member 14 (Baris Ke- 2)'),
(105, 1, '2016-04-20 06:11:46', 6, 3000, 7, 15, 'Bonus Member Baru Di Struktur Matrix - Member 14 (Baris Ke- 3)'),
(106, 1, '2016-04-20 06:11:46', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(107, 2, '2016-04-20 06:11:46', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(108, 3, '2016-04-20 06:11:46', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(109, 4, '2016-04-20 06:11:46', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(110, 5, '2016-04-20 06:11:46', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(111, 6, '2016-04-20 06:11:46', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(112, 7, '2016-04-20 06:11:46', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(115, 17, '2016-04-20 06:13:53', 1, 240000, 1, 0, 'Saldo Awal Member Baru'),
(116, 7, '2016-04-20 06:13:53', 13, -900000, 8, 18, 'Pengambilan Saldo Sebagai Modal Awal Bagi Member Yang Didaftarkan - Member 15'),
(117, 7, '2016-04-20 06:13:53', 3, 60000, 4, 18, 'Bonus Mendaftarkan Member Baru - Member 15'),
(118, 7, '2016-04-20 06:13:53', 6, 5000, 7, 17, 'Bonus Member Baru Di Struktur Matrix - Member 15 (Baris Ke- 1)'),
(119, 3, '2016-04-20 06:13:53', 6, 4000, 7, 17, 'Bonus Member Baru Di Struktur Matrix - Member 15 (Baris Ke- 2)'),
(120, 1, '2016-04-20 06:13:53', 6, 3000, 7, 17, 'Bonus Member Baru Di Struktur Matrix - Member 15 (Baris Ke- 3)'),
(121, 1, '2016-04-20 06:13:53', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(122, 2, '2016-04-20 06:13:53', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(123, 3, '2016-04-20 06:13:53', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(124, 4, '2016-04-20 06:13:53', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(125, 5, '2016-04-20 06:13:53', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(126, 6, '2016-04-20 06:13:53', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(127, 7, '2016-04-20 06:13:53', 8, 5000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(128, 1, '2016-04-20 06:13:53', 7, 500000, 5, 2, 'Bonus Fly Dari Board I Ke Board II'),
(129, 6, '2016-04-25 07:26:29', 2, 102776, 2, 1, 'Pengisian Saldo / Top Up - No : 831397758254583'),
(130, 6, '2016-04-26 03:42:38', 2, 502039, 2, 2, 'Pengisian Saldo / Top Up - No : 278751265083702');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_khusus`
--

CREATE TABLE IF NOT EXISTS `wallet_khusus` (
  `id` int(11) NOT NULL,
  `uraian` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `debet` bigint(20) NOT NULL DEFAULT '0',
  `kredit` bigint(20) NOT NULL DEFAULT '0',
  `saldo` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wallet_khusus`
--

INSERT INTO `wallet_khusus` (`id`, `uraian`, `debet`, `kredit`, `saldo`) VALUES
(1, 'Perusahaan', 0, 1080000, 1080000),
(2, 'Simpanan', -1501000, 4480000, 2979000);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_khusus_history`
--

CREATE TABLE IF NOT EXISTS `wallet_khusus_history` (
`id` bigint(20) NOT NULL,
  `id_wk` int(11) NOT NULL,
  `tgl_history` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_jenis` int(4) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `id_sumber` int(4) NOT NULL,
  `sumber_id` bigint(20) NOT NULL DEFAULT '0',
  `keterangan` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=59 ;

--
-- Dumping data for table `wallet_khusus_history`
--

INSERT INTO `wallet_khusus_history` (`id`, `id_wk`, `tgl_history`, `id_jenis`, `nilai`, `id_sumber`, `sumber_id`, `keterangan`) VALUES
(1, 1, '2016-04-19 08:27:37', 12, 10000, 3, 1, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 8087479713833'),
(2, 1, '2016-04-19 08:27:39', 12, 10000, 3, 2, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 3499035169443'),
(7, 1, '2016-04-20 05:49:10', 1, 100000, 1, 10, 'Pendaftaran Member Baru - Member 8'),
(8, 2, '2016-04-20 05:49:10', 1, 560000, 1, 10, 'Pendaftaran Member Baru - Member 8'),
(9, 2, '2016-04-20 05:49:10', 3, -60000, 4, 10, 'Bonus Mendaftarkan Member Baru - Member 8'),
(10, 2, '2016-04-20 05:49:10', 6, -12000, 7, 9, 'Bonus Member Baru Di Struktur Matrix - Member 8'),
(11, 2, '2016-04-20 05:49:10', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 8'),
(12, 1, '2016-04-20 05:52:01', 1, 100000, 1, 11, 'Pendaftaran Member Baru - Member 9'),
(13, 2, '2016-04-20 05:52:01', 1, 560000, 1, 11, 'Pendaftaran Member Baru - Member 9'),
(14, 2, '2016-04-20 05:52:01', 3, -60000, 4, 11, 'Bonus Mendaftarkan Member Baru - Member 9'),
(15, 2, '2016-04-20 05:52:01', 4, -70000, 6, 10, 'Bonus Struktur Binary Ganjil - Member 9'),
(16, 2, '2016-04-20 05:52:01', 5, -75000, 6, 10, 'Bonus Struktur Binary Genap - Member 9'),
(17, 2, '2016-04-20 05:52:01', 6, -12000, 7, 10, 'Bonus Member Baru Di Struktur Matrix - Member 9'),
(18, 2, '2016-04-20 05:52:01', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 9'),
(19, 1, '2016-04-20 06:04:47', 12, 10000, 3, 3, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 0307215524068'),
(20, 1, '2016-04-20 06:04:49', 12, 10000, 3, 4, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 7989094991823'),
(21, 1, '2016-04-20 06:05:09', 1, 100000, 1, 12, 'Pendaftaran Member Baru - Member 10'),
(22, 2, '2016-04-20 06:05:09', 1, 560000, 1, 12, 'Pendaftaran Member Baru - Member 10'),
(23, 2, '2016-04-20 06:05:09', 3, -60000, 4, 12, 'Bonus Mendaftarkan Member Baru - Member 10'),
(24, 2, '2016-04-20 06:05:09', 6, -12000, 7, 11, 'Bonus Member Baru Di Struktur Matrix - Member 10'),
(25, 2, '2016-04-20 06:05:09', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 10'),
(26, 1, '2016-04-20 06:05:30', 1, 100000, 1, 13, 'Pendaftaran Member Baru - Member 11'),
(27, 2, '2016-04-20 06:05:30', 1, 560000, 1, 13, 'Pendaftaran Member Baru - Member 11'),
(28, 2, '2016-04-20 06:05:30', 3, -60000, 4, 13, 'Bonus Mendaftarkan Member Baru - Member 11'),
(29, 2, '2016-04-20 06:05:30', 6, -12000, 7, 12, 'Bonus Member Baru Di Struktur Matrix - Member 11'),
(30, 2, '2016-04-20 06:05:30', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 11'),
(31, 1, '2016-04-20 06:06:10', 12, 10000, 3, 5, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 4080790955183'),
(32, 1, '2016-04-20 06:06:12', 12, 10000, 3, 6, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 2826532536547'),
(33, 1, '2016-04-20 06:06:43', 1, 100000, 1, 14, 'Pendaftaran Member Baru - Member 12'),
(34, 2, '2016-04-20 06:06:43', 1, 560000, 1, 14, 'Pendaftaran Member Baru - Member 12'),
(35, 2, '2016-04-20 06:06:43', 3, -60000, 4, 14, 'Bonus Mendaftarkan Member Baru - Member 12'),
(36, 2, '2016-04-20 06:06:43', 6, -12000, 7, 13, 'Bonus Member Baru Di Struktur Matrix - Member 12'),
(37, 2, '2016-04-20 06:06:43', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 12'),
(38, 1, '2016-04-20 06:07:05', 1, 100000, 1, 15, 'Pendaftaran Member Baru - Member 13'),
(39, 2, '2016-04-20 06:07:05', 1, 560000, 1, 15, 'Pendaftaran Member Baru - Member 13'),
(40, 2, '2016-04-20 06:07:05', 3, -60000, 4, 15, 'Bonus Mendaftarkan Member Baru - Member 13'),
(41, 2, '2016-04-20 06:07:05', 6, -12000, 7, 14, 'Bonus Member Baru Di Struktur Matrix - Member 13'),
(42, 2, '2016-04-20 06:07:05', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 13'),
(43, 1, '2016-04-20 06:11:22', 12, 10000, 3, 7, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 9548173001570'),
(44, 1, '2016-04-20 06:11:24', 12, 10000, 3, 8, 'Penjualan Tiket Untuk Mendaftarkan Member Baru - No.Tiket 3536689998027'),
(45, 1, '2016-04-20 06:11:46', 1, 100000, 1, 16, 'Pendaftaran Member Baru - Member 14'),
(46, 2, '2016-04-20 06:11:46', 1, 560000, 1, 16, 'Pendaftaran Member Baru - Member 14'),
(47, 2, '2016-04-20 06:11:46', 3, -60000, 4, 16, 'Bonus Mendaftarkan Member Baru - Member 14'),
(48, 2, '2016-04-20 06:11:46', 6, -12000, 7, 15, 'Bonus Member Baru Di Struktur Matrix - Member 14'),
(49, 2, '2016-04-20 06:11:46', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 14'),
(52, 1, '2016-04-20 06:13:53', 1, 100000, 1, 18, 'Pendaftaran Member Baru - Member 15'),
(53, 2, '2016-04-20 06:13:53', 1, 560000, 1, 18, 'Pendaftaran Member Baru - Member 15'),
(54, 2, '2016-04-20 06:13:53', 3, -60000, 4, 18, 'Bonus Mendaftarkan Member Baru - Member 15'),
(55, 2, '2016-04-20 06:13:53', 6, -12000, 7, 17, 'Bonus Member Baru Di Struktur Matrix - Member 15'),
(56, 2, '2016-04-20 06:13:53', 8, -35000, 4, 1, 'Bonus Waiting List Di Board I - Member 15'),
(57, 2, '2016-04-20 06:13:53', 7, -500000, 5, 2, 'Bonus Fly Dari Board I Ke Board II - Member 1 (1 to 2)'),
(58, 1, '2016-04-20 06:13:53', 7, 200000, 5, 2, 'Margin Fly Board I Ke Board II - Member 1 (1 to 2)');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_topup`
--

CREATE TABLE IF NOT EXISTS `wallet_topup` (
`id` bigint(20) NOT NULL,
  `nomor_topup` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_topup` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_member` bigint(20) NOT NULL,
  `id_wallet` bigint(20) NOT NULL,
  `transfer_to` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transfer_to_rek` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transfer_to_nama` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_unik` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `digit_unik` int(4) NOT NULL,
  `jml_topup` bigint(20) NOT NULL,
  `jml_transfer` bigint(20) NOT NULL,
  `use_bank` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use_bank_rek` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use_bank_nama` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `tgl_confirmed` timestamp NULL DEFAULT NULL,
  `status` smallint(4) NOT NULL DEFAULT '0',
  `tgl_status` timestamp NULL DEFAULT NULL,
  `status_by` bigint(20) DEFAULT NULL,
  `ket_status` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `wallet_topup`
--

INSERT INTO `wallet_topup` (`id`, `nomor_topup`, `tgl_topup`, `id_member`, `id_wallet`, `transfer_to`, `transfer_to_rek`, `transfer_to_nama`, `tgl_unik`, `digit_unik`, `jml_topup`, `jml_transfer`, `use_bank`, `use_bank_rek`, `use_bank_nama`, `is_confirmed`, `tgl_confirmed`, `status`, `tgl_status`, `status_by`, `ket_status`) VALUES
(1, '831397758254583', '2016-04-22 15:15:59', 7, 6, 'Mandiri', '0202020202', 'HS Company', '2016-04-22', 2776, 100000, 102776, 'BRI', '123456', 'Nama Saya', 1, '2016-04-24 04:53:57', 1, '2016-04-25 07:26:29', 1, 'Diterima, Uang Sudah Masuk.'),
(2, '278751265083702', '2016-04-26 02:51:06', 7, 6, 'BCA', '0101010101', 'HS Company', '2016-04-25', 2039, 500000, 502039, 'BNI', '789654123', 'Sukiyem', 1, '2016-04-26 02:51:38', 1, '2016-04-26 03:47:51', 1, 'test');

-- --------------------------------------------------------

--
-- Structure for view `vbinary_baris`
--
DROP TABLE IF EXISTS `vbinary_baris`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbinary_baris` AS select distinct `binary_struktur`.`baris_urut` AS `baris`,(`binary_struktur`.`baris_urut` - 1) AS `pbaris`,pow(2,(`binary_struktur`.`baris_urut` - 1)) AS `max_elemen` from `binary_struktur` union all select (max(`binary_struktur`.`baris_urut`) + 1) AS `baris`,max(`binary_struktur`.`baris_urut`) AS `pbaris`,pow(2,((max(`binary_struktur`.`baris_urut`) + 1) - 1)) AS `max_elemen` from `binary_struktur`;

-- --------------------------------------------------------

--
-- Structure for view `vbinary_kaki`
--
DROP TABLE IF EXISTS `vbinary_kaki`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbinary_kaki` AS select 1 AS `urut` union select 2 AS `urut`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `binary_struktur`
--
ALTER TABLE `binary_struktur`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_member` (`id_member`), ADD UNIQUE KEY `kode` (`kode`), ADD UNIQUE KEY `baris_urut` (`baris_urut`,`side_urut`);

--
-- Indexes for table `board`
--
ALTER TABLE `board`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_member`
--
ALTER TABLE `board_member`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_board` (`id_board`,`id_member`), ADD UNIQUE KEY `id_board_2` (`id_board`,`baris_urut`,`id_parent`,`kaki_urut`), ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `matrix_struktur`
--
ALTER TABLE `matrix_struktur`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_member` (`id_member`), ADD UNIQUE KEY `kode` (`kode`), ADD UNIQUE KEY `baris_urut` (`baris_urut`,`side_urut`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `kode` (`kode`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `tiket_register`
--
ALTER TABLE `tiket_register`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `tiket_nomor` (`tiket_nomor`), ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `transaksi_jenis`
--
ALTER TABLE `transaksi_jenis`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_sumber`
--
ALTER TABLE `transaksi_sumber`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_member` (`id_member`), ADD UNIQUE KEY `nomor` (`nomor`);

--
-- Indexes for table `wallet_history`
--
ALTER TABLE `wallet_history`
 ADD PRIMARY KEY (`id`), ADD KEY `id_wallet` (`id_wallet`), ADD KEY `jenis` (`id_jenis`), ADD KEY `sumber` (`id_sumber`);

--
-- Indexes for table `wallet_khusus`
--
ALTER TABLE `wallet_khusus`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_khusus_history`
--
ALTER TABLE `wallet_khusus_history`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_topup`
--
ALTER TABLE `wallet_topup`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `nomor_topup` (`nomor_topup`), ADD UNIQUE KEY `tgl_unik` (`tgl_unik`,`digit_unik`), ADD KEY `id_member` (`id_member`), ADD KEY `id_wallet` (`id_wallet`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `binary_struktur`
--
ALTER TABLE `binary_struktur`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `board`
--
ALTER TABLE `board`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `board_member`
--
ALTER TABLE `board_member`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `matrix_struktur`
--
ALTER TABLE `matrix_struktur`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tiket_register`
--
ALTER TABLE `tiket_register`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `wallet_history`
--
ALTER TABLE `wallet_history`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `wallet_khusus_history`
--
ALTER TABLE `wallet_khusus_history`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `wallet_topup`
--
ALTER TABLE `wallet_topup`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `binary_struktur`
--
ALTER TABLE `binary_struktur`
ADD CONSTRAINT `fk_binary_member` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `board_member`
--
ALTER TABLE `board_member`
ADD CONSTRAINT `fk_board` FOREIGN KEY (`id_board`) REFERENCES `board` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_member_board` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `matrix_struktur`
--
ALTER TABLE `matrix_struktur`
ADD CONSTRAINT `fk_matrix_member` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tiket_register`
--
ALTER TABLE `tiket_register`
ADD CONSTRAINT `fk_member_tiket` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
ADD CONSTRAINT `fk_member_wallet` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet_history`
--
ALTER TABLE `wallet_history`
ADD CONSTRAINT `fk_wallet_transaksi` FOREIGN KEY (`id_wallet`) REFERENCES `wallet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_whistory_jenis` FOREIGN KEY (`id_jenis`) REFERENCES `transaksi_jenis` (`id`),
ADD CONSTRAINT `fk_whistory_sumber` FOREIGN KEY (`id_sumber`) REFERENCES `transaksi_sumber` (`id`);

--
-- Constraints for table `wallet_topup`
--
ALTER TABLE `wallet_topup`
ADD CONSTRAINT `fk_member_topup` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_wallet_topup` FOREIGN KEY (`id_wallet`) REFERENCES `wallet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
