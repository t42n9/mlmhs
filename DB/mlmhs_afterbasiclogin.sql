-- phpMyAdmin SQL Dump
-- version 4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2016 at 07:24 PM
-- Server version: 5.5.38
-- PHP Version: 5.4.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mlmhs`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `getTempBinaryCode`(`baris` INT, `side` INT) RETURNS varchar(200) CHARSET utf8 COLLATE utf8_unicode_ci
    NO SQL
BEGIN

DECLARE awal INTEGER;
DECLARE urut INTEGER;
DECLARE kaki INTEGER;
DECLARE pside INTEGER;
DECLARE pkaki INTEGER;

DECLARE hasil VARCHAR(200);

IF (baris < 1) THEN
	SET hasil = 'NOTHING';
ELSEIF (baris = 1) THEN
	SET hasil = 'B-1.1';
ELSE
	SET kaki = 1;
    IF (MOD(side, 2) > 0) THEN
    	SET kaki = 1;
    ELSE 
    	SET kaki = 2;
    END IF;
    
	SET hasil = CONCAT('-', baris, '.', kaki);
    SET pside = side;
    SET awal = 1;
    SET urut = baris - 1;
    WHILE (urut > awal) DO
    	SET pside = CEILING(pside / 2);
        IF (MOD(pside, 2) > 0) THEN
    		SET kaki = 1;
    	ELSE 
    		SET kaki = 2;
    	END IF;
        SET hasil = CONCAT('-', urut, '.', kaki, hasil);
        SET urut = urut - 1;
    END WHILE;
    SET hasil = CONCAT('B-1.1', hasil);
END IF;

RETURN hasil;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `binary_struktur`
--

CREATE TABLE IF NOT EXISTS `binary_struktur` (
  `id` bigint(20) NOT NULL,
  `id_parent` bigint(20) NOT NULL,
  `baris_urut` int(11) NOT NULL,
  `kaki_urut` smallint(4) NOT NULL,
  `side_urut` int(11) NOT NULL,
  `kode` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `binary_struktur`
--

INSERT INTO `binary_struktur` (`id`, `id_parent`, `baris_urut`, `kaki_urut`, `side_urut`, `kode`, `id_member`, `created_at`) VALUES
(1, 0, 1, 1, 1, 'B-1.1', 1, '2016-04-04 11:05:11'),
(2, 1, 2, 2, 2, 'B-1.1-2.2', 2, '2016-04-04 11:11:19'),
(3, 1, 2, 1, 1, 'B-1.1-2.1', 3, '2016-04-04 11:19:09'),
(4, 3, 3, 2, 2, 'B-1.1-2.1-3.2', 4, '2016-04-04 11:19:34'),
(5, 3, 3, 1, 1, 'B-1.1-2.1-3.1', 5, '2016-04-04 11:20:08'),
(6, 2, 3, 1, 3, 'B-1.1-2.2-3.1', 6, '2016-04-04 11:22:13'),
(7, 4, 4, 1, 3, 'B-1.1-2.1-3.2-4.1', 7, '2016-04-12 12:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `board_type` smallint(2) NOT NULL COMMENT '1=original board, 2=dest board',
  `max_member` int(11) NOT NULL,
  `jml_member` int(11) NOT NULL DEFAULT '0',
  `id_fromboard` int(11) NOT NULL,
  `is_closed` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=open, 1=closed',
  `closed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`id`, `created_at`, `board_type`, `max_member`, `jml_member`, `id_fromboard`, `is_closed`, `closed_at`) VALUES
(1, '2016-04-04 11:05:11', 1, 15, 7, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `board_member`
--

CREATE TABLE IF NOT EXISTS `board_member` (
  `id` bigint(20) NOT NULL,
  `id_board` int(11) NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `baris_urut` smallint(4) NOT NULL,
  `id_parent` bigint(20) NOT NULL,
  `kaki_urut` smallint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_lastboard` int(11) NOT NULL DEFAULT '0',
  `id_next_dest` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `board_member`
--

INSERT INTO `board_member` (`id`, `id_board`, `id_member`, `baris_urut`, `id_parent`, `kaki_urut`, `created_at`, `id_lastboard`, `id_next_dest`) VALUES
(1, 1, 1, 1, 0, 1, '2016-04-04 11:05:11', 0, 0),
(2, 1, 2, 2, 1, 1, '2016-04-04 11:11:19', 0, 0),
(3, 1, 3, 2, 1, 2, '2016-04-04 11:19:09', 0, 0),
(4, 1, 4, 3, 2, 1, '2016-04-04 11:19:34', 0, 0),
(5, 1, 5, 3, 2, 2, '2016-04-04 11:20:08', 0, 0),
(6, 1, 6, 3, 3, 1, '2016-04-04 11:22:13', 0, 0),
(7, 1, 7, 3, 3, 2, '2016-04-12 12:05:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `matrix_struktur`
--

CREATE TABLE IF NOT EXISTS `matrix_struktur` (
  `id` bigint(20) NOT NULL,
  `id_parent` bigint(20) NOT NULL,
  `baris_urut` int(11) NOT NULL,
  `kaki_urut` smallint(4) NOT NULL,
  `side_urut` int(11) NOT NULL,
  `kode` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `matrix_struktur`
--

INSERT INTO `matrix_struktur` (`id`, `id_parent`, `baris_urut`, `kaki_urut`, `side_urut`, `kode`, `id_member`, `created_at`) VALUES
(1, 0, 1, 1, 1, 'M-1.1', 1, '2016-04-04 11:05:11'),
(2, 1, 2, 1, 1, 'M-1.1-2.1', 2, '2016-04-04 11:11:19'),
(3, 1, 2, 2, 2, 'M-1.1-2.2', 3, '2016-04-04 11:19:09'),
(4, 1, 2, 3, 3, 'M-1.1-2.3', 4, '2016-04-04 11:19:34'),
(5, 1, 2, 4, 4, 'M-1.1-2.4', 5, '2016-04-04 11:20:08'),
(6, 1, 2, 5, 5, 'M-1.1-2.5', 6, '2016-04-04 11:22:13'),
(7, 4, 3, 1, 11, 'M-1.1-2.3-3.1', 7, '2016-04-12 12:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` bigint(20) NOT NULL,
  `kode` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_sponsor` bigint(20) NOT NULL,
  `registered_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` smallint(2) NOT NULL DEFAULT '0',
  `status_at` timestamp NULL DEFAULT NULL,
  `remember_toker` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `kode`, `email`, `password`, `nama`, `id_sponsor`, `registered_at`, `status`, `status_at`, `remember_toker`) VALUES
(1, '57024a67debf5', 'a@gmail.com', '$2y$10$JUzLY7WAD9dNx2PB.kXqDuwWZzrhUkI2mPadSEeHHhYO9goe64VMi', 'Hadi S', -1, '2016-04-04 11:05:11', 1, '2016-04-04 11:05:11', NULL),
(2, '57024bd76db87', 'b@gmail.com', '$2y$10$JUzLY7WAD9dNx2PB.kXqDuwWZzrhUkI2mPadSEeHHhYO9goe64VMi', 'Budi H', 1, '2016-04-04 11:11:19', 1, '2016-04-04 11:11:19', NULL),
(3, '57024dadd20b1', 'c@gmail.com', '$2y$10$JUzLY7WAD9dNx2PB.kXqDuwWZzrhUkI2mPadSEeHHhYO9goe64VMi', 'Kunto Adi', 1, '2016-04-04 11:19:09', 1, '2016-04-04 11:19:09', NULL),
(4, '57024dc6240ee', 'zaidug@gmail.com', '$2y$10$JUzLY7WAD9dNx2PB.kXqDuwWZzrhUkI2mPadSEeHHhYO9goe64VMi', 'Zaid', 1, '2016-04-04 11:19:34', 1, '2016-04-04 11:19:34', NULL),
(5, '57024de8048ad', 'd@gmail.com', '$2y$10$JUzLY7WAD9dNx2PB.kXqDuwWZzrhUkI2mPadSEeHHhYO9goe64VMi', 'Tatang S', 1, '2016-04-04 11:20:08', 1, '2016-04-04 11:20:08', NULL),
(6, '57024e650ecfa', 'e@gmail.com', '$2y$10$JUzLY7WAD9dNx2PB.kXqDuwWZzrhUkI2mPadSEeHHhYO9goe64VMi', 'Wid', 1, '2016-04-04 11:22:13', 1, '2016-04-04 11:22:13', NULL),
(7, '570ce46c6f279', 'fucklahwoy@gmail.com', '$2y$10$jBeY59m2q5A6OneGn8Fqe.DIAkS5Wz5d7Der5Eq99QST2oN6FO.di', 'Fucklah', 4, '2016-04-12 12:05:00', 1, '2016-04-12 12:05:00', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vbinary_baris`
--
CREATE TABLE IF NOT EXISTS `vbinary_baris` (
`baris` bigint(20)
,`pbaris` bigint(12)
,`max_elemen` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vbinary_kaki`
--
CREATE TABLE IF NOT EXISTS `vbinary_kaki` (
`urut` bigint(20)
);

-- --------------------------------------------------------

--
-- Structure for view `vbinary_baris`
--
DROP TABLE IF EXISTS `vbinary_baris`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbinary_baris` AS select distinct `binary_struktur`.`baris_urut` AS `baris`,(`binary_struktur`.`baris_urut` - 1) AS `pbaris`,pow(2,(`binary_struktur`.`baris_urut` - 1)) AS `max_elemen` from `binary_struktur` union all select (max(`binary_struktur`.`baris_urut`) + 1) AS `baris`,max(`binary_struktur`.`baris_urut`) AS `pbaris`,pow(2,((max(`binary_struktur`.`baris_urut`) + 1) - 1)) AS `max_elemen` from `binary_struktur`;

-- --------------------------------------------------------

--
-- Structure for view `vbinary_kaki`
--
DROP TABLE IF EXISTS `vbinary_kaki`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbinary_kaki` AS select 1 AS `urut` union select 2 AS `urut`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `binary_struktur`
--
ALTER TABLE `binary_struktur`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_member` (`id_member`), ADD UNIQUE KEY `kode` (`kode`), ADD UNIQUE KEY `baris_urut` (`baris_urut`,`side_urut`);

--
-- Indexes for table `board`
--
ALTER TABLE `board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_member`
--
ALTER TABLE `board_member`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_board` (`id_board`,`id_member`), ADD UNIQUE KEY `id_board_2` (`id_board`,`baris_urut`,`id_parent`,`kaki_urut`), ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `matrix_struktur`
--
ALTER TABLE `matrix_struktur`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_member` (`id_member`), ADD UNIQUE KEY `kode` (`kode`), ADD UNIQUE KEY `baris_urut` (`baris_urut`,`side_urut`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `kode` (`kode`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `binary_struktur`
--
ALTER TABLE `binary_struktur`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `board`
--
ALTER TABLE `board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `board_member`
--
ALTER TABLE `board_member`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `matrix_struktur`
--
ALTER TABLE `matrix_struktur`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `binary_struktur`
--
ALTER TABLE `binary_struktur`
ADD CONSTRAINT `fk_binary_member` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `board_member`
--
ALTER TABLE `board_member`
ADD CONSTRAINT `fk_board` FOREIGN KEY (`id_board`) REFERENCES `board` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_member_board` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `matrix_struktur`
--
ALTER TABLE `matrix_struktur`
ADD CONSTRAINT `fk_matrix_member` FOREIGN KEY (`id_member`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
